﻿using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.MessageModule.Abstractions.Managers
{
    [PublicAPI]
    public interface IMessageManager
    {
        [NotNull]
        Message Send([NotNull]UserId senderId, [NotNull]UserId receiverId, [NotNull]string text);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Message> GetMessages();

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Message> GetReceivedMessages([NotNull]UserId userId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Message> GetSentMessages([NotNull]UserId userId);
    }
}