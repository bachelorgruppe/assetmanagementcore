using System;
using System.Collections.Generic;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.MessageModule.ViewModels.Message
{
    public class MessageViewModel
    {
        public int ConversationCount { get; set; }
        public List<User> UserList { get; set; }
        public int UserListCount { get; set; }
        public List<Core.DB.Entities.Message> Messages { get; set; }
        public User Receiver { get; set; }
        public string TextToSend { get; set; }
        public Guid UserToSendTo { get; set; }
    }
}