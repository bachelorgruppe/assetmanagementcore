﻿using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.MessageModule.Abstractions.Managers;
using AssetManagementPlatform.MessageModule.Managers;
using AssetManagementPlatform.MessageModule.SavingManagers;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.MessageModule
{
    [PublicAPI]
    public class Setup : ISetup
    {
        /// <inheritdoc />
        public void AddServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IMessageManager, MessageManager>();
            services.AddScoped<SavingMessageManager>();
            services.AddEventBus(builder => builder.AddInMemoryEventBus(subscriber => subscriber.Subscribe<SentMessageEvent, MessageEventHandler>()));
        }

        public void AddSetup(IApplicationBuilder builder) { }
    }
}