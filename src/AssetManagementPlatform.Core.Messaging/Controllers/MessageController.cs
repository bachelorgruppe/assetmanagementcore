﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Notification.Abstractions;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.MessageModule.ViewModels.Message;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using IMessageManager = AssetManagementPlatform.MessageModule.Abstractions.Managers.IMessageManager;
using SavingMessageManager = AssetManagementPlatform.MessageModule.SavingManagers.SavingMessageManager;

namespace AssetManagementPlatform.MessageModule.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private readonly IMessageManager _messageManager;
        private readonly INotificationService _notificationService;
        private readonly IUserManager _userManager;

        public MessageController(SavingMessageManager messageManager, SavingUserManager userManager, INotificationService notificationService)
        {
            _messageManager = messageManager;
            _userManager = userManager;
            _notificationService = notificationService;
        }

        public ViewResult Index(Guid currentConversationUserId)
        {
            MessageViewModel model = new MessageViewModel();
            UserId currentUserId = User.GetUserId();

            if (currentConversationUserId == default)
            {
                currentConversationUserId = _userManager.GetUsers().First().Id;
                currentConversationUserId = currentUserId.Id; // todo remove when multiple users can login
            }

            model.ConversationCount = _userManager.GetUsers().Count(u => u.Id != currentUserId.Id);

            model.UserList = _userManager.GetUsers().Where(u => u.Id != currentUserId.Id).ToList();
            model.UserListCount = _userManager.GetUsers().Count(u => u.Id != currentUserId.Id);

            model.Messages = _messageManager.GetMessages()
                                            .Where(m => m.ReceiverId == currentConversationUserId &&
                                                        m.SenderId == currentUserId.Id ||
                                                        m.ReceiverId == currentUserId.Id &&
                                                        m.SenderId == currentConversationUserId)
                                            .ToList();

            model.Receiver = _userManager.GetUsers().GetEntity(new UserId(currentConversationUserId));

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendMessage(MessageViewModel model)
        {
            UserId currentUserId = User.GetUserId();
            UserId receiver = new UserId(model.UserToSendTo);
            var sender = await _userManager.GetUsers().GetEntityAsync(currentUserId);
            _messageManager.Send(currentUserId, receiver, model.TextToSend);
            await _notificationService.SendNotificationAsync(receiver, "NewMessage", new { UserName = sender.FullName, Text = model.TextToSend});
            return RedirectToAction("Index", new { currentConversationUserId = model.UserToSendTo });
        }
    }
}