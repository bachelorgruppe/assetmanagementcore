﻿using System.Linq;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.MessageModule.Abstractions.Managers;

namespace AssetManagementPlatform.MessageModule.Managers
{
    public class MessageManager : BaseManager<DataContext>, IMessageManager
    {
        public MessageManager(DataContext dbContext) : base(dbContext) { }

        public Message Send(UserId senderId, UserId receiverId, string text)
        {
            Message message = new Message(senderId.Id, receiverId.Id, text);
            Db.Messages.Add(message);
            return message;
        }

        public IQueryable<Message> GetMessages()
        {
            return Db.Messages;
        }

        public IQueryable<Message> GetReceivedMessages(UserId userId)
        {
            return Db.Messages.Where(message => message.ReceiverId == userId.Id);
        }

        public IQueryable<Message> GetSentMessages(UserId userId)
        {
            return Db.Messages.Where(message => message.SenderId == userId.Id);
        }
    }
}