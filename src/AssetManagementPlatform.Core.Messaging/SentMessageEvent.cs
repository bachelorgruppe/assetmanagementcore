﻿using JetBrains.Annotations;
using MessageId = AssetManagementPlatform.MessageModule.IdHelpers.MessageId;

namespace AssetManagementPlatform.MessageModule
{
    [UsedImplicitly]
    public class SentMessageEvent
    {
        public MessageId MessageId { get; set; }
    }
}