﻿using System.Linq;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using IMessageManager = AssetManagementPlatform.MessageModule.Abstractions.Managers.IMessageManager;

namespace AssetManagementPlatform.MessageModule.SavingManagers
{
    public class SavingMessageManager : BaseManager<DataContext>, IMessageManager
    {
        private readonly IMessageManager _manager;

        public SavingMessageManager(DataContext dbContext, IMessageManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public Message Send(UserId senderId, UserId receiverId, string text)
        {
            Message message = _manager.Send(senderId, receiverId, text);
            SaveContext();
            return message;
        }

        public IQueryable<Message> GetMessages()
        {
            return _manager.GetMessages();
        }

        public IQueryable<Message> GetReceivedMessages(UserId userId)
        {
            return _manager.GetReceivedMessages(userId);
        }

        public IQueryable<Message> GetSentMessages(UserId userId)
        {
            return _manager.GetSentMessages(userId);
        }
    }
}