﻿using System.Threading.Tasks;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.Notification.Abstractions;
using JKang.EventBus;

namespace AssetManagementPlatform.MessageModule
{
    public class MessageEventHandler : IEventHandler<SentMessageEvent>
    {
        private readonly DataContext _context;
        private readonly INotificationService _notificationService;

        public MessageEventHandler(INotificationService notificationService, DataContext context)
        {
            _notificationService = notificationService;
            _context = context;
        }

        public Task HandleEventAsync(SentMessageEvent @event)
        {
            Message message = _context.Messages.GetEntity(@event.MessageId);
            return _notificationService.SendNotificationAsync(message.Receiver, "MessageReceived", message.Text);
        }
    }
}