﻿using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.ConstructionModule.Code.Managers
{
    public class ToolSubscriptionManager : BaseManager<ConstructionContext>, IToolSubscriptionManager
    {
        private readonly IAssetGroupManager _assetGroupManager;

        public ToolSubscriptionManager(ConstructionContext dbContext, IAssetGroupManager assetGroupManager) : base(dbContext)
        {
            _assetGroupManager = assetGroupManager;
        }

        public IQueryable<Tool> GetSubscribedTools(UserId userId)
        {
            return _assetGroupManager.GetGroupsFromAsset(userId).SelectMany(group => group.Assets).Select(asset => asset.Asset).OfType<Tool>();
        }

        public ToolSubscriptionGroup Create(string name)
        {
            ToolSubscriptionGroup subscriptionGroup = new ToolSubscriptionGroup { Name = name };
            _assetGroupManager.CreateAssetGroup(subscriptionGroup);
            return subscriptionGroup;
        }

        public void Edit(ToolSubscriptionGroup subscriptionGroup)
        {
            Db.ToolSubscriptionGroups.Update(subscriptionGroup);
        }

        public IQueryable<Tool> GetToolsInSubscriptionGroup(ToolSubscriptionGroupId toolSubscriptionGroupId)
        {
            return _assetGroupManager.GetAssetsInGroup(toolSubscriptionGroupId).OfType<Tool>();
        }

        public IQueryable<User> GetEngineersInSubscriptionGroup(ToolSubscriptionGroupId toolSubscriptionGroupId)
        {
            return _assetGroupManager.GetAssetsInGroup(toolSubscriptionGroupId).OfType<User>();
        }

        public void Delete(ToolSubscriptionGroupId subscriptionGroupId)
        {
            ToolSubscriptionGroup toolSubscriptionGroup = Db.ToolSubscriptionGroups.GetEntity(subscriptionGroupId);
            Db.ToolSubscriptionGroups.Remove(toolSubscriptionGroup);
        }

        public void Subscribe(ToolSubscriptionGroupId subscriptionGroupId, UserId userId)
        {
            _assetGroupManager.AddAssetToGroup(subscriptionGroupId, userId);
        }

        public void Unsubscribe(ToolSubscriptionGroupId subscriptionGroupId, UserId userId)
        {
            _assetGroupManager.RemoveAssetFromGroup(subscriptionGroupId, userId);
        }

        public void AddTool(ToolSubscriptionGroupId subscriptionGroupId, ToolId toolId)
        {
            _assetGroupManager.AddAssetToGroup(subscriptionGroupId, toolId);
        }

        public void RemoveTool(ToolSubscriptionGroupId subscriptionGroupId, ToolId toolId)
        {
            _assetGroupManager.RemoveAssetFromGroup(subscriptionGroupId, toolId);
        }

        public IQueryable<ToolSubscriptionGroup> GetSubscriptionGroups()
        {
            return Db.ToolSubscriptionGroups.AsQueryable();
        }

        public IQueryable<ToolSubscriptionGroup> GetSubscriptionGroups(AssetId assetId)
        {
            return _assetGroupManager.GetGroupsFromAsset(assetId).OfType<ToolSubscriptionGroup>();
        }
    }
}