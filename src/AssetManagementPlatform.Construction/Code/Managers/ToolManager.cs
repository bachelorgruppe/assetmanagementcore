﻿using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.ConstructionModule.Code.Managers
{
    public class ToolManager : BaseManager<ConstructionContext>, IToolManager
    {
        private readonly IAssetManager _assetManager;
        private readonly IEventManager _eventManager;

        public ToolManager(ConstructionContext dbContext,
                           IAssetManager assetManager,
                           IEventManager eventManager) : base(dbContext)
        {
            _eventManager = eventManager;
            _assetManager = assetManager;
        }

        public IQueryable<Tool> GetToolsAssignedToUser(UserId userId)
        {
            return _eventManager.GetAssignedAssets<Tool>(userId);
        }

        public User GetUserAssignedToTool(ToolId toolId)
        {
            return _eventManager.GetUserAssigned(toolId);
        }

        public IQueryable<AssignmentRequestEvent> GetAssignmentRequests(ToolId toolId)
        {
            return _eventManager.GetEventsByType<AssignmentRequestEvent>(toolId).Where(x => x.IsActive);
        }

        public IQueryable<ReportBrokenEvent> GetRepairRequests(ToolId toolId)
        {
            return _eventManager.GetEventsByType<ReportBrokenEvent>(toolId).Where(x => x.IsActive);
        }

        public void CancelToolRequest(EventId eventId)
        {
            _eventManager.CancelEvent(eventId);
        }

        public AssignmentRequestEvent RequestTool(ToolId toolId, UserId userId)
        {
            var user = GetUserAssignedToTool(toolId);
            AssignmentRequestEvent assignmentRequestEvent = new AssignmentRequestEvent();

            // If Tool is owned by the user asking for it
            if (user != null && user.Id != userId.Id)
            {
                assignmentRequestEvent.RequestingUserId = userId.Id;
                _eventManager.CreateEvent(toolId, assignmentRequestEvent);

                return assignmentRequestEvent;
            }

            _eventManager.CreateEvent(toolId, new AssignmentEvent
            {
                AssignedUserId = userId.Id
            });

            return null;
        }

        public AssignmentConfirmationEvent AcceptTool(EventId eventId, ToolId toolId, UserId userId)
        {
            _eventManager.CancelEvent(eventId);
            var confirmAssignmentEvent = new AssignmentConfirmationEvent
            {
                ConfirmedUserId = userId.Id
            };
            _eventManager.CreateEvent(toolId, confirmAssignmentEvent);
            _eventManager.CreateEvent(toolId, new AssignmentEvent
            {
                AssignedUserId = userId.Id
            });

            return confirmAssignmentEvent;
        }

        public Tool CreateTool(string model, string manufacture, double? price)
        {
            Tool tool = new Tool
            {
                Model = model,
                Manufacture = manufacture,
                Price = price
            };
            _assetManager.CreateAsset(tool);
            return tool;
        }

        public void EditTool(Tool tool)
        {
            Db.Tools.Update(tool);
        }

        public IQueryable<Tool> GetTools()
        {
            return Db.Tools.AsNoTracking();
        }

        public void RemoveTool(ToolId toolId)
        {
            _assetManager.RemoveAsset(toolId);
        }

        public void AssignToUser(ToolId toolId, UserId userId)
        {
            _eventManager.CreateEvent(toolId, new AssignmentEvent
            {
                AssignedUserId = userId.Id
            });
        }

        public ReportBrokenEvent ReportTool(UserId userId, ToolId toolId, string reason)
        {
            ReportBrokenEvent theEvent = new ReportBrokenEvent
            {
                ReportedByUserId = userId.Id,
                Reason = reason
            };
            _eventManager.CreateEvent(toolId, theEvent);
            return theEvent;
        }

        public ReportFixedEvent FixTool(EventId eventId, UserId userId, ToolId toolId)
        {
            ReportFixedEvent theEvent = new ReportFixedEvent
            {
                FixedByUserId = userId.Id,
            };
            _eventManager.CancelEvent(eventId);
            _eventManager.CreateEvent(toolId, theEvent);
            return theEvent;
        }

        public bool IsBroken(ToolId toolId)
        {
            return GetRepairRequests(toolId).Any();
        }
    }
}