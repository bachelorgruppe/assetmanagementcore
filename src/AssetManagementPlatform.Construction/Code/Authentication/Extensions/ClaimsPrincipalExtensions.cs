﻿using System.Security.Claims;
using AssetManagementPlatform.Core.Authorization.Permissions;
using JetBrains.Annotations;

namespace AssetManagementPlatform.ConstructionModule.Code.Authentication.Extensions
{
    [PublicAPI]
    public static class ClaimsPrincipalExtensions
    {
        public static ConstructionSitePermissions GetConstructionUserPermissions([NotNull]this ClaimsPrincipal principal)
        {
            return SitePermissionsExtensions.CreateFromPermissionString<ConstructionSitePermissions>(principal.FindFirst(AssetClaims.ConstructionPermissions).Value);
        }

        public static bool TryGetConstructionUserPermissions([NotNull]this ClaimsPrincipal principal, out ConstructionSitePermissions constructionSitePermissions)
        {
            constructionSitePermissions = null;
            Claim findFirst = principal.FindFirst(AssetClaims.ConstructionPermissions);
            if (findFirst == null)
                return false;

            constructionSitePermissions = SitePermissionsExtensions.CreateFromPermissionString<ConstructionSitePermissions>(findFirst.Value);
            return true;
        }
    }
}