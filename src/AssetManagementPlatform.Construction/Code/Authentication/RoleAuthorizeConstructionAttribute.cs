﻿using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.ConstructionModule.Code.Authentication
{
    public sealed class RoleAuthorizeConstructionAttribute : TypeFilterAttribute
    {
        public RoleAuthorizeConstructionAttribute(params string[] item)
            : base(typeof(AuthorizeActionFilter))
        {
            Arguments = new object[] { item };
        }
    }
}