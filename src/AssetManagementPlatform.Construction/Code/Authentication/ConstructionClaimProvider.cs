﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AssetManagementPlatform.Core.Authentication;
using AssetManagementPlatform.Core.Authorization.Permissions;
using Microsoft.IdentityModel.JsonWebTokens;

namespace AssetManagementPlatform.ConstructionModule.Code.Authentication
{
    public class ConstructionClaimProvider : IDomainClaimProvider
    {
        public IEnumerable<Claim> GetClaims(string id)
        {
            if (id == "con1")
            {
                const string userId = "292fa3ef-8617-4f30-ae30-5571fa0488e1";
                Claim[] claims =
                {
                    new Claim(ClaimTypes.NameIdentifier, userId),
                    new Claim(JwtRegisteredClaimNames.NameId, userId),
                    new Claim(JwtRegisteredClaimNames.Sub, userId),
                    new Claim(JwtRegisteredClaimNames.GivenName, "Manager"),
                    new Claim("middle_name", "Den"),
                    new Claim(JwtRegisteredClaimNames.FamilyName, "Store"),
                    new Claim(JwtRegisteredClaimNames.Email, "A@B.dk"),
                    new Claim(Core.Authentication.AssetClaims.OrganizationId, "391ab491-4233-4a8f-b9c6-4719dbb202d9"),
                    new Claim(Core.Authorization.AssetClaims.Permissions, new BaseSitePermissions
                    {
                        Message = true,
                        CanSeeOrganizations = true,
                        IsAdmin = true,
                    }.GetPermissionsAsString()),
                    new Claim(AssetClaims.ConstructionPermissions, new ConstructionSitePermissions
                    {
                        CanCreateTool = true,
                        CanDeleteTool = true,
                        CanEditTool = true,
                        CanManageSubscriptions = true,
                        EngineerDashboard = true,
                        CanReportTool = true,
                        CanFixTool = true,
                        CanRequestTool = true,
                        CanSeeAllTools = true,
                        CanSeeAllUsers = true,
                        CanEditUser = true,
                        CanDeleteUser = true,
                    }.GetPermissionsAsString())
                };
                return claims;
            }

            if (id == "con2")
            {
                const string userId = "c7f9d850-9383-4142-b782-d7d4c9e7a8f0";
                Claim[] claims =
                {
                    new Claim(ClaimTypes.NameIdentifier, userId),
                    new Claim(JwtRegisteredClaimNames.NameId, userId),
                    new Claim(JwtRegisteredClaimNames.Sub, userId),
                    new Claim(JwtRegisteredClaimNames.GivenName, "Worker"),
                    new Claim("middle_name", "Den"),
                    new Claim(JwtRegisteredClaimNames.FamilyName, "Store"),
                    new Claim(JwtRegisteredClaimNames.Email, "A@B.dk"),
                    new Claim(Core.Authentication.AssetClaims.OrganizationId, "391ab491-4233-4a8f-b9c6-4719dbb202d9"),
                    new Claim(Core.Authorization.AssetClaims.Permissions, new BaseSitePermissions
                    {
                        Message = true,
                        CanSeeOrganizations = true
                    }.GetPermissionsAsString()),
                    new Claim(AssetClaims.ConstructionPermissions, new ConstructionSitePermissions
                    {
                        CanReportTool = true,
                        CanRequestTool = true,
                        CanSeeAllTools = true,

                    }.GetPermissionsAsString())
                };
                return claims;
            }

            if (id == "con3")
            {
                const string userId = "ac7a7638-dc99-4ad1-be16-e56cfae4ca42";
                Claim[] claims =
                {
                    new Claim(ClaimTypes.NameIdentifier, userId),
                    new Claim(JwtRegisteredClaimNames.NameId, userId),
                    new Claim(JwtRegisteredClaimNames.Sub, userId),
                    new Claim(JwtRegisteredClaimNames.GivenName, "Engineer"),
                    new Claim("middle_name", "Den"),
                    new Claim(JwtRegisteredClaimNames.FamilyName, "Store"),
                    new Claim(JwtRegisteredClaimNames.Email, "A@B.dk"),
                    new Claim(Core.Authentication.AssetClaims.OrganizationId, "9449e933-05e0-4c5c-bee4-15e6b73be3e8"),
                    new Claim(Core.Authorization.AssetClaims.Permissions, new BaseSitePermissions
                    {
                        Message = true,
                        CanSeeOrganizations = true
                    }.GetPermissionsAsString()),
                    new Claim(AssetClaims.ConstructionPermissions, new ConstructionSitePermissions
                    {
                        CanCreateTool = true,
                        CanDeleteTool = true,
                        CanEditTool = true,
                        CanSeeAllTools = true,
                        CanManageSubscriptions = true,
                        EngineerDashboard = true,
                        CanReportTool = true,
                        CanRequestTool = true,
                        CanFixTool = true,
                    }.GetPermissionsAsString())
                };
                return claims;
            }

            return Enumerable.Empty<Claim>();
        }
    }
}