﻿using System.Security.Claims;
using AssetManagementPlatform.Core.Authorization.Permissions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AssetManagementPlatform.ConstructionModule.Code.Authentication
{
    public class AuthorizeActionFilter : IAuthorizationFilter
    {
        private readonly ConstructionSitePermissions _item;

        public AuthorizeActionFilter(string[] item)
        {
            _item = SitePermissionsExtensions.CreateFromPermissionNames<ConstructionSitePermissions>(item);
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            Claim claim = context.HttpContext.User.FindFirst(AssetClaims.ConstructionPermissions);

            if (claim == null)
            {
                context.Result = new ForbidResult();
                return;
            }

            ConstructionSitePermissions baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionString<ConstructionSitePermissions>(claim.Value);
            bool isAuthorized = baseSitePermissions.HasAllPermissions(_item);

            if (!isAuthorized)
                context.Result = new ForbidResult();
        }
    }
}