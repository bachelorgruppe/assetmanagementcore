﻿namespace AssetManagementPlatform.ConstructionModule.Code.Authentication
{
    public static class AssetClaims
    {
        public const string ConstructionPermissions = "asset_permissions_construction";
    }
}