﻿using AssetManagementPlatform.Core.Authorization.Permissions;

namespace AssetManagementPlatform.ConstructionModule.Code.Authentication
{
    public class ConstructionSitePermissions : BasePermissions
    {
        [Order(1)]
        public bool CanCreateTool { get; set; }

        [Order(2)]
        public bool EngineerDashboard { get; set; }

        [Order(3)]
        public bool CanReportTool { get; set; }

        [Order(4)]
        public bool CanFixTool { get; set; }

        [Order(5)]
        public bool CanSeeAllTools { get; set; }

        [Order(6)]
        public bool CanDeleteTool { get; set; }

        [Order(7)]
        public bool CanEditTool { get; set; }

        [Order(8)]
        public bool CanManageSubscriptions { get; set; }

        [Order(9)]
        public bool CanRequestTool { get; set; }

        [Order(10)]
        public bool CanSeeAllUsers { get; set; }

        [Order(11)]
        public bool CanEditUser { get; set; }

        [Order(12)]
        public bool CanDeleteUser { get; set; }
    }
}