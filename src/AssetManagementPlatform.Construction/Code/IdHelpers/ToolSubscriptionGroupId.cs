﻿using System;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.IdHelpers;

namespace AssetManagementPlatform.ConstructionModule.Code.IdHelpers {
    public class ToolSubscriptionGroupId : IHelperId
    {
        public ToolSubscriptionGroupId(Guid assetId)
        {
            Id = assetId;
        }

        public Guid Id { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator ToolSubscriptionGroupId(ToolSubscriptionGroup asset)
        {
            return new ToolSubscriptionGroupId(asset.Id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator AssetGroupId(ToolSubscriptionGroupId asset)
        {
            return new AssetGroupId(asset.Id);
        }
    }
}