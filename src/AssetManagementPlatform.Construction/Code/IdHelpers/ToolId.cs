﻿using System;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;

namespace AssetManagementPlatform.ConstructionModule.Code.IdHelpers
{
    public class ToolId : IHelperId
    {
        public ToolId(Guid assetId)
        {
            Id = assetId;
        }

        public Guid Id { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator ToolId(Asset asset)
        {
            return new ToolId(asset.Id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator AssetId(ToolId toolId)
        {
            return new AssetId(toolId.Id);
        }
    }
}