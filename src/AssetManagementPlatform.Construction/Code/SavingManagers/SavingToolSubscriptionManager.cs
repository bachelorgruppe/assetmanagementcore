﻿using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.ConstructionModule.Code.SavingManagers
{
    public class SavingToolSubscriptionManager : BaseManager<ConstructionContext>, IToolSubscriptionManager
    {
        private readonly IToolSubscriptionManager _manager;

        public SavingToolSubscriptionManager(ConstructionContext dbContext, IToolSubscriptionManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<Tool> GetSubscribedTools(UserId userId)
        {
            return _manager.GetSubscribedTools(userId);
        }

        public void Subscribe(ToolSubscriptionGroupId subscriptionGroupId, UserId userId)
        {
            _manager.Subscribe(subscriptionGroupId, userId);
            SaveContext();
        }

        public void AddTool(ToolSubscriptionGroupId subscriptionGroupId, ToolId toolId)
        {
            _manager.AddTool(subscriptionGroupId, toolId);
            SaveContext();
        }

        public ToolSubscriptionGroup Create(string name)
        {
            ToolSubscriptionGroup toolSubscriptionGroup = _manager.Create(name);
            SaveContext();
            return toolSubscriptionGroup;
        }

        public void Delete(ToolSubscriptionGroupId subscriptionGroupId)
        {
            _manager.Delete(subscriptionGroupId);
            SaveContext();
        }

        public IQueryable<ToolSubscriptionGroup> GetSubscriptionGroups()
        {
            return _manager.GetSubscriptionGroups();
        }

        public IQueryable<ToolSubscriptionGroup> GetSubscriptionGroups(AssetId assetId)
        {
            return _manager.GetSubscriptionGroups(assetId);
        }

        public void Unsubscribe(ToolSubscriptionGroupId subscriptionGroupId, UserId userId)
        {
            _manager.Unsubscribe(subscriptionGroupId, userId);
            SaveContext();
        }

        public void RemoveTool(ToolSubscriptionGroupId subscriptionGroupId, ToolId toolId)
        {
            _manager.RemoveTool(subscriptionGroupId, toolId);
            SaveContext();
        }

        public void Edit(ToolSubscriptionGroup subscriptionGroup)
        {
            _manager.Edit(subscriptionGroup);
            SaveContext();
        }

        public IQueryable<Tool> GetToolsInSubscriptionGroup(ToolSubscriptionGroupId toolSubscriptionGroupId)
        {
            return _manager.GetToolsInSubscriptionGroup(toolSubscriptionGroupId);
        }

        public IQueryable<User> GetEngineersInSubscriptionGroup(ToolSubscriptionGroupId toolSubscriptionGroupId)
        {
            return _manager.GetEngineersInSubscriptionGroup(toolSubscriptionGroupId);
        }
    }
}