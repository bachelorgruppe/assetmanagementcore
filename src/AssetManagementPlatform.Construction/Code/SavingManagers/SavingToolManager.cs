﻿using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.ConstructionModule.Code.SavingManagers
{
    public class SavingToolManager : BaseManager<ConstructionContext>, IToolManager
    {
        private readonly IToolManager _manager;

        public SavingToolManager(ConstructionContext dbContext, IToolManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<Tool> GetToolsAssignedToUser(UserId userId)
        {
            return _manager.GetToolsAssignedToUser(userId);
        }

        public User GetUserAssignedToTool(ToolId toolId)
        {
            return _manager.GetUserAssignedToTool(toolId);
        }

        public IQueryable<AssignmentRequestEvent> GetAssignmentRequests(ToolId toolId)
        {
            return _manager.GetAssignmentRequests(toolId);
        }

        public IQueryable<ReportBrokenEvent> GetRepairRequests(ToolId toolId)
        {
            return _manager.GetRepairRequests(toolId);
        }

        public void CancelToolRequest(EventId eventId)
        {
            _manager.CancelToolRequest(eventId);
            SaveContext();
        }

        public AssignmentRequestEvent RequestTool(ToolId toolId, UserId userId)
        {
            var requestEvent = _manager.RequestTool(toolId, userId);
            SaveContext();
            return requestEvent;
        }

        public AssignmentConfirmationEvent AcceptTool(EventId eventId, ToolId toolId, UserId userId)
        {
            var confirmationEvent = _manager.AcceptTool(eventId, toolId, userId);
            SaveContext();
            return confirmationEvent;
        }

        public Tool CreateTool(string model, string manufacture, double? price)
        {
            Tool tool = _manager.CreateTool(model, manufacture, price);
            SaveContext();
            return tool;
        }

        public void RemoveTool(ToolId toolId)
        {
            _manager.RemoveTool(toolId);
            SaveContext();
        }

        public IQueryable<Tool> GetTools()
        {
            return _manager.GetTools();
        }

        public void AssignToUser(ToolId tool, UserId user)
        {
            _manager.AssignToUser(tool, user);
            SaveContext();
        }

        public ReportBrokenEvent ReportTool(UserId userId, ToolId toolId, string reason)
        {
            ReportBrokenEvent theEvent = _manager.ReportTool(userId, toolId, reason);
            SaveContext();
            return theEvent;
        }

        public ReportFixedEvent FixTool(EventId eventId, UserId userId, ToolId toolId)
        {
            ReportFixedEvent theEvent = _manager.FixTool(eventId,userId, toolId);
            SaveContext();
            return theEvent;
        }

        public bool IsBroken(ToolId toolId)
        {
            return _manager.IsBroken(toolId);
        }

        public void EditTool(Tool tool)
        {
            _manager.EditTool(tool);
            SaveContext();
        }
    }
}