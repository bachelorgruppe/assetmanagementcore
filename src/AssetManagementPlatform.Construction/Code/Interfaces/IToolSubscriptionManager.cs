﻿using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.ConstructionModule.Code.Interfaces
{
    public interface IToolSubscriptionManager
    {
        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Tool> GetSubscribedTools([NotNull]UserId userId);

        void Subscribe([NotNull]ToolSubscriptionGroupId subscriptionGroupId, [NotNull]UserId userId);
        void AddTool([NotNull]ToolSubscriptionGroupId subscriptionGroupId, [NotNull]ToolId toolId);
        ToolSubscriptionGroup Create(string name);
        void Delete(ToolSubscriptionGroupId subscriptionGroupId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<ToolSubscriptionGroup> GetSubscriptionGroups();

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<ToolSubscriptionGroup> GetSubscriptionGroups([NotNull]AssetId assetId);

        void Unsubscribe([NotNull]ToolSubscriptionGroupId subscriptionGroupId, [NotNull]UserId userId);
        void RemoveTool([NotNull]ToolSubscriptionGroupId subscriptionGroupId, [NotNull]ToolId toolId);
        void Edit(ToolSubscriptionGroup subscriptionGroup);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Tool> GetToolsInSubscriptionGroup([NotNull]ToolSubscriptionGroupId toolSubscriptionGroupId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<User> GetEngineersInSubscriptionGroup([NotNull]ToolSubscriptionGroupId toolSubscriptionGroupId);
    }
}