﻿using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.ConstructionModule.Code.Interfaces
{
    public interface IToolManager
    {
        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Tool> GetToolsAssignedToUser([NotNull]UserId userId);

        [Pure]
        [CanBeNull]
        User GetUserAssignedToTool([NotNull]ToolId toolId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<AssignmentRequestEvent> GetAssignmentRequests([NotNull]ToolId toolId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<ReportBrokenEvent> GetRepairRequests([NotNull]ToolId toolId);

        void CancelToolRequest([NotNull]EventId eventId);

        [NotNull]
        AssignmentRequestEvent RequestTool([NotNull]ToolId toolId, [NotNull]UserId userId);

        [NotNull]
        AssignmentConfirmationEvent AcceptTool([NotNull]EventId eventId, [NotNull]ToolId toolId, [NotNull]UserId userId);

        [NotNull]
        Tool CreateTool(string model, string manufacture, double? price);

        void RemoveTool([NotNull]ToolId toolId);
        void EditTool([NotNull]Tool tool);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Tool> GetTools();

        void AssignToUser([NotNull]ToolId tool, [NotNull]UserId user);

        [NotNull]
        ReportBrokenEvent ReportTool([NotNull]UserId userId, [NotNull]ToolId toolId, string reason);

        [NotNull]
        ReportFixedEvent FixTool([NotNull]EventId eventId, [NotNull]UserId userId, [NotNull]ToolId toolId);

        bool IsBroken([NotNull]ToolId toolId);
    }
}