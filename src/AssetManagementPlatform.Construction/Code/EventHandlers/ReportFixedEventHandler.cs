﻿using System.Threading.Tasks;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Notification.Abstractions;
using JKang.EventBus;

namespace AssetManagementPlatform.ConstructionModule.Code.EventHandlers
{
    public class ReportFixedEventHandler : IEventHandler<ReportFixedEvent>
    {
        private readonly INotificationService _notificationService;
        private readonly IToolSubscriptionManager _subscriptionManager;
        private readonly IUserManager _userManager;

        public ReportFixedEventHandler(INotificationService notificationService,
                                       IToolSubscriptionManager subscriptionManager,
                                       IUserManager userManager)
        {
            _notificationService = notificationService;
            _subscriptionManager = subscriptionManager;
            _userManager = userManager;
        }

        public async Task HandleEventAsync(ReportFixedEvent @event)
        {
            ToolId toolId = new ToolId(@event.AssetId);
            UserId userId = new UserId(@event.FixedByUserId);
            User user = _userManager.GetUsers().GetEntity(userId);
            foreach (ToolSubscriptionGroup toolSubscriptionGroup in _subscriptionManager.GetSubscriptionGroups(toolId))
            {
                await _notificationService.SendGroupNotificationAsync(toolSubscriptionGroup, nameof(ReportFixedEvent), new { UserName = user.FullName, ToolId = toolId.Id });
            }
        }
    }
}