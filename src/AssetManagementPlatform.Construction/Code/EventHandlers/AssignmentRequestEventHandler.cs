﻿using System.Threading.Tasks;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Notification.Abstractions;
using JKang.EventBus;

namespace AssetManagementPlatform.ConstructionModule.Code.EventHandlers
{
    public class AssignmentRequestEventHandler : IEventHandler<AssignmentRequestEvent>
    {
        private readonly INotificationService _notificationService;
        private readonly IUserManager _userManager;
        private readonly IEventManager _eventManager;

        public AssignmentRequestEventHandler(INotificationService notificationService,
                                             IUserManager userManager,
                                             IEventManager eventManager)
        {
            _notificationService = notificationService;
            _userManager = userManager;
            _eventManager = eventManager;
        }

        public async Task HandleEventAsync(AssignmentRequestEvent @event)
        {
            ToolId toolId = new ToolId(@event.AssetId);
            UserId userId = new UserId(@event.RequestingUserId);

            User userAssigned = _eventManager.GetUserAssigned(toolId);
            User user = _userManager.GetUsers().GetEntity(userId);
            await _notificationService.SendNotificationAsync(userAssigned, nameof(AssignmentRequestEvent), new { UserName = user.FullName, ToolId = toolId.Id });
        }
    }
}