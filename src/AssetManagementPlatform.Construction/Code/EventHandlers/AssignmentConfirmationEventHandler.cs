﻿using System.Threading.Tasks;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Notification.Abstractions;
using JKang.EventBus;

namespace AssetManagementPlatform.ConstructionModule.Code.EventHandlers
{
    public class AssignmentConfirmationEventHandler : IEventHandler<AssignmentConfirmationEvent>
    {
        private readonly INotificationService _notificationService;
        private readonly IToolSubscriptionManager _subscriptionManager;
        private readonly IUserManager _userManager;

        public AssignmentConfirmationEventHandler(INotificationService notificationService,
                                        IToolSubscriptionManager subscriptionManager,
                                        IUserManager userManager)
        {
            _notificationService = notificationService;
            _subscriptionManager = subscriptionManager;
            _userManager = userManager;
        }

        public async Task HandleEventAsync(AssignmentConfirmationEvent @event)
        {
            ToolId toolId = new ToolId(@event.AssetId);
            User user = _userManager.GetUsers().GetEntity(new UserId(@event.ConfirmedUserId));
            foreach (ToolSubscriptionGroup toolSubscriptionGroup in _subscriptionManager.GetSubscriptionGroups(toolId))
            {
                await _notificationService.SendGroupNotificationAsync(toolSubscriptionGroup, nameof(AssignmentConfirmationEvent), new { UserName = user.FullName, ToolId = toolId.Id });
            }
        }
    }
}