﻿using System;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.ConstructionModule
{
    public class ConstructionContext : DataContext
    {
        public ConstructionContext(DbContextOptions<DataContext> dbContextOptions) : base(dbContextOptions) { }

        public DbSet<Tool> Tools { get; set; }
        public DbSet<ToolSubscriptionGroup> ToolSubscriptionGroups { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Tool>()
                .HasBaseType<Asset>()
                .Property(x => x.Price);
                        //.HasConversion(v => Decimal.ToDouble((decimal)v),
                        //    v => Convert.ToDecimal(v));

            modelBuilder.Entity<ToolSubscriptionGroup>()
                        .Ignore(g => g.Engineers)
                        .Ignore(g => g.Tools)
                        .HasBaseType<AssetGroup>();

            #region Events

            modelBuilder.Entity<ReportBrokenEvent>()
                        .HasOne(a => a.ReportedByUser)
                        .WithMany()
                        .HasForeignKey(a => a.ReportedByUserId);

            modelBuilder.Entity<ReportBrokenEvent>()
                        .HasBaseType<Event>();

            modelBuilder.Entity<ReportFixedEvent>()
                        .HasOne(a => a.FixedByUser)
                        .WithMany()
                        .HasForeignKey(a => a.FixedByUserId);

            modelBuilder.Entity<ReportFixedEvent>()
                        .HasBaseType<Event>();

            #endregion
        }
    }
}