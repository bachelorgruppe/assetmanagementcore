﻿using JetBrains.Annotations;

[assembly: AspMvcViewLocationFormat("/Views/{1}/{0}.cshtml")]