﻿using System.Collections.Generic;
using System.Security.Claims;
using AssetManagementPlatform.ConstructionModule.Code.Authentication;
using AssetManagementPlatform.ConstructionModule.Code.Authentication.Extensions;
using AssetManagementPlatform.ConstructionModule.Code.EventHandlers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Code.Managers;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.Authentication;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.SignalR;
using AssetManagementPlatform.Core.Web.Navbar;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.JsonWebTokens;

namespace AssetManagementPlatform.ConstructionModule
{
    [PublicAPI]
    public class Setup : ISetup
    {
        public void AddServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ConstructionContext>(optionsBuilder => optionsBuilder.UseMySql(configuration.GetConnectionString("ConstructionContext")));
            //services.AddDbContext<ConstructionContext>(optionsBuilder => optionsBuilder.UseInMemoryDatabase("db"));
            services.AddScoped(provider => (DataContext)provider.GetService<ConstructionContext>());

            services.AddScoped<IDbContextSeed, DbConstructionSeeder>();

            services.AddScoped<IToolManager, ToolManager>();
            services.AddScoped<SavingToolManager>();

            services.AddScoped<IToolSubscriptionManager, ToolSubscriptionManager>();
            services.AddScoped<SavingToolSubscriptionManager>();

            services.AddScoped<INavBarService, DefaultNavBarService>(provider =>
            {
                IHttpContextAccessor httpContextAccessor = provider.GetRequiredService<IHttpContextAccessor>();
                ClaimsPrincipal httpContextUser = httpContextAccessor.HttpContext.User;
                Claim claim = httpContextUser.FindFirst(JwtRegisteredClaimNames.GivenName);

                string name = claim != null ? claim.Value : "NoUser";

                List<NavBarItem> barItems = new List<NavBarItem>();

                barItems.Add(new NavBarItem("Home", "Home", "Index"));
                barItems.Add(new NavBarItem("Tools", "Tool", "Index"));
                barItems.Add(new NavBarItem("Reservations", "Reservation", "Index"));
                barItems.Add(new NavBarItem("Projects", "Project", "Index"));
                barItems.Add(new NavBarItem("Organization", "Organization", "Index"));
                barItems.Add(new NavBarItem("Users", "User", "Index"));

                if (httpContextUser.TryGetConstructionUserPermissions(out ConstructionSitePermissions permissions2) && permissions2.CanManageSubscriptions)
                    barItems.Add(new NavBarItem("Subscription", "Subscription", "Index"));

                barItems.Add(new NavBarItem("Messages", "Message", "Index", true));
                barItems.Add(new NavBarItem(name, "Login", "Index", true));

                return new DefaultNavBarService(barItems);
            });

            services.AddSingleton<IDomainClaimProvider, ConstructionClaimProvider>();

            services.AddSingleton<IHomeRedirectionService, DefaultRedirectionService>(provider => new DefaultRedirectionService("Construction", "Index"));




            services.AddEventBus(builder => builder.AddInMemoryEventBus(subscriber =>
            {
                subscriber.Subscribe<ReportFixedEvent, ReportFixedEventHandler>();
                subscriber.Subscribe<ReportBrokenEvent, ReportBrokenEventHandler>();
                subscriber.Subscribe<AssignmentRequestEvent, AssignmentRequestEventHandler>();
                subscriber.Subscribe<AssignmentConfirmationEvent, AssignmentConfirmationEventHandler>();
            }));

            services.AddAuthentication();
            services.AddAuthorization();
            services.UseFakeAuth();

            services.AddSignalRSetup();
        }

        public void AddSetup(IApplicationBuilder builder)
        {
            builder.UseAuthentication();
            builder.UseAuthorization();
            builder.AddSignalRSetup();
        }
    }
}