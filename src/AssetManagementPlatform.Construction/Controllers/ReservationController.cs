﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.ViewModels.Reservation;
using AssetManagementPlatform.ConstructionModule.ViewModels.Reservation.Model;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.ConstructionModule.Controllers
{
    public class ReservationController : Controller
    {
        private readonly IReservationManager _reservationManager;
        private readonly IToolManager _toolManager;
        private readonly IUserManager _userManager;

        public ReservationController(SavingReservationManager reservationManager, SavingToolManager toolManager, SavingUserManager userManager)
        {
            _reservationManager = reservationManager;
            _toolManager = toolManager;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            ReservationIndexModel indexModel = new ReservationIndexModel();
            var reservationEvents = _reservationManager.GetReservationEvents();
            indexModel.Reservations = reservationEvents
                                      .Include(a => a.ReservedAsset)
                                      .Include(a => a.ReservedTo)
                                      .Where(a => a.IsActive)
                                      .Select(x => new ReservationModel
                                      {
                                          ReservationEvent = x,
                                          Tool = (Tool)x.ReservedAsset
                                      })
                                      .ToList();

            return View(indexModel);
        }

        public IActionResult Create()
        {
            ReservationCreateModel createModel = new ReservationCreateModel();
            createModel.ReservedToOptions = GetReservedToOptions();
            createModel.ReservedAssetOptions = GetReservedAssetOptions();
            return View(createModel);
        }

        [HttpPost]
        public IActionResult Create(ReservationCreateModel createModel)
        {
            AssetId reservedTo = new AssetId(createModel.ReservedToId);
            AssetId reservedAsset = new AssetId(createModel.ReservedAssetId);
            _reservationManager.CreateReservation(createModel.ReservationStartDate, createModel.ReservationEndDate, reservedTo, reservedAsset);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(Guid id)
        {
            ReservationEditModel editModel = new ReservationEditModel();
            ReservationEvent reservationEvent = _reservationManager.GetReservationEvents().GetEntity(new EventId(id));

            editModel.ReservationStartDate = reservationEvent.ReservationStartDate;
            editModel.ReservationEndDate = reservationEvent.ReservationEndDate;

            editModel.ReservedToOptions = GetReservedToOptions()
                .Select(r =>
                {
                    r.Selected = reservationEvent.ReservedToId.ToString() == r.Value;
                    return r;
                }).ToList();
            editModel.ReservedAssetOptions = GetReservedAssetOptions()
                .Select(r =>
                {
                    r.Selected = reservationEvent.ReservedAssetId.ToString() == r.Value;
                    return r;
                }).ToList();
            return View(editModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ReservationEditModel editModel)
        {
            _reservationManager.CancelReservation(new EventId(editModel.Id));
            AssetId reservedTo = new AssetId(editModel.ReservedToId);
            AssetId reservedAsset = new AssetId(editModel.ReservedAssetId);
            _reservationManager.CreateReservation(editModel.ReservationStartDate, editModel.ReservationEndDate, reservedTo, reservedAsset);
            return RedirectToAction("Index");
        }

        private List<SelectListItem> GetReservedAssetOptions()
        {
            List<SelectListItem> reservedAssetOptions = _toolManager.GetTools()
                                                                    .Select(a => new SelectListItem($"{a.Manufacture}, {a.Model}", a.Id.ToString()))
                                                                    .ToList();

            return reservedAssetOptions;
        }

        private List<SelectListItem> GetReservedToOptions()
        {
            List<SelectListItem> reservedToOptions = _userManager.GetUsers()
                                                                 .Select(a => new SelectListItem(a.FullName, a.Id.ToString()))
                                                                 .ToList();

            return reservedToOptions;
        }

        public IActionResult Delete(Guid id)
        {
            _reservationManager.CancelReservation(new EventId(id));
            return RedirectToAction("Index");
        }
    }
}