﻿using System;
using System.Linq;
using AssetManagementPlatform.ConstructionModule.ViewModels.Project;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.Core.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.ConstructionModule.Controllers
{
    public class ProjectController : BaseController
    {
        private readonly IOrganizationManager _organizationManager;
        private readonly IProjectManager _projectManager;
        private readonly IUserManager _userManager;

        public ProjectController(SavingProjectManager projectManager, IOrganizationManager organizationManager, IUserManager userManager)
        {
            _projectManager = projectManager;
            _organizationManager = organizationManager;
            _userManager = userManager;
        }

        public ViewResult Index(string query)
        {
            var projects = _projectManager.GetProjects();

            if (!string.IsNullOrEmpty(query))
                projects = projects.Where(a => a.Name.Contains(query, StringComparison.OrdinalIgnoreCase) || a.Location.Address.Contains(query, StringComparison.OrdinalIgnoreCase));


            var model = projects.Include(x => x.Assets)
                                   .Include(x => x.Organization)
                                   .ThenInclude(x => x.Location)
                                   .Include(x => x.Location)
                                   .ToList();




            return View(model);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ProjectCreateModel createModel)
        {
            Organization org = _organizationManager.GetOrganizations().GetEntity(User.GetUserOrganizationId());
            Location location = new Location
            {
                Address = createModel.Address
            };
            _projectManager.CreateProject(createModel.Name, location, org);

            return RedirectToAction("Index");
        }

        public ViewResult Edit(Guid id)
        {
            Project project = _projectManager.GetProjects()
                                             .Include(x => x.Location)
                                             .GetEntity(new ProjectId(id));

            ProjectEditModel model = new ProjectEditModel
            {
                Name = project.Name,
                Address = project.Location.Address
            };
            return View(model);
        }

        public IActionResult Delete(Guid id)
        {
            _projectManager.RemoveProject(new ProjectId(id));
            return RedirectToAction("Index");
        }

        public ViewResult Details(Guid id)
        {
            ProjectId projectId = new ProjectId(id);
            Project project = _projectManager.GetProjects()
                                             .Include(x => x.Location)
                                             .GetEntity(projectId);

            return View(new ProjectDetailsModel
            {
                Name = project.Name,
                Address = project.Location.Address,
                CreatedOn = project.CreatedOn,
                UpdatedOn = project.UpdatedOn,
                Users = _userManager.GetUserListByProject(projectId).ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProjectEditModel editModel)
        {
            if (ModelState.IsValid)
            {
                ProjectId projectId = new ProjectId(editModel.Id);
                Project project = _projectManager.GetProjects()
                                                 .Include(x => x.Location)
                                                 .GetEntity(projectId);

                project.Name = editModel.Name;
                project.Location = new Location
                {
                    Address = editModel.Address
                };
                _projectManager.EditProject(project);

                return RedirectToAction("Index");
            }

            return View(editModel);
        }
    }
}