﻿using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.ConstructionModule.Code.Authentication;
using AssetManagementPlatform.ConstructionModule.Code.Authentication.Extensions;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.ViewModels.Construction;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.ConstructionModule.Controllers
{
    public class ConstructionController : BaseController
    {
        private readonly IToolManager _toolManager;
        private readonly IToolSubscriptionManager _toolSubscriptionManager;

        public ConstructionController(SavingToolManager toolManager, SavingToolSubscriptionManager toolSubscriptionManager)
        {
            _toolManager = toolManager;
            _toolSubscriptionManager = toolSubscriptionManager;
        }

        public IActionResult Index()
        {
            if (User.GetConstructionUserPermissions().EngineerDashboard)
                return RedirectToAction("EngineerIndex");

            return View();
        }

        [RoleAuthorizeConstruction(nameof(ConstructionSitePermissions.EngineerDashboard))]
        public IActionResult EngineerIndex()
        {
            var model = new ConstructionEngineerIndexModel();
            UserId userId = User.GetUserId();
            var tools = _toolSubscriptionManager.GetSubscribedTools(userId).ToList();
            model.BrokenTools =  tools.Where(t => _toolManager.IsBroken(new ToolId(t.Id))).ToList();

            return View(model);
        }
    }
}