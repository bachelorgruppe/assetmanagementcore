﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.ConstructionModule.Code.Authentication;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.ViewModels.Shared;
using AssetManagementPlatform.ConstructionModule.ViewModels.Subscription;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Web;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.ConstructionModule.Controllers
{
    [RoleAuthorizeConstruction(nameof(ConstructionSitePermissions.CanManageSubscriptions))]
    public class SubscriptionController : BaseController
    {
        private readonly IToolSubscriptionManager _subscriptionManager;
        private readonly IUserManager _userManager;
        private readonly IToolManager _toolManager;

        public SubscriptionController(SavingToolSubscriptionManager subscriptionManager, SavingUserManager userManager, SavingToolManager toolManager)
        {
            _subscriptionManager = subscriptionManager;
            _userManager = userManager;
            _toolManager = toolManager;
        }

        public async Task<IActionResult> Index()
        {
            var listAsync = await _subscriptionManager.GetSubscriptionGroups()
                                                      .Include(g => g.Assets)
                                                      .ThenInclude(a => a.Asset)
                                                      .ToListAsync();

            return View(listAsync);
        }

        [HttpPost]
        public IActionResult Create(string name)
        {
            if(string.IsNullOrEmpty(name))
                return RedirectToAction("Index");

            var toolSubscriptionGroup = _subscriptionManager.Create(name);

            return RedirectToAction("Details", new { id = toolSubscriptionGroup.Id });
        }

        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            _subscriptionManager.Delete(new ToolSubscriptionGroupId(id));

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult DeleteTool(Guid id, Guid toolId)
        {
            _subscriptionManager.RemoveTool(new ToolSubscriptionGroupId(id), new ToolId(toolId));

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var toolSubscriptionGroupId = new ToolSubscriptionGroupId(id);
            var model = await _subscriptionManager.GetSubscriptionGroups()
                                                  .Include(g => g.Assets)
                                                  .ThenInclude(a => a.Asset)
                                                  .GetEntityAsync(toolSubscriptionGroupId);

            return View(model);
        }

        public ViewResult Edit(Guid id)
        {
            var toolSubscriptionGroup = _subscriptionManager.GetSubscriptionGroups()
                                                            .Include(n => n.Assets)
                                                            .GetEntity(new ToolSubscriptionGroupId(id));
            var subscribedEngineers = _subscriptionManager.GetEngineersInSubscriptionGroup(toolSubscriptionGroup).ToList();
            var subscribedTools = _subscriptionManager.GetToolsInSubscriptionGroup(toolSubscriptionGroup).ToList();
            return View(new SubsctiptionEditModel
            {
                Id = toolSubscriptionGroup.Id,
                Name = toolSubscriptionGroup.Name,
                SubscribedEngineers = GetEngineerOptions()
                    .Select(u =>
                    {
                        u.IsSelected = subscribedEngineers.Any(x => x.Id == u.Id);
                        return u;
                    }).ToList(),
                SubscribedTools = GetToolOptions()
                    .Select(t =>
                    {
                        t.IsSelected = subscribedTools.Any(x => x.Id == t.Id);
                        return t;
                    }).ToList()
        });
        }

        [HttpPost]
        public ActionResult Edit(SubsctiptionEditModel editModel)
        {
            if (!ModelState.IsValid) return View(editModel);

            var toolSubscriptionGroupId = new ToolSubscriptionGroupId(editModel.Id);
            var toolSubscriptionGroup = _subscriptionManager.GetSubscriptionGroups()
                                                            .Include(n => n.Assets)
                                                            .GetEntity(toolSubscriptionGroupId);

            var subscribedEngineers = _subscriptionManager.GetEngineersInSubscriptionGroup(toolSubscriptionGroup);
            var subscribedTools = _subscriptionManager.GetToolsInSubscriptionGroup(toolSubscriptionGroup);

            foreach (var engineerSelection in editModel.SubscribedEngineers)
            {
                var userId = new UserId(engineerSelection.Id);
                if (engineerSelection.IsSelected && !subscribedEngineers.Any(u => u.Id == engineerSelection.Id))
                    _subscriptionManager.Subscribe(toolSubscriptionGroupId, userId);
                if (!engineerSelection.IsSelected && subscribedEngineers.Any(u => u.Id == engineerSelection.Id))
                    _subscriptionManager.Unsubscribe(toolSubscriptionGroupId, userId);
            }

            foreach (var toolSelection in editModel.SubscribedTools)
            {
                var toolId = new ToolId(toolSelection.Id);
                if (toolSelection.IsSelected && !subscribedTools.Any(t => t.Id == toolSelection.Id))
                    _subscriptionManager.AddTool(toolSubscriptionGroupId, toolId);
                if (!toolSelection.IsSelected && subscribedTools.Any(t => t.Id == toolSelection.Id))
                    _subscriptionManager.RemoveTool(toolSubscriptionGroupId, toolId);
            }

            toolSubscriptionGroup.Name = editModel.Name;
            _subscriptionManager.Edit(toolSubscriptionGroup);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult DeleteSubscriber(Guid id, Guid userId)
        {
            _subscriptionManager.Unsubscribe(new ToolSubscriptionGroupId(id), new UserId(userId));

            return RedirectToAction("Index");
        }

        private IEnumerable<ItemSelection> GetEngineerOptions()
        {
            return _userManager.GetUsers()
                               .Select(u => new ItemSelection
                               {
                                   Id = u.Id,
                                   Name = u.FullName,
                                   IsSelected = false
                               });
        }

        private IEnumerable<ItemSelection> GetToolOptions()
        {
            return _toolManager.GetTools()
                               .Select(t => new ItemSelection
                               {
                                   Id = t.Id,
                                   Name = t.Model,
                                   IsSelected = false
                               });
        }
    }
}