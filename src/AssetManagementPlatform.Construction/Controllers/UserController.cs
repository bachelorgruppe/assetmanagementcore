﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.ViewModels.User;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.Core.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.ConstructionModule.Controllers
{
    public class UserController : BaseController
    {
        private readonly IProjectManager _projectManager;
        private readonly IToolManager _toolManager;
        private readonly IUserManager _userManager;
        private readonly IOrganizationManager _organizationManager;

        public UserController(IToolManager toolManager, SavingUserManager userManager,
                              SavingOrganizationManager organizationManager, SavingProjectManager projectManager)
        {
            _toolManager = toolManager;
            _userManager = userManager;
            _organizationManager = organizationManager;
            _projectManager = projectManager;
        }

        public ViewResult Index(string query)
        {
            OrganizationId organization = User.GetUserOrganizationId();
            IQueryable<User> users = _userManager.GetUserListByOrganization(organization);

            if (!string.IsNullOrEmpty(query))
                users = users.Where(a => a.FirstName.Contains(query, StringComparison.OrdinalIgnoreCase));

            var model = users
                        .Include(x => x.Project)
                        .ThenInclude(x => x.Location)
                        .Include(x => x.Organization)
                        .ToList();

            return View(model);
        }

        public IActionResult Create()
        {
            return View(new UserCreateModel
            {
                ProjectOptions = GetProjectOptions()
            });
        }

        [HttpPost]
        public RedirectToActionResult Create(UserCreateModel createModel)
        {
            User user = _userManager.CreateUser(createModel.Firstname, createModel.LastName);
            _organizationManager.AssignAssetToOrganization(user, User.GetUserOrganizationId());

            if (createModel.ProjectId.HasValue)
                _projectManager.AssignAssetToProject(new ProjectId(createModel.ProjectId.Value), user);

            return RedirectToAction("Index");
        }

        public ViewResult Details(Guid id)
        {
            UserId userId = new UserId(id);
            User user = _userManager.GetUsers()
                                    .Include(x => x.Project)
                                    .ThenInclude(x => x.Location)
                                    .GetEntity(userId);

            List<Tool> tools = _toolManager.GetToolsAssignedToUser(user).ToList();
            UserDetailsModel model = new UserDetailsModel
            {
                Firstname = user.FirstName,
                LastName = user.LastName,
                Address = user.Project?.Location?.Address ?? "",
                CreatedOn = user.CreatedOn,
                UpdatedOn = user.UpdatedOn,
                Project = user.Project,
                Tools = tools
            };
            return View(model);
        }

        public RedirectToActionResult Delete(Guid id)
        {
            _userManager.RemoveUser(new UserId(id));
            return RedirectToAction("Index");
        }

        public ViewResult Edit(Guid id)
        {
            User user = _userManager.GetUsers()
                                    .Include(x => x.Project)
                                    .GetEntity(new UserId(id));

            UserEditModel editmodel = new UserEditModel
            {
                ProjectOptions = GetProjectOptions()
                    .Select(x =>
                    {
                        x.Selected = user.Project?.Id.ToString() == x.Value;
                        return x;
                    }).ToList(),
                Firstname = user.FirstName,
                LastName = user.LastName,
                Address = user.Project.Location.Address
            };

            return View(editmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserEditModel editModel)
        {
            if (!ModelState.IsValid)
                return View(editModel);

            UserId userId = new UserId(editModel.Id);

            User user = _userManager.GetUsers().GetEntity(userId);
            //todo fix hacks
            user.FirstName = string.IsNullOrEmpty(editModel.Firstname) ? user.FirstName : editModel.Firstname;
            user.LastName = string.IsNullOrEmpty(editModel.LastName) ? user.LastName : editModel.LastName;

            if (editModel.ProjectId.HasValue)
                _projectManager.AssignAssetToProject(new ProjectId(editModel.ProjectId.Value), user);

            _userManager.EditUser(user);

            return RedirectToAction("Index");

        }

        private IList<SelectListItem> GetProjectOptions()
        {
            List<SelectListItem> projectOptions = _projectManager.GetProjectsByOrganization(User.GetUserOrganizationId())
                                                                 .Select(x => new SelectListItem(x.Name, x.Id.ToString()))
                                                                 .ToList();
            projectOptions.Add(new SelectListItem("", null));
            return projectOptions;
        }
    }
}