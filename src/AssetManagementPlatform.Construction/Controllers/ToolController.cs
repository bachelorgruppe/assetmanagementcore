﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.ConstructionModule.Code.Authentication;
using AssetManagementPlatform.ConstructionModule.Code.Authentication.Extensions;
using AssetManagementPlatform.ConstructionModule.Code.IdHelpers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.ViewModels.Tool;
using AssetManagementPlatform.ConstructionModule.ViewModels.Tool.Model;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.Core.Web;
using JKang.EventBus;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.ConstructionModule.Controllers
{
    public class ToolController : BaseController
    {
        private readonly IAssetManager _assetManager;
        private readonly IEventManager _eventManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly IProjectManager _projectManager;
        private readonly IToolManager _toolManager;
        private readonly IOrganizationManager _organizationManager;
        private readonly IUserManager _userManager;

        public ToolController(SavingToolManager toolManager,
                              SavingOrganizationManager organizationManager,
                              SavingEventManager eventManager,
                              SavingUserManager userManager,
                              IEventPublisher eventPublisher,
                              SavingAssetManager assetManager,
                              SavingProjectManager projectManager)
        {
            _toolManager = toolManager;
            _organizationManager = organizationManager;
            _eventManager = eventManager;
            _eventPublisher = eventPublisher;
            _assetManager = assetManager;
            _projectManager = projectManager;
            _userManager = userManager;
        }

        public async Task<IActionResult> IndexAdmin(string query)
        {

            var toolQuery = _toolManager.GetTools();

            if (!string.IsNullOrWhiteSpace(query))
                toolQuery = toolQuery.Where(a => a.Model.Contains(query, StringComparison.OrdinalIgnoreCase) ||
                                                 a.Manufacture.Contains(query, StringComparison.OrdinalIgnoreCase));

            var tools = await toolQuery.ToListAsync();

            var toolModels = new List<ToolModel>();
            foreach (var tool in tools)
                toolModels.Add(new ToolModel
                {
                    Tool = tool,
                    AssignedUser = _toolManager.GetUserAssignedToTool(tool)
                });

            return View(toolModels);
        }

        public async Task<IActionResult> Index(string query)
        {
            var permissions = User.GetUserPermissions();
            if (permissions.IsAdmin)
                return RedirectToAction("IndexAdmin", new { query });

            var toolQuery = _toolManager.GetTools();

            if (!string.IsNullOrWhiteSpace(query))
                toolQuery = toolQuery.Where(a =>
                                                a.Model.Contains(query, StringComparison.OrdinalIgnoreCase) ||
                                                a.Manufacture.Contains(query, StringComparison.OrdinalIgnoreCase));

            var tools = await toolQuery.ToListAsync();

            var toolModels = new List<ToolModel>();
            foreach (var tool in tools)
                toolModels.Add(new ToolModel
                {
                    Tool = tool,
                    AssignedUser = _toolManager.GetUserAssignedToTool(tool),
                    RequestEvents = _toolManager.GetAssignmentRequests(tool).Include(x => x.RequestingUser).ToList(),
                    RepairEvents = _toolManager.GetRepairRequests(tool).Include(x => x.ReportedByUser).ToList()
                });
            return View(toolModels);
        }

        [RoleAuthorizeConstruction(nameof(ConstructionSitePermissions.CanCreateTool))]
        public IActionResult Create()
        {
            var user = User.GetUserId();
            var userOptions = new List<SelectListItem>();
            if (user != null)
                userOptions = GetUserOptions()
                              .Select(x =>
                              {
                                  x.Selected = user.Id.ToString() == x.Value;
                                  return x;
                              })
                              .ToList();
            ToolCreateModel model = new ToolCreateModel
            {
                UserOptions = userOptions
            };
            return View(model);
        }

        [RoleAuthorizeConstruction(nameof(ConstructionSitePermissions.CanCreateTool))]
        [HttpPost]
        public IActionResult Create(ToolCreateModel createModel)
        {
            var tool = _toolManager.CreateTool(createModel.Model, createModel.Manufacture, createModel.Price);
            _organizationManager.AssignAssetToOrganization(tool, User.GetUserOrganizationId());
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var tool = await _toolManager.GetTools().GetEntityAsync(new ToolId(id));

            var assignedUser = _toolManager.GetUserAssignedToTool(tool);

            // You should not be able to access other tools than your own on less you may see all tools.
            if (!User.GetConstructionUserPermissions().CanSeeAllTools &&
                assignedUser != null &&
                assignedUser.Id != User.GetUserId().Id)
                return Unauthorized();

            var eventsByAsset = _eventManager.GetEventsByAsset(tool);

            var brokenEvents = _toolManager.GetRepairRequests(tool)
                                           .Include(x => x.ReportedByUser)
                                           .ToList();
            var model = new ToolDetailsModel
            {
                Id = tool.Id,
                Model = tool.Model,
                Manufacture = tool.Manufacture,
                Price = tool.Price,
                Events = eventsByAsset.ToList(),
                AssignedUser = assignedUser,
                CreatedOn = tool.CreatedOn,
                UpdatedOn = tool.UpdatedOn,
                BrokenEvents = brokenEvents
            };

            return View(model);
        }

        [RoleAuthorizeConstruction(nameof(ConstructionSitePermissions.CanDeleteTool))]
        public async Task<ActionResult> Delete(Guid id)
        {
            var toolId = new ToolId(id);
            var tool = await _toolManager.GetTools().GetEntityAsync(toolId);

            var assignedUser = _toolManager.GetUserAssignedToTool(tool);

            // You should not be able to access other tools than your own on less you may see all tools.
            if (!User.GetConstructionUserPermissions().CanSeeAllTools &&
                assignedUser != null &&
                assignedUser.Id != User.GetUserId().Id)
                return Unauthorized();

            _toolManager.RemoveTool(toolId);
            return RedirectToAction("Index");
        }

        public ViewResult Edit(Guid id)
        {
            ToolId toolId = new ToolId(id);

            var tool = _toolManager.GetTools().GetEntity(toolId);
            var user = _toolManager.GetUserAssignedToTool(toolId);
            var userOptions = new List<SelectListItem>();

            // If the tool is assigned a user
            if (user != null)
            {
                userOptions = GetUserOptions()
                              .Select(x =>
                              {
                                  x.Selected = user.Id.ToString() == x.Value;
                                  return x;
                              })
                              .ToList();
            }

            List<SelectListItem> projectOptions = _projectManager.GetProjectsByOrganization(User.GetUserOrganizationId())
                                                                 .Select(a => new SelectListItem(a.Name, a.Id.ToString(), a.Id == tool.ProjectId))
                                                                 .ToList();
            projectOptions.Add(new SelectListItem("-----", string.Empty, user == null));

            return View(new ToolEditModel
            {
                Model = tool.Model,
                Manufacture = tool.Manufacture,
                Price = tool.Price,
                ProjectOptions = projectOptions,
                UserOptions = userOptions
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ToolEditModel editModel)
        {
            if (!ModelState.IsValid)
                return View(editModel);

            var toolId = new ToolId(editModel.Id);
            var tool = _toolManager.GetTools().GetEntity(toolId);

            tool.Model = editModel.Model;
            tool.Manufacture = editModel.Manufacture;
            tool.Price = editModel.Price;
            tool.ProjectId = editModel.SelectedProjectId;
            var assignmentEvent = _eventManager.GetEventsByType<AssignmentEvent>().LastOrDefault(t => t.AssetId == toolId.Id);
            if (assignmentEvent != null && assignmentEvent.AssignedUserId == editModel.SelectedUserId)
            {
                _toolManager.EditTool(tool);
                return RedirectToAction("Index");
            }

            _assetManager.AssignAsset(toolId, new UserId(editModel.SelectedUserId));
            _toolManager.EditTool(tool);

            return RedirectToAction("Index");
        }

        public IActionResult TransferToMe(Guid id)
        {
            _toolManager.AssignToUser(new ToolId(id), User.GetUserId());

            return RedirectToAction("Index");
        }

        [RoleAuthorizeConstruction(nameof(ConstructionSitePermissions.CanReportTool))]
        public async Task<IActionResult> Report(Guid toolId, string reason)
        {
            var reportBrokenEvent = _toolManager.ReportTool(User.GetUserId(), new ToolId(toolId), reason);
            await _eventPublisher.PublishEventAsync(reportBrokenEvent);

            return RedirectToAction("Details", new { id = toolId });
        }

        [RoleAuthorizeConstruction(nameof(ConstructionSitePermissions.CanFixTool))]
        public async Task<IActionResult> ReportFixed(Guid eventId, Guid toolId)
        {
            var reportFixedEvent = _toolManager.FixTool(new EventId(eventId), User.GetUserId(), new ToolId(toolId));
            await _eventPublisher.PublishEventAsync(reportFixedEvent);

            return RedirectToAction("Details", new { id = toolId });
        }

        private List<SelectListItem> GetUserOptions()
        {
            var userOptions = _userManager.GetUsers()
                                          .Select(a => new SelectListItem(a.FullName, a.Id.ToString()))
                                          .ToList();

            return userOptions;
        }

        public async Task<IActionResult> ToolRequest(Guid id)
        {
            AssignmentRequestEvent assignmentRequestEvent = _toolManager.RequestTool(new ToolId(id), User.GetUserId());
            if (assignmentRequestEvent != null)
                await _eventPublisher.PublishEventAsync(assignmentRequestEvent);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Accept(Guid eventId, Guid toolId, Guid userId)
        {
            var assignmentConfirmationEvent =
                _toolManager.AcceptTool(new EventId(eventId), new ToolId(toolId), new UserId(userId));
            await _eventPublisher.PublishEventAsync(assignmentConfirmationEvent);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> AcceptResolution(Guid id)
        {
            return View(new ToolAcceptResolutionModel
            {
                PendingRequests = GetAssignmentRequests(id).ToList()
            });
        }

        public async Task<IActionResult> CancelToolRequest(Guid id)
        {
            _toolManager.CancelToolRequest(new EventId(id));
            return RedirectToAction("Index");
        }

        private IEnumerable<ToolRequestSelection> GetAssignmentRequests(Guid id)
        {
            var awdd = _toolManager.GetAssignmentRequests(new ToolId(id)).Include(x => x.RequestingUser).ToList();

            return _toolManager.GetAssignmentRequests(new ToolId(id))
                               .Include(x => x.RequestingUser)
                               .Select(x => new ToolRequestSelection
                               {
                                   Id = x.Id,
                                   Name = x.RequestingUser.FullName,
                                   AssignmentRequestEvent = x
                               });
        }
    }
}