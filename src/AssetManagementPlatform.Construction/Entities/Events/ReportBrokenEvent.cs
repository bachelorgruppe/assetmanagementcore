﻿using System;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.ConstructionModule.Entities.Events
{
    public class ReportBrokenEvent : Event
    {
        public string Reason { get; set; }
        public Guid ReportedByUserId { get; set; }
        public User ReportedByUser { get; set; }
    }
}