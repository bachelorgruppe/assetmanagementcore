﻿using System;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.ConstructionModule.Entities.Events
{
    public class ReportFixedEvent : Event
    {
        public Guid FixedByUserId { get; set; }
        public User FixedByUser { get; set; }
    }
}