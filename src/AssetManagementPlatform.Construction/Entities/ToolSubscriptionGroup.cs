﻿using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.ConstructionModule.Entities {
    public class ToolSubscriptionGroup : AssetGroup
    {
        public IEnumerable<Tool> Tools => Assets.Select(asset => asset.Asset).OfType<Tool>();
        public IEnumerable<User> Engineers => Assets.Select(asset => asset.Asset).OfType<User>();
    }
}