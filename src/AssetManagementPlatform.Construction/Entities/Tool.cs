﻿using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.ConstructionModule.Entities
{
    public class Tool : Asset
    {
        public string Model { get; set; }

        public string Manufacture { get; set; }

        public double? Price { get; set; }
    }
}