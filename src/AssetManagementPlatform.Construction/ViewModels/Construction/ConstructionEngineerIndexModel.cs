﻿using System.Collections.Generic;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Construction
{
    public class ConstructionEngineerIndexModel
    {
        public List<Entities.Tool> BrokenTools { get; set; }
    }
}