﻿using System;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Project
{
    public class ProjectEditModel : ProjectCreateModel
    {
        public Guid Id { get; set; }
    }
}