﻿namespace AssetManagementPlatform.ConstructionModule.ViewModels.Project
{
    public class ProjectCreateModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}