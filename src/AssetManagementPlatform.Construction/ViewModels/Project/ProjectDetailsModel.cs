﻿using System;
using System.Collections.Generic;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Project
{
    public class ProjectDetailsModel : ProjectCreateModel
    {
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public ICollection<Core.DB.Entities.User> Users { get; set; }
    }
}