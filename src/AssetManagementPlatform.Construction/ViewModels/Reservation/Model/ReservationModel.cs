﻿using AssetManagementPlatform.Core.DB.Entities.Events;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Reservation.Model
{
    public class ReservationModel
    {
        public ReservationEvent ReservationEvent { get; set; }
        public Entities.Tool Tool { get; set; }
    }
}
