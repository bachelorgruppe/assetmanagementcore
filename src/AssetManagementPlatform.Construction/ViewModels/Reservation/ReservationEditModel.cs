using System;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Reservation
{
    public class ReservationEditModel : ReservationCreateModel
    {
        public Guid Id { get; set; }
    }
}