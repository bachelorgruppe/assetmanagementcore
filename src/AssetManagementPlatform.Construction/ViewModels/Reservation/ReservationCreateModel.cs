using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Reservation
{
    public class ReservationCreateModel
    {
        public IList<SelectListItem> ReservedToOptions { get; set; }
        public IList<SelectListItem> ReservedAssetOptions { get; set; }

        public Guid ReservedToId { get; set; }
        public Guid ReservedAssetId { get; set; }

        [Required]
        public DateTime ReservationStartDate { get; set; }

        [Required]
        public DateTime ReservationEndDate { get; set; }
    }
}