using System.Collections.Generic;
using AssetManagementPlatform.ConstructionModule.ViewModels.Reservation.Model;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Reservation
{
    public class ReservationIndexModel
    {
        public ReservationIndexModel()
        {
            Reservations = new List<ReservationModel>();
        }

        public List<ReservationModel> Reservations { get; set; }
    }
}