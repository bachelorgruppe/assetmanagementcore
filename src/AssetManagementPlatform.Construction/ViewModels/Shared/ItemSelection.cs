﻿using System;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Shared
{
    public class ItemSelection
    {
        public bool IsSelected { set; get; }
        public string Name { set; get; }
        public Guid Id { set; get; }
    }
}
