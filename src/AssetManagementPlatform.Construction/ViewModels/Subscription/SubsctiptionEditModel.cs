﻿using System;
using System.Collections.Generic;
using AssetManagementPlatform.ConstructionModule.ViewModels.Shared;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Subscription
{
    public class SubsctiptionEditModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public IList<ItemSelection> SubscribedTools { get; set; }
        public IList<ItemSelection> SubscribedEngineers { get; set; }

    }
}