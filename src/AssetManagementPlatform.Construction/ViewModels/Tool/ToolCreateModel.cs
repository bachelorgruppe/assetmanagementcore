﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Tool
{
    public class ToolCreateModel
    {
        public string Model { get; set; }
        public string Manufacture { get; set; }
        public double? Price { get; set; }
        public IList<SelectListItem> UserOptions { get; set; }
        public Guid UserId { get; set; }


    }
}