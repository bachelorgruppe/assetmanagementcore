﻿using System;
using System.Collections.Generic;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Tool
{
    public class ToolAcceptResolutionModel
    {
        public Guid Id { get; set; }
        public IList<ToolRequestSelection> PendingRequests { get; set; }
    }
}
