﻿using System.Collections.Generic;
using AssetManagementPlatform.ConstructionModule.ViewModels.Tool.Model;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Tool
{
    public class ToolIndexModel
    {
        public ICollection<ToolModel> ToolModelList { get; set; }
        public ICollection<Entities.Tool> AssignedTools { get; set; }
    }
}