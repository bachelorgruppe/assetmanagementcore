﻿using System.Collections.Generic;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.DB.Entities.Events;
using JetBrains.Annotations;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Tool.Model
{
    public class ToolModel
    {
        [NotNull]
        public Entities.Tool Tool { get; set; }

        [CanBeNull]
        public Core.DB.Entities.User AssignedUser { get; set; }

       [NotNull]
       public ICollection<AssignmentRequestEvent> RequestEvents { get; set; }

       [NotNull]
       public ICollection<ReportBrokenEvent> RepairEvents { get; set; } 
       //TODO: Create an "TicketEvent" to inherit from
    }
}