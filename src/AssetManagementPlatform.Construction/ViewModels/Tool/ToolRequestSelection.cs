﻿using System;
using AssetManagementPlatform.Core.DB.Entities.Events;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Tool
{
    public class ToolRequestSelection
    {
        public AssignmentRequestEvent AssignmentRequestEvent { get; set; }
        public string Name { set; get; }
        public Guid Id { set; get; }
    }
}
