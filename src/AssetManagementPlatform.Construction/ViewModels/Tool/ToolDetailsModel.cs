﻿using System;
using System.Collections.Generic;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Tool
{
    public class ToolDetailsModel : ToolCreateModel
    {
        public Guid Id { get; set; }
        public Core.DB.Entities.User AssignedUser { get; set; }
        public ICollection<Event> Events { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public ICollection<ReportBrokenEvent> BrokenEvents { get; set; }
    }
}