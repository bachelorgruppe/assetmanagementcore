﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.Tool
{
    public class ToolEditModel : ToolCreateModel
    {
        public Guid Id { get; set; }

        public IList<SelectListItem> ProjectOptions { get; set; }

        [Required]
        public Guid SelectedUserId { get; set; }

        public Guid? SelectedProjectId { get; set; }
    }
}