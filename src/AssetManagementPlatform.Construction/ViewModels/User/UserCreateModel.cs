﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.User
{
    public class UserCreateModel
    {
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public Guid? ProjectId { get; set; }
        public IList<SelectListItem> ProjectOptions { get; set; }
    }
}