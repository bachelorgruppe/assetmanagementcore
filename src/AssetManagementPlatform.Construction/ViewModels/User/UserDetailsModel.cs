﻿using System;
using System.Collections.Generic;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.User
{
    public class UserDetailsModel : UserCreateModel
    {
        public string FullName => $"{Firstname} {LastName}";
        public ICollection<Entities.Tool> Tools { get; set; }
        public Core.DB.Entities.Project Project { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}