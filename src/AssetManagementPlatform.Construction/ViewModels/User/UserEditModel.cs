﻿using System;

namespace AssetManagementPlatform.ConstructionModule.ViewModels.User
{
    public class UserEditModel : UserCreateModel
    {
        public Guid Id { get; set; }
    }
}