﻿using System;
using System.Collections.Generic;
using System.Reflection;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.SavingManagers;
using Bogus;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.ConstructionModule
{
    public class DbConstructionSeeder : IDbContextSeed
    {
        private readonly ConstructionContext _db;
        private readonly SavingProjectManager _projectManager;
        private readonly SavingToolSubscriptionManager _subscriptionManager;
        private readonly SavingOrganizationManager _organizationManager;
        private readonly SavingToolManager _toolManager;


        public DbConstructionSeeder(IServiceProvider serviceProvider)
        {
            Randomizer.Seed = new Random(8675309);

            _toolManager = serviceProvider.GetRequiredService<SavingToolManager>();
            _organizationManager = serviceProvider.GetRequiredService<SavingOrganizationManager>();
            _subscriptionManager = serviceProvider.GetRequiredService<SavingToolSubscriptionManager>();
            _projectManager = serviceProvider.GetRequiredService<SavingProjectManager>();
            _db = serviceProvider.GetRequiredService<ConstructionContext>();

        }

        public void Seed()
        {
            ToolSubscriptionGroup toolSubscriptionGroup1 = _subscriptionManager.Create("Tier 1 Tool group");
            

            List<Location> address = GetLocations().Generate(7);

            // Setup Organizations
            List<Organization> organizations = GetOrganizations().Generate(3);
            organizations[0].Name = "Bob the Builder´s Building Firm";
            organizations[0].Location = address[0];
            SetId(typeof(Organization), organizations[0], new Guid("391ab491-4233-4a8f-b9c6-4719dbb202d9"));

            organizations[1].Name = "Chris the Constructor´s Construction Firm";
            organizations[1].Location = address[1];
            SetId(typeof(Organization), organizations[1], new Guid("9449e933-05e0-4c5c-bee4-15e6b73be3e8"));

            organizations[2].Name = "BOB the Constructor´s Construction Firm";
            organizations[2].Location = address[2];
            SetId(typeof(Organization), organizations[2], new Guid("c7f9d850-9383-4142-b782-d7d4c9e7a8f0"));

            _db.Organizations.AddRange(organizations);
            _db.SaveChanges();

            // Setup Projects
            List<Project> projects = GetProjects().Generate(4);

            // Bob the builder Org
            projects[0].Name = "Hotel 1st floor";
            projects[0].Location = address[2];

            projects[1].Name = "Hotel 2nd floor";
            projects[1].Location = address[3];

            projects[2].Name = "Hotel 3nd floor";
            projects[2].Location = address[4];

            // Chris the Constructor Org
            projects[3].Name = "House 34";
            projects[3].Location = address[5];

            _db.Projects.AddRange(projects);
            _db.SaveChanges();

            _organizationManager.AssignProjectToOrganization(projects[0], organizations[0]);
            _organizationManager.AssignProjectToOrganization(projects[1], organizations[0]);
            _organizationManager.AssignProjectToOrganization(projects[2], organizations[0]);

            _organizationManager.AssignProjectToOrganization(projects[3], organizations[1]);

            // Setup Assets
            List<Tool> tools = GetTools().Generate(5);

            _db.AddRange(tools);
            _db.SaveChanges();

            // Setup Users
            List<User> users = GetUsers().Generate(3);
            SetId(typeof(User).BaseType, users[0], new Guid("292fa3ef-8617-4f30-ae30-5571fa0488e1"));
            SetId(typeof(User).BaseType, users[1], new Guid("c7f9d850-9383-4142-b782-d7d4c9e7a8f0"));
            SetId(typeof(User).BaseType, users[2], new Guid("ac7a7638-dc99-4ad1-be16-e56cfae4ca42"));

            users[0].FirstName = "Manager";
            users[1].FirstName = "Worker";
            users[2].FirstName = "Engineer";

            _db.Users.AddRange(users);
            _db.SaveChanges();

            _subscriptionManager.Subscribe(toolSubscriptionGroup1, users[2]);
            _subscriptionManager.AddTool(toolSubscriptionGroup1, tools[2]);

            //Assign users to organizations
            _organizationManager.AssignAssetToOrganization(users[0], organizations[0]);
            _organizationManager.AssignAssetToOrganization(users[1], organizations[0]);
            _organizationManager.AssignAssetToOrganization(users[2], organizations[1]);

            //Assign users to projects
            _projectManager.AssignAssetToProject(projects[0], users[0]);
            _projectManager.AssignAssetToProject(projects[1], users[0]);
            _projectManager.AssignAssetToProject(projects[2], users[0]);
            _projectManager.AssignAssetToProject(projects[0], users[1]);
            _projectManager.AssignAssetToProject(projects[1], users[1]);

            _projectManager.AssignAssetToProject(projects[3], users[2]);

            _organizationManager.AssignAssetToOrganization(tools[0], organizations[0]);
            _organizationManager.AssignAssetToOrganization(tools[1], organizations[0]);
            _organizationManager.AssignAssetToOrganization(tools[2], organizations[0]);

            _organizationManager.AssignAssetToOrganization(tools[3], organizations[1]);
            _organizationManager.AssignAssetToOrganization(tools[4], organizations[1]);

            // Assign tools to projects
            _projectManager.AssignAssetToProject(projects[0], tools[0]);
            _projectManager.AssignAssetToProject(projects[1], tools[1]);
            _projectManager.AssignAssetToProject(projects[2], tools[2]);

            _projectManager.AssignAssetToProject(projects[3], tools[3]);
            _projectManager.AssignAssetToProject(projects[3], tools[4]);

            //Assign tools
            _toolManager.AssignToUser(tools[0], users[0]);
            _toolManager.AssignToUser(tools[1], users[0]);
            _toolManager.AssignToUser(tools[2], users[1]);

            _toolManager.AssignToUser(tools[3], users[2]);
            _toolManager.AssignToUser(tools[4], users[2]);
        }

        private void SetId(Type type, object theObject, Guid guid)
        {
            FieldInfo field = type.GetField("<Id>k__BackingField", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(theObject, guid);
        }

        private Faker<Location> GetLocations()
        {
            return new Faker<Location>()
                   .StrictMode(true)
                   .RuleFor(o => o.Address, f => f.Address.FullAddress());
        }

        private Faker<Organization> GetOrganizations()
        {
            return new Faker<Organization>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.Name, f => f.Company.CompanyName())
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Projects, f => new List<Project>())
                   .RuleFor(o => o.Assets, f => new List<Asset>())
                   .RuleFor(o => o.Location, f => null);
        }

        private Faker<Project> GetProjects()
        {
            return new Faker<Project>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.Name, f => f.Name.JobTitle())
                   .RuleFor(o => o.Location, f => null)
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null)
                   .RuleFor(o => o.StartOn, f => f.Date.Recent(50))
                   .RuleFor(o => o.EndOn, f => f.Date.Recent(30))
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Assets, f => new List<Asset>());
        }

        private Faker<User> GetUsers()
        {
            return new Faker<User>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.FirstName, f => f.Name.FirstName())
                   .RuleFor(o => o.LastName, f => f.Name.LastName())
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Project, f => null)
                   .RuleFor(o => o.ProjectId, f => null)
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null);
        }

        private Faker<AssignmentEvent> GetEvents()
        {
            return new Faker<AssignmentEvent>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.Asset, f => null)
                   .RuleFor(o => o.AssetId, f => f.Random.Guid())
                   .RuleFor(o => o.AssignedUser, f => null)
                   .RuleFor(o => o.AssignedUserId, f => f.Random.Guid())
                   .RuleFor(o => o.Type, f => null);
        }

        private Faker<Tool> GetTools()
        {
            return new Faker<Tool>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.Project, f => null)
                   .RuleFor(o => o.ProjectId, f => null)
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Model, f => f.Vehicle.Model())
                   .RuleFor(o => o.Manufacture, f => f.Vehicle.Manufacturer())
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null)
                   .RuleFor(o => o.Price, f => null)
                   .RuleFor(o => o.Events, f => new List<Event>())
                   .RuleFor(o => o.AssetGroups, f => null);
        }

        private Faker<Asset> GetAssets()
        {
            return new Faker<Asset>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.Project, f => null)
                   .RuleFor(o => o.ProjectId, f => null)
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null)
                   .RuleFor(o => o.Events, f => new List<Event>())
                   .RuleFor(o => o.AssetGroups, f => null);
        }
    }
}