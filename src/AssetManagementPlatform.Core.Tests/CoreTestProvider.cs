﻿using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Shared.Tests;

namespace AssetManagementPlatform.Core.Tests
{
    public class CoreTestProvider : TestDependencyBuilder<DataContext>
    {
        public CoreTestProvider(bool disableSeededDb = true) : base(disableSeededDb) { }
    }
}