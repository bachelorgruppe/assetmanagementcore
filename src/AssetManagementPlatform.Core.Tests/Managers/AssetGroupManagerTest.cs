using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Core.Tests.Managers
{
    public class AssetGroupManagerTest
    {
        [Fact]
        public void CreateDeleteTest()
        {
            CoreTestProvider provider = new CoreTestProvider();

            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                manager.CreateAssetGroup("TestOrg");
                manager.CreateAssetGroup("TestOrg1");
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                List<AssetGroup> assetGroups = manager.GetAssetGroups().ToList();

                Assert.Equal(2, assetGroups.Count);
                Assert.Equal(1, assetGroups.Count(a => a.Name == "TestOrg"));
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                AssetGroup assetGroup = manager.GetAssetGroups().Single(a => a.Name == "TestOrg");
                manager.RemoveAssetGroup(assetGroup);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                List<AssetGroup> assetGroups = manager.GetAssetGroups().ToList();

                Assert.Single(assetGroups);
            }

            provider.Dispose();
        }

        [Fact]
        public void Test1()
        {
            CoreTestProvider provider = new CoreTestProvider();

            AssetGroup assetGroup;
            Asset asset;
            AssetGroup assetGroup2;
            Asset asset2;
            Project project;
            using (IServiceScope scope = provider.GetScope())
            {
                using DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();

                project = new Project { Name = "TestProject" };

                context.Projects.Add(project);

                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                assetGroup = manager.CreateAssetGroup("TestGroup");

                asset = new Asset();
                context.Assets.Add(asset);

                assetGroup2 = manager.CreateAssetGroup("TestGroup2", project);

                asset2 = new Asset();
                context.Assets.Add(asset2);

                manager.AddAssetToGroup(assetGroup, asset);
                manager.AddAssetToGroup(assetGroup2, asset);
                manager.AddAssetToGroup(assetGroup2, asset2);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                List<AssetGroup> assetGroups = manager.GetAssetGroups().ToList();
                Assert.Equal(2, assetGroups.Count);
                Assert.Equal("TestGroup", assetGroups[0].Name);
                Assert.Equal("TestGroup2", assetGroups[1].Name);
                Assert.Equal(project.Id, assetGroups[1].ProjectId);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                Asset single = manager.GetAssetsInGroup(assetGroup).Single();

                Assert.Equal(asset.Id, single.Id);

                AssetGroup group1 = manager.GetGroupsFromAsset(asset2).ToList().Single();

                Assert.Equal(assetGroup2.Id, group1.Id);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                manager.RemoveAssetFromGroup(assetGroup, asset);

                int groupCount = manager.GetGroupsFromAsset(asset).Count();

                Assert.Equal(1, groupCount);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetGroupManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetGroupManager>();
                manager.RemoveAssetGroup(assetGroup2);

                int groupCount = manager.GetGroupsFromAsset(asset2).Count();

                Assert.Equal(0, groupCount);
            }

            provider.Dispose();
        }
    }
}