using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Core.Tests.Managers
{
    public class OrganizationManagerTest
    {
        [Fact]
        public void Test1()
        {
            CoreTestProvider provider = new CoreTestProvider();

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                organizationManager.CreateOrganization("TestOrg");
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                List<Organization> organizations = organizationManager.GetOrganizations().ToList();

                Assert.Single(organizations);
                Assert.Equal("TestOrg", organizations.Single().Name);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                Organization organization = organizationManager.GetOrganizations().Single();
                organizationManager.RemoveOrganization(organization);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                List<Organization> organizations = organizationManager.GetOrganizations().ToList();

                Assert.Empty(organizations);
            }

            provider.Dispose();
        }
    }
}