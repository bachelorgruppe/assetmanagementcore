﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Core.Tests.Managers
{
    public class ReservationManagerTest
    {
        [Fact]
        public void CancelReservation()
        {
            CoreTestProvider provider = new CoreTestProvider();

            Asset asset;
            User user;
            using (IServiceScope scope = provider.GetScope())
            {
                using DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();

                user = new User { FirstName = "test person" };
                context.Users.Add(user);

                asset = new Asset();
                context.Assets.Add(asset);
            }

            ReservationEvent reservationResult1;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();

                reservationResult1 = manager.CreateReservation(new DateTime(), new DateTime(), new AssetId(user.Id),
                                                               new AssetId(asset.Id));
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                Assert.True(reservationResult1.IsActive);
                manager.CancelReservation(new EventId(reservationResult1.Id));
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                ReservationEvent result = manager.GetReservationEvents().Single();
                Assert.False(result.IsActive);
            }

            provider.Dispose();
        }

        [Fact]
        public void CreateReservation()
        {
            CoreTestProvider provider = new CoreTestProvider();

            Asset asset;
            Asset user;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingAssetManager manager = scope.ServiceProvider.GetRequiredService<SavingAssetManager>();

                user = new User() { FirstName = "test person" };
                asset = new Asset();

                manager.CreateAsset(user);
                manager.CreateAsset(asset);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                Assert.Empty(manager.GetReservationEvents());
                manager.CreateReservation(new DateTime(), new DateTime(), new AssetId(user.Id), new AssetId(asset.Id));
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                Assert.NotEmpty(manager.GetReservationEvents());
                User reservedTo = (User)manager.GetReservationEvents().Include(r => r.ReservedTo).Single().ReservedTo;
                Assert.Equal("test person",
                             reservedTo.FirstName);
                Assert.Equal(asset.Id,
                             manager.GetReservationEvents().Include(a => a.ReservedAsset).Single().ReservedAsset.Id);
                Assert.IsType<DateTime>(manager.GetReservationEvents().Single().ReservationStartDate);
                Assert.IsType<DateTime>(manager.GetReservationEvents().Single().ReservationEndDate);
            }

            provider.Dispose();
        }

        [Fact]
        public void EditReservation()
        {
            CoreTestProvider provider = new CoreTestProvider();

            Asset asset;
            User user;
            User user2;
            using (IServiceScope scope = provider.GetScope())
            {
                using DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();

                user = new User { FirstName = "test person" };
                context.Users.Add(user);

                user2 = new User { FirstName = "new test person" };
                context.Users.Add(user);

                asset = new Asset();
                context.Assets.Add(asset);
            }

            ReservationEvent reservationResult1;
            ReservationEvent reservationResult2;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                reservationResult1 = manager.CreateReservation(new DateTime(), new DateTime(), new AssetId(user.Id),
                                                               new AssetId(asset.Id));
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                IQueryable<ReservationEvent> events = manager.GetReservationEvents();
                Assert.Equal(reservationResult1.Id, events.Single(a => a.ReservedToId == user.Id).Id);

                reservationResult1.ReservedToId = user2.Id;
                reservationResult2 = manager.EditReservation(reservationResult1);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                IQueryable<ReservationEvent> events = manager.GetReservationEvents();

                Assert.False(events.Single(r => r.Id == reservationResult1.Id).IsActive);
                Assert.True(events.Single(r => r.Id == reservationResult2.Id).IsActive);

                Guid blyat = events.Single(r => r.ReservedToId == user.Id && !r.IsActive).Id;
                Guid actual = events.Single(r => r.ReservedToId == user2.Id && r.IsActive).Id;
                Assert.Equal(reservationResult2.Id, actual);
                Assert.Equal(reservationResult1.Id, blyat);
            }

            provider.Dispose();
        }

        [Fact]
        public void GetReservationEvents()
        {
            CoreTestProvider provider = new CoreTestProvider();

            Asset asset;
            User user;
            using (IServiceScope scope = provider.GetScope())
            {
                using DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();

                user = new User { FirstName = "test person" };
                context.Users.Add(user);

                asset = new Asset();
                context.Assets.Add(asset);
            }

            ReservationEvent reservationResult;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                Assert.Empty(manager.GetReservationEvents());
                reservationResult = manager.CreateReservation(new DateTime(), new DateTime(), new AssetId(user.Id),
                                                              new AssetId(asset.Id));
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager manager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                Assert.Equal(reservationResult.Id, manager.GetReservationEvents().Single().Id);
            }

            provider.Dispose();
        }
    }
}