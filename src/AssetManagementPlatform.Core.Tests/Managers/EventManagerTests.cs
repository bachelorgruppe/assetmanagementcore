﻿using System.Linq;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Core.Tests.Managers
{
    public class EventManagerTests
    {
        [Fact]
        public void RetrieveLastestAssignmentEventTest()
        {
            CoreTestProvider provider = new CoreTestProvider();

            Asset testAsset = new Asset();
            User testUser1 = new User
            {
                FirstName = "Rasmus",
                LastName = "Raskemus"
            };
            User testUser2 = new User
            {
                FirstName = "Lasse",
                LastName = "Luskebuks"
            };

            using (IServiceScope scope = provider.GetScope())
            {
                using DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();
                context.Users.Add(testUser1);
                context.Users.Add(testUser2);
                context.Assets.Add(testAsset);
                context.SaveChanges();
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                eventManager.CreateEvent(testAsset, new AssignmentEvent
                {
                    AssignedUserId = testUser1.Id
                });

                AssignmentEvent testEvent = eventManager.GetEventsByType<AssignmentEvent>(testAsset).Include(a => a.AssignedUser).First();

                Assert.Equal(((User)testEvent.AssignedUser).FullName, testUser1.FullName);
            }

            provider.Dispose();
        }

        [Fact]
        public void RetrieveListOfAllAssigmentsTest()
        {
            CoreTestProvider provider = new CoreTestProvider();

            Asset testAsset1 = new Asset();
            Asset testAsset2 = new Asset();
         

            User testUser1 = new User
            {
                FirstName = "Rasmus",
                LastName = "Raskemus"
            };
            User testUser2 = new User
            {
                FirstName = "Lasse",
                LastName = "Luskebuks"
            };

            using (IServiceScope scope = provider.GetScope())
            {
                using DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();
                context.Users.Add(testUser1);
                context.Users.Add(testUser2);
                context.Assets.Add(testAsset1);
                context.Assets.Add(testAsset2);
                context.SaveChanges();
            }

            // Creates another assigment event
            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                eventManager.CreateEvent(testAsset1,new AssignmentEvent
                {
                    AssignedUserId = testUser1.Id
                });
                eventManager.CreateEvent(testAsset1, new AssignmentEvent
                {
                    AssignedUserId = testUser2.Id
                });
                eventManager.CreateEvent(testAsset2, new AssignmentEvent
                {
                    AssignedUserId = testUser1.Id
                });
            }

            // Tries to retrieve assigment event
            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();

                IIncludableQueryable<AssignmentEvent, Asset> eventList1 = eventManager.GetEventsByType<AssignmentEvent>(testAsset1).Include(a => a.AssignedUser);

                // Does the list of events for testAsset1, contain an assignment to testUser1?
                Assert.Contains(eventList1, e => ((User)e.AssignedUser).FullName == testUser1.FullName);
                Assert.Contains(eventList1, e => ((User)e.AssignedUser).FullName == testUser2.FullName);

                IIncludableQueryable<AssignmentEvent, Asset> eventList2 = eventManager.GetEventsByType<AssignmentEvent>(testAsset2).Include(a => a.AssignedUser);

                Assert.Contains(eventList2, e => ((User)e.AssignedUser).FullName == testUser1.FullName);
            }

            provider.Dispose();
        }
    }
}