using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Core.Tests.Managers
{
    public class ProjectManagerTest
    {
        [Fact]
        public void Test1()
        {
            CoreTestProvider provider = new CoreTestProvider();

            Organization organization;
            Asset asset;
            AssetGroup assetGroup;
            using (IServiceScope scope = provider.GetScope())
            {
                using DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();

                organization = new Organization { Name = "TestOrg" };
                context.Organizations.Add(organization);

                asset = new Asset();
                context.Assets.Add(asset);

                assetGroup = new AssetGroup { Name = "TestGroup" };
                context.AssetGroups.Add(assetGroup);

                Location location = new Location
                {
                    Address = "Test address"
                };

                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                Project project = projectManager.CreateProject("TestProject", location, organization);

                projectManager.AssignAssetToProject(project, asset);
                projectManager.AssignAssetGroupToProject(project, assetGroup);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                Project project = projectManager.GetProjects().Include(p => p.Assets).Include(p => p.AssetGroups).Single();

                Assert.Equal("TestProject", project.Name);
                Assert.Equal(organization.Id, project.OrganizationId.Value);

                Assert.Equal(asset.Id, project.Assets.Single().Id);
                Assert.Equal(assetGroup.Id, project.AssetGroups.Single().Id);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();

                Project project = projectManager.GetProjects().Single();
                projectManager.RemoveAssetFromProject(project, asset);
                projectManager.RemoveAssetGroupFromProject(project, assetGroup);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();

                Project project = projectManager.GetProjects().Include(p => p.Assets).Include(p => p.AssetGroups).Single();

                Assert.Empty(project.Assets);
                Assert.Empty(project.AssetGroups);

                projectManager.RemoveProject(project);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                List<Project> projects = projectManager.GetProjects().ToList();

                Assert.Empty(projects);
            }

            provider.Dispose();
        }
    }
}