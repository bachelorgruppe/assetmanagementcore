using AssetManagementPlatform.Core.Authorization.Permissions;
using Xunit;

namespace AssetManagementPlatform.Core.Tests
{
    public class PermissionTest
    {
        [Fact]
        public void PermissionInputFromNamesTest()
        {
            BaseSitePermissions baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message));

            Assert.True(baseSitePermissions.Message);
            Assert.False(baseSitePermissions.MessageRead);
            Assert.False(baseSitePermissions.MessageSend);
        }

        [Fact]
        public void PermissionInputTest()
        {
            BaseSitePermissions baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionString<BaseSitePermissions>("100");

            Assert.True(baseSitePermissions.Message);
            Assert.False(baseSitePermissions.MessageRead);
            Assert.False(baseSitePermissions.MessageSend);
        }

        [Fact]
        public void PermissionInputTest2()
        {
            BaseSitePermissions baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionString<BaseSitePermissions>("1000000000");

            Assert.True(baseSitePermissions.Message);
            Assert.False(baseSitePermissions.MessageRead);
            Assert.False(baseSitePermissions.MessageSend);
        }

        [Fact]
        public void PermissionOutputTest()
        {
            BaseSitePermissions baseSitePermissions = new BaseSitePermissions();
            baseSitePermissions.Message = true;
            string permissionsAsString = baseSitePermissions.GetPermissionsAsString();
            Assert.Equal('1', permissionsAsString[0]);
            Assert.Equal('0', permissionsAsString[1]);
            Assert.Equal('0', permissionsAsString[2]);
        }

        [Fact]
        public void PermissionsExstensionsTest()
        {
            BaseSitePermissions baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message));

            Assert.True(baseSitePermissions.HasAllPermissions(nameof(BaseSitePermissions.Message)));

            baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message));

            Assert.False(baseSitePermissions.HasAllPermissions(nameof(BaseSitePermissions.Message), nameof(BaseSitePermissions.MessageSend)));

            baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message), nameof(BaseSitePermissions.MessageSend));

            Assert.True(baseSitePermissions.HasAllPermissions(nameof(BaseSitePermissions.Message)));
        }

        [Fact]
        public void PermissionsTest()
        {
            BaseSitePermissions baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message));
            BaseSitePermissions baseSitePermissions2 = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message));

            Assert.True(baseSitePermissions.HasAllPermissions(baseSitePermissions2));

            baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message));
            baseSitePermissions2 = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message), nameof(BaseSitePermissions.MessageSend));

            Assert.False(baseSitePermissions.HasAllPermissions(baseSitePermissions2));

            baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message), nameof(BaseSitePermissions.MessageSend));
            baseSitePermissions2 = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(nameof(BaseSitePermissions.Message));

            Assert.True(baseSitePermissions.HasAllPermissions(baseSitePermissions2));
        }
    }
}