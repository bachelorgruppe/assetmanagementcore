﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AssetManagementPlatform.Core.Web.Navbar
{
    public interface INavBarService
    {
        [NotNull]
        IHtmlContent GetHtml(IHtmlHelper<object> html);
    }
}