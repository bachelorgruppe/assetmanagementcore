﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AssetManagementPlatform.Core.Web.Navbar
{
    public class DefaultNavBarService : INavBarService
    {
        private readonly IList<NavBarItem> _items;

        public DefaultNavBarService(IEnumerable<NavBarItem> items)
        {
            _items = items.ToList();
        }

        public IHtmlContent GetHtml(IHtmlHelper<dynamic> html)
        {
            bool first = true;
            IHtmlContentBuilder tagBuilder = new HtmlContentBuilder();
            foreach (NavBarItem navBarItem in _items)
            {
                IHtmlContent htmlContent = html.ActionLink(navBarItem.Text, navBarItem.Action, navBarItem.Controller, null, new { @class = "nav-link text-dark" });

                TagBuilder nav = new TagBuilder("li");
                nav.AddCssClass("nav-item");

                if (navBarItem.FloatRight && first)
                {
                    first = false;
                    nav.AddCssClass("ml-auto");
                }

                nav.InnerHtml.AppendHtml(htmlContent);

                tagBuilder = tagBuilder.AppendHtml(nav);
            }

            return tagBuilder;
        }
    }
}