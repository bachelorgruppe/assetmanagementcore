﻿using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Web.Navbar
{
    public class NavBarItem
    {
        public NavBarItem(string text, [AspMvcController]string controller, [AspMvcAction]string action, bool floatRight = false)
        {
            Text = text;
            Controller = controller;
            Action = action;
            FloatRight = floatRight;
        }

        public string Text { get; }
        public string Controller { get; }
        public string Action { get; }
        public bool FloatRight { get; }
    }
}