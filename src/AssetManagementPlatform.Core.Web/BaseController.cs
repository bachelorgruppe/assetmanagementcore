﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.Core.Web
{
    [Authorize]
    public class BaseController : Controller { }
}