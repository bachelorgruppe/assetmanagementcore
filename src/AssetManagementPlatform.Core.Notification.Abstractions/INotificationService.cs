﻿using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;
using System.Threading.Tasks;

namespace AssetManagementPlatform.Core.Notification.Abstractions
{
    [PublicAPI]
    public interface INotificationService
    {
        void SendNotification([NotNull]UserId userId, string eventType, object data);
        Task SendNotificationAsync([NotNull]UserId userId, string eventType, object data);
        void SendGroupNotification([NotNull]AssetGroupId assetGroupId, string eventType, object data);
        Task SendGroupNotificationAsync([NotNull]AssetGroupId assetGroupId, string eventType, object data);
    }
}