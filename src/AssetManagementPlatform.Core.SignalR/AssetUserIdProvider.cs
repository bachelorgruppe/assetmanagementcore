﻿using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.AspNetCore.SignalR;

namespace AssetManagementPlatform.Core.SignalR
{
    public class AssetUserIdProvider : IUserIdProvider
    {
        public string GetUserId(HubConnectionContext connection)
        {
            if (connection.User.TryGetUserId(out UserId userId))
                return userId.Id.ToString();

            return "DefaultUser";
        }
    }
}