﻿using AssetManagementPlatform.Core.Notification.Abstractions;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace AssetManagementPlatform.Core.SignalR
{
    public class SignalRNotificationService : INotificationService
    {
        private readonly IHubContext<SignalRNotificationHub> _hubContext;
        private readonly ILogger<SignalRNotificationService> _logger;

        public SignalRNotificationService(IHubContext<SignalRNotificationHub> hubContext, ILogger<SignalRNotificationService> logger)
        {
            _hubContext = hubContext;
            _logger = logger;
        }

        public void SendNotification(UserId userId, string eventType, object data)
        {
            SendNotificationAsync(userId, eventType, data).GetAwaiter().GetResult();
        }

        public async Task SendNotificationAsync(UserId userId, string eventType, object data)
        {
            _logger.LogDebug("Sending notification to {UserId}", userId.Id);
            await _hubContext.Clients.User(userId.Id.ToString()).SendAsync("NotificationUpdate", eventType, data);
        }

        public void SendGroupNotification(AssetGroupId assetGroupId, string eventType, object data)
        {
            SendGroupNotificationAsync(assetGroupId, eventType, data).GetAwaiter().GetResult();
        }

        public async Task SendGroupNotificationAsync(AssetGroupId assetGroupId, string eventType, object data)
        {
            await _hubContext.Clients.Group(assetGroupId.Id.ToString()).SendAsync("NotificationUpdate", eventType, data);
        }
    }
}