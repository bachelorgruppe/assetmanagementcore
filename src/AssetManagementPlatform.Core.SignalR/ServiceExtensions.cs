﻿using AssetManagementPlatform.Core.Notification.Abstractions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.Core.SignalR
{
    public static class ServiceExtensions
    {
        public static void AddSignalRSetup(this IServiceCollection services)
        {
            services.AddSingleton<IUserIdProvider, AssetUserIdProvider>();
            services.AddScoped<INotificationService, SignalRNotificationService>();

            services.AddSignalR();
        }

        public static void AddSignalRSetup(this IApplicationBuilder app)
        {
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                                             "default",
                                             "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapHub<SignalRNotificationHub>("/notifications");
            });
        }
    }
}