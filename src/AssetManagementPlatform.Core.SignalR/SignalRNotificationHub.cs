﻿using System.Linq;
using AssetManagementPlatform.Core.DB;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.AspNetCore.SignalR;

namespace AssetManagementPlatform.Core.SignalR
{
    public class SignalRNotificationHub : Hub
    {
        private readonly DataContext _context;

        public SignalRNotificationHub(DataContext context)
        {
            _context = context;
        }

        public override Task OnConnectedAsync()
        {
            if (!Context.User.TryGetUserId(out UserId userId))
                return base.OnConnectedAsync();

            foreach (AssetGroup assetGroup in _context.AssetGroupAssets.Where(asset => asset.AssetId == userId.Id).Select(asset => asset.AssetGroup))
            {
                Groups.AddToGroupAsync(Context.ConnectionId, assetGroup.Id.ToString());
            }

            return base.OnConnectedAsync();
        }
    }
}