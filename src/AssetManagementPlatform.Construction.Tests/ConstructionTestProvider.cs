﻿using AssetManagementPlatform.ConstructionModule;
using AssetManagementPlatform.ConstructionModule.Code.EventHandlers;
using AssetManagementPlatform.ConstructionModule.Code.Interfaces;
using AssetManagementPlatform.ConstructionModule.Code.Managers;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Controllers;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.Notification.Abstractions;
using AssetManagementPlatform.Frontend.Controllers;
using AssetManagementPlatform.Shared.Tests;
using AssetManagementPlatform.Shared.Tests.TestHelpers;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.Construction.Tests
{
    public class ConstructionTestProvider : TestDependencyBuilder<ConstructionContext>
    {
        public ConstructionTestProvider(bool disableSeededDb = true) : base(disableSeededDb) { }

        protected override void SetupServices(ServiceCollection services)
        {
            base.SetupServices(services);
            services.AddScoped<IToolManager, ToolManager>();
            services.AddScoped<SavingToolManager>();

            services.AddScoped<IToolSubscriptionManager, ToolSubscriptionManager>();
            services.AddScoped<SavingToolSubscriptionManager>();

            services.AddScoped<ConstructionController>();
            services.AddScoped<OrganizationController>();
            services.AddScoped<ReservationController>();
            services.AddScoped<ProjectController>();
            services.AddScoped<ToolController>();
            services.AddScoped<UserController>();

            services.AddSingleton<INotificationService, MockNotificationService>();

            services.AddEventBus(builder => builder.AddInMemoryEventBus(subscriber =>
            {
                subscriber.Subscribe<ReportFixedEvent, ReportFixedEventHandler>();
                subscriber.Subscribe<ReportBrokenEvent, ReportBrokenEventHandler>();
                subscriber.Subscribe<AssignmentRequestEvent, AssignmentRequestEventHandler>();
                subscriber.Subscribe<AssignmentConfirmationEvent, AssignmentConfirmationEventHandler>();
            }));
        }
    }
}