using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.ConstructionModule;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Construction.Tests.Managers
{
    public class ToolManagerTests
    {
        [Fact]
        public void AssignToolToUserTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool = new Tool
            {
                Model = "Den store hammer",
                Manufacture = "Gerts Hammerfabrik"
            };

            User testUser1 = new User
            {
                FirstName = "Rasmus",
                LastName = "Raskemus"
            };

            User testUser2 = new User
            {
                FirstName = "Lasse",
                LastName = "Luskebuks"
            };

            using (IServiceScope scope = provider.GetScope())
            {
                using ConstructionContext context = scope.ServiceProvider.GetRequiredService<ConstructionContext>();

                context.Users.Add(testUser1);
                context.Users.Add(testUser2);
                context.Tools.Add(testTool);
                context.SaveChanges();
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                toolManager.AssignToUser(testTool, testUser1);
                toolManager.AssignToUser(testTool, testUser2);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();

                AssignmentEvent testEvent = eventManager.GetEventsByType<AssignmentEvent>(testTool)
                    .Include(a => a.AssignedUser).First();

                Assert.Equal(((User) testEvent.AssignedUser).FullName, testUser2.FullName);
            }

            provider.Dispose();
        }


        [Fact]
        public void CreateToolTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            const string modelTestString = "Hammer";
            const string manufactureTestString = "Black and deck";
            const int priceTestDecimal = 900;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                Tool newTestTool = toolManager.CreateTool(modelTestString, manufactureTestString, priceTestDecimal);

                Assert.Equal(newTestTool.Model, modelTestString);
                Assert.Equal(newTestTool.Manufacture, manufactureTestString);
                Assert.Equal(newTestTool.Price, priceTestDecimal);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                using ConstructionContext context = scope.ServiceProvider.GetRequiredService<ConstructionContext>();
                Tool newTestTool = context.Tools.First();
                Assert.Equal(newTestTool.Model, modelTestString);
                Assert.Equal(newTestTool.Manufacture, manufactureTestString);
                Assert.Equal(newTestTool.Price, priceTestDecimal);
            }

            provider.Dispose();
        }

        [Fact]
        public void EditToolTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            const string modelTestString1 = "Hammer";
            const string manufactureTestString1 = "Black and deck";
            const int priceTestDecimal1 = 900;

            const string modelTestString2 = "Skruetrękker";
            const string manufactureTestString2 = "Gerts Hammerfabrik";
            const int priceTestDecimal2 = 750;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                Tool newTestTool = toolManager.CreateTool(modelTestString1, manufactureTestString1, priceTestDecimal1);

                Assert.Equal(newTestTool.Model, modelTestString1);
                Assert.Equal(newTestTool.Manufacture, manufactureTestString1);
                Assert.Equal(newTestTool.Price, priceTestDecimal1);

                newTestTool.Model = modelTestString2;
                newTestTool.Manufacture = manufactureTestString2;
                newTestTool.Price = priceTestDecimal2;
                toolManager.EditTool(newTestTool);

                IQueryable<Tool> tools = toolManager.GetTools();

                Assert.Contains(tools,
                    e => e.Model == modelTestString2 && e.Manufacture == manufactureTestString2 &&
                         e.Price == priceTestDecimal2);
            }

            provider.Dispose();
        }

        [Fact]
        public void GetToolsAssignedToUserTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool1 = new Tool
            {
                Model = "Den store hammer",
                Manufacture = "Gerts Hammerfabrik"
            };

            Tool testTool2 = new Tool
            {
                Model = "Den mindre hammer",
                Manufacture = "Gerts Hammerfabrik"
            };

            User testUser1 = new User
            {
                FirstName = "Rasmus",
                LastName = "Raskemus"
            };

            using (IServiceScope scope = provider.GetScope())
            {
                using ConstructionContext context = scope.ServiceProvider.GetRequiredService<ConstructionContext>();
                context.Users.Add(testUser1);
                context.Tools.Add(testTool1);
                context.Tools.Add(testTool2);
                context.SaveChanges();
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                toolManager.AssignToUser(testTool1, testUser1);
                toolManager.AssignToUser(testTool2, testUser1);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                List<Tool> toolList = toolManager.GetToolsAssignedToUser(testUser1).ToList();

                Assert.Contains(toolList, e => e.Model == testTool1.Model);
                Assert.Contains(toolList, e => e.Model == testTool2.Model);
            }

            provider.Dispose();
        }

        [Fact]
        public void GetToolsAssignedToOrganizationTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool1 = new Tool
            {
                Model = "Den store hammer",
                Manufacture = "Gerts Hammerfabrik"
            };

            Tool testTool2 = new Tool
            {
                Model = "Den mindre hammer",
                Manufacture = "Gerts Hammerfabrik"
            };

            User testUser1 = new User
            {
                FirstName = "Rasmus",
                LastName = "Raskemus"
            };

            using (IServiceScope scope = provider.GetScope())
            {
                using ConstructionContext context = scope.ServiceProvider.GetRequiredService<ConstructionContext>();

                context.Users.Add(testUser1);
                context.Tools.Add(testTool1);
                context.Tools.Add(testTool2);
                context.SaveChanges();
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager =
                    scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();

                organizationManager.AssignAssetToOrganization(testTool1, provider.GetTestOrg);
                organizationManager.AssignAssetToOrganization(testTool2, provider.GetTestOrg);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                List<Tool> toolList = toolManager.GetTools().ToList();

                Assert.Contains(toolList, e => e.Model == testTool1.Model);
                Assert.Contains(toolList, e => e.Model == testTool2.Model);
            }

            provider.Dispose();
        }


        [Fact]
        public void GetUserAssignedToToolTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool = new Tool
            {
                Model = "Den store hammer",
                Manufacture = "Gerts Hammerfabrik"
            };

            User testUser = new User
            {
                FirstName = "Rasmus",
                LastName = "Raskemus"
            };

            using (IServiceScope scope = provider.GetScope())
            {
                using ConstructionContext context = scope.ServiceProvider.GetRequiredService<ConstructionContext>();

                context.Users.Add(testUser);
                context.Tools.Add(testTool);
                context.SaveChanges();
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                toolManager.AssignToUser(testTool, testUser);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                User userAssignedToTool = toolManager.GetUserAssignedToTool(testTool);

                Assert.Equal(userAssignedToTool.Id, testUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void RemoveToolTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool1 = new Tool
            {
                Model = "Den mindre hammer",
                Manufacture = "Gerts Hammerfabrik"
            };

            using (IServiceScope scope = provider.GetScope())
            {
                using ConstructionContext context = scope.ServiceProvider.GetRequiredService<ConstructionContext>();
                context.Tools.Add(testTool1);
                context.SaveChanges();
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                toolManager.RemoveTool(testTool1);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                using ConstructionContext context = scope.ServiceProvider.GetRequiredService<ConstructionContext>();
                List<Tool> toolList = context.Tools.ToList();
                Assert.DoesNotContain(toolList, tool => tool == testTool1);
            }

            provider.Dispose();
        }

        [Fact]
        public void RequestToolTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool tool;
            // Request tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                tool = toolManager.CreateTool("", "", 1);
                var user = userManager.CreateUser("aawd", "adads");
                toolManager.AssignToUser(tool, user);

                var assignmentRequestEvent = toolManager.RequestTool(tool, provider.GetTestUser);
                Assert.Equal(assignmentRequestEvent.RequestingUserId, provider.GetTestUser.Id);
                Assert.Equal(assignmentRequestEvent.AssetId, tool.Id);
            }

            // Check if request exist
            using (IServiceScope scope = provider.GetScope())
            {
                var eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                var events = eventManager.GetEventsByType<AssignmentRequestEvent>(tool);
                Assert.Contains(events,
                    e => e.AssetId == tool.Id &&
                         e.RequestingUserId == provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void GetAssignmentRequestsTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool;
            // Request tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                testTool = toolManager.CreateTool("", "", 1);
                var user = userManager.CreateUser("aawd", "adads");
                toolManager.AssignToUser(testTool, user);
                var assignmentRequestEvent = toolManager.RequestTool(testTool, provider.GetTestUser);
                Assert.Equal(assignmentRequestEvent.RequestingUserId, provider.GetTestUser.Id);
                Assert.Equal(assignmentRequestEvent.AssetId, testTool.Id);
            }

            // Check if request exist
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                var assigmentRequestsEvents = toolManager.GetAssignmentRequests(testTool);
                Assert.Contains(assigmentRequestsEvents,
                    e => e.AssetId == testTool.Id &&
                         e.RequestingUserId == provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void CancelToolRequestTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);
            AssignmentRequestEvent assignmentRequestEvent;

            Tool testTool;
            // Request tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                testTool = toolManager.CreateTool("", "", 1);
                var user = userManager.CreateUser("aawd", "adads");
                toolManager.AssignToUser(testTool, user);

                assignmentRequestEvent = toolManager.RequestTool(testTool, provider.GetTestUser);
                Assert.Equal(assignmentRequestEvent.RequestingUserId, provider.GetTestUser.Id);
                Assert.Equal(assignmentRequestEvent.AssetId, testTool.Id);
            }

            // Check if request exist
            using (IServiceScope scope = provider.GetScope())
            {
                var eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                var events = eventManager.GetEventsByType<AssignmentRequestEvent>(testTool);
                Assert.Contains(events,
                    e => e.AssetId == testTool.Id &&
                         e.RequestingUserId == provider.GetTestUser.Id);
            }

            // Cancel tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                toolManager.CancelToolRequest(assignmentRequestEvent);
            }

            // Check if tool is cancelled
            using (IServiceScope scope = provider.GetScope())
            {
                var eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                var events = eventManager.GetEventsByType<AssignmentRequestEvent>(testTool);
                Assert.Contains(events,
                    e => !e.IsActive && e.AssetId == testTool.Id &&
                        e.RequestingUserId == provider.GetTestUser.Id);
                Assert.DoesNotContain(events,
                    e => e.IsActive && e.AssetId == testTool.Id &&
                         e.RequestingUserId == provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void AcceptToolTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            AssignmentRequestEvent assignmentRequestEvent;
            Tool tool;
            // Request tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                tool = toolManager.CreateTool("", "", 1);
                var user = userManager.CreateUser("aawd", "adads");
                toolManager.AssignToUser(tool, user);
                assignmentRequestEvent = toolManager.RequestTool(tool, provider.GetTestUser);
                Assert.Equal(assignmentRequestEvent.RequestingUserId, provider.GetTestUser.Id);
                Assert.Equal(assignmentRequestEvent.AssetId, tool.Id);
            }

            // Accept tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                var assignmentConfirmationEvent =
                    toolManager.AcceptTool(assignmentRequestEvent, tool, provider.GetTestUser);
                Assert.Equal(assignmentConfirmationEvent.ConfirmedUserId, provider.GetTestUser.Id);
                Assert.Equal(assignmentConfirmationEvent.AssetId, tool.Id);
            }

            // Check if accept exists
            using (IServiceScope scope = provider.GetScope())
            {
                var eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                var confirmationEvents = eventManager.GetEventsByType<AssignmentConfirmationEvent>(tool);
                Assert.Contains(confirmationEvents,
                    e => e.IsActive && e.AssetId == tool.Id &&
                         e.ConfirmedUserId == provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void ReportToolTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool = new Tool
            {
                Model = "Den mindre hammer",
                Manufacture = "Gerts Hammerfabrik"
            };
            ReportBrokenEvent reportBrokenEvent;
            // Report tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                reportBrokenEvent = toolManager.ReportTool(provider.GetTestUser, testTool, "test");
                Assert.Equal(reportBrokenEvent.ReportedByUserId, provider.GetTestUser.Id);
                Assert.Equal(reportBrokenEvent.AssetId, testTool.Id);
            }

            // Check if report exists
            using (IServiceScope scope = provider.GetScope())
            {
                var eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                var brokenEvents = eventManager.GetEventsByType<ReportBrokenEvent>(testTool);
                Assert.Contains(brokenEvents,
                    e => e.IsActive && e.AssetId == testTool.Id &&
                         e.ReportedByUserId == provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void GetRepairRequests()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool = new Tool
            {
                Model = "Den mindre hammer",
                Manufacture = "Gerts Hammerfabrik"
            };
            // Repair tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                var reportEvent = toolManager.ReportTool(provider.GetTestUser, testTool, "test");
                Assert.Equal(reportEvent.ReportedByUserId, provider.GetTestUser.Id);
                Assert.Equal(reportEvent.AssetId, testTool.Id);
            }

            // Check if Repair exist
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                var reportEvents = toolManager.GetRepairRequests(testTool);
                Assert.Contains(reportEvents,
                    e => e.AssetId == testTool.Id &&
                         e.ReportedByUserId == provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void FixToolTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool = new Tool
            {
                Model = "Den mindre hammer",
                Manufacture = "Gerts Hammerfabrik"
            };
            ReportBrokenEvent reportBrokenEvent;
            ReportFixedEvent reportFixedEvent;
            // Report tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                reportBrokenEvent = toolManager.ReportTool(provider.GetTestUser, testTool, "test");
                Assert.Equal(reportBrokenEvent.ReportedByUserId, provider.GetTestUser.Id);
                Assert.Equal(reportBrokenEvent.AssetId, testTool.Id);
            }

            // Check if report exists
            using (IServiceScope scope = provider.GetScope())
            {
                var eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                var brokenEvents = eventManager.GetEventsByType<ReportBrokenEvent>(testTool);
                Assert.Contains(brokenEvents,
                    e => e.IsActive && e.AssetId == testTool.Id &&
                         e.ReportedByUserId == provider.GetTestUser.Id);
            }

            // Fix tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                reportFixedEvent = toolManager.FixTool(reportBrokenEvent, provider.GetTestUser, testTool);
                Assert.Equal(reportFixedEvent.FixedByUserId, provider.GetTestUser.Id);
                Assert.Equal(reportFixedEvent.AssetId, testTool.Id);
            }

            // Check if fix exist
            using (IServiceScope scope = provider.GetScope())
            {
                var eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                var brokenEvents = eventManager.GetEventsByType<ReportBrokenEvent>(testTool);
                var fixedEvents = eventManager.GetEventsByType<ReportFixedEvent>(testTool);
                Assert.Contains(fixedEvents,
                    e => e.IsActive && e.AssetId == testTool.Id &&
                         e.FixedByUserId == provider.GetTestUser.Id);
                Assert.DoesNotContain(brokenEvents,
                    e => e.IsActive && e.AssetId == testTool.Id &&
                         e.ReportedByUserId == provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void IsBrokenTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Tool testTool = new Tool
            {
                Model = "Den mindre hammer",
                Manufacture = "Gerts Hammerfabrik"
            };
            ReportBrokenEvent reportBrokenEvent;
            // Report tool
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                reportBrokenEvent = toolManager.ReportTool(provider.GetTestUser, testTool, "test");
                Assert.Equal(reportBrokenEvent.ReportedByUserId, provider.GetTestUser.Id);
                Assert.Equal(reportBrokenEvent.AssetId, testTool.Id);
            }

            // Check if report exists
            using (IServiceScope scope = provider.GetScope())
            {
                var toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                var isBroken = toolManager.IsBroken(testTool);
                Assert.True(isBroken);
            }

            provider.Dispose();
        }
    }
}