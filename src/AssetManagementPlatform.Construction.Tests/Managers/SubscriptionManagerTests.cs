﻿using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Construction.Tests.Managers
{
    public class SubscriptionManagerTests
    {
        [Fact]
        public void SubscribeToTool()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider();

            User user;
            ToolSubscriptionGroup toolSubscriptionGroup;
            using (IServiceScope serviceScope = provider.GetScope())
            {
                SavingToolSubscriptionManager subscriptionManager = serviceScope.ServiceProvider.GetRequiredService<SavingToolSubscriptionManager>();
                SavingToolManager toolManager = serviceScope.ServiceProvider.GetRequiredService<SavingToolManager>();
                SavingUserManager userManager = serviceScope.ServiceProvider.GetRequiredService<SavingUserManager>();

                Tool tool = toolManager.CreateTool("A", "B", 1);
                user = userManager.CreateUser("user1", "dd");

                toolSubscriptionGroup = subscriptionManager.Create("Test group");
                subscriptionManager.AddTool(toolSubscriptionGroup, tool);
                subscriptionManager.Subscribe(toolSubscriptionGroup, user);
            }

            using (IServiceScope serviceScope = provider.GetScope())
            {
                SavingToolSubscriptionManager subscriptionManager = serviceScope.ServiceProvider.GetRequiredService<SavingToolSubscriptionManager>();
                Assert.NotEmpty(subscriptionManager.GetSubscribedTools(user));
            }

            using (IServiceScope serviceScope = provider.GetScope())
            {
                SavingToolSubscriptionManager subscriptionManager = serviceScope.ServiceProvider.GetRequiredService<SavingToolSubscriptionManager>();
                subscriptionManager.Delete(toolSubscriptionGroup);
            }

            using (IServiceScope serviceScope = provider.GetScope())
            {
                SavingToolSubscriptionManager subscriptionManager = serviceScope.ServiceProvider.GetRequiredService<SavingToolSubscriptionManager>();
                IQueryable<Tool> subscribedTools = subscriptionManager.GetSubscribedTools(user);
                Assert.Empty(subscribedTools);
            }

            provider.Dispose();
        }
    }
}