﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.ConstructionModule.Code.Authentication;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Controllers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.Entities.Events;
using AssetManagementPlatform.ConstructionModule.ViewModels.Tool;
using AssetManagementPlatform.ConstructionModule.ViewModels.Tool.Model;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.Notification.Abstractions;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using AssetManagementPlatform.Shared.Tests;
using AssetManagementPlatform.Shared.Tests.TestHelpers;

namespace AssetManagementPlatform.Construction.Tests.Controllers
{
    public class ToolControllerTest
    {
        [Fact]
        public async Task TestDelete()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            const string toolModel = "test model";
            const string toolManufacture = "test manufacture";
            const int toolPrice = 111;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                                                                   new ConstructionSitePermissions());

                // Create tool
                Tool testTool = toolManager.CreateTool(toolModel, toolManufacture, toolPrice);
                toolManager.AssignToUser(testTool, provider.GetTestUser);

                // Fetch tool
                IQueryable<Tool> tools = toolManager.GetTools();
                Assert.Contains(tools,
                                e => e.Model == toolModel && e.Manufacture == toolManufacture && e.Price == toolPrice);

                // Delete org
                await toolController.Delete(testTool.Id);

                // Fetch orgs
                tools = toolManager.GetTools();

                Assert.DoesNotContain(tools,
                                      e => e.Model == toolModel && e.Manufacture == toolManufacture && e.Price == toolPrice);
            }

            provider.Dispose();
        }

        [Fact]
        public async Task TestCreate()
        {
            const string toolModel = "test model";
            const string toolManufacture = "test manufacture";
            const int toolPrice = 111;
            ConstructionTestProvider provider = new ConstructionTestProvider(false);
            Tool testTool;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                                                                   new ConstructionSitePermissions());

                // Create tool
                toolController.Create(new ToolCreateModel
                {
                    Manufacture = toolManufacture,
                    Model = toolModel,
                    Price = toolPrice
                });
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                // Fetch tool
                IQueryable<Tool> tools = toolManager.GetTools();
                Assert.Contains(tools,
                                e => e.Manufacture == toolManufacture && e.Model == toolModel && e.Price == toolPrice);
            }

            provider.Dispose();
        }

        [Fact]
        public async Task TestDetails()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            const string modelString = "Den store hammer";
            const string manufactureString = "Gerts Hammerfabrik";
            const int priceDecimal = 900;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(
                                                                   provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                                                                   new ConstructionSitePermissions()
                                                                   {
                                                                       CanSeeAllTools = true
                                                                   });

                Tool testTool = toolManager.CreateTool(modelString, manufactureString, priceDecimal);

                toolManager.AssignToUser(testTool, provider.GetTestUser);
                IActionResult result = await toolController.Details(testTool.Id);
                ViewResult viewResult = Assert.IsType<ViewResult>(result);
                ToolDetailsModel model = Assert.IsType<ToolDetailsModel>(viewResult.Model);

                Assert.Equal(modelString, model.Model);
                Assert.Equal(manufactureString, model.Manufacture);
                Assert.Equal(priceDecimal, model.Price);
                Assert.Equal(model.AssignedUser.Id, provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestEdit()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            const string toolModel1 = "test model";
            const string toolManufacture1 = "test manaufature";
            const int toolPrice1 = 111;

            const string toolModel2 = "another test model";
            const string toolManufacture2 = "another test manaufature";
            const int toolPrice2 = 222;

            Tool testTool;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                testTool = toolManager.CreateTool(toolModel1, toolManufacture1, toolPrice1);
                toolManager.AssignToUser(testTool, provider.GetTestUser);

                IQueryable<Tool> tools = toolManager.GetTools();
                Assert.Contains(tools, e => e.Model == toolModel1 && e.Manufacture == toolManufacture1 && e.Price == toolPrice1);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>();

                ToolEditModel editModel = new ToolEditModel
                {
                    Model = toolModel2,
                    Manufacture = toolManufacture2,
                    Price = toolPrice2,
                    Id = testTool.Id
                };

                toolController.Edit(editModel);

                var tools = toolManager.GetTools();
                Assert.Contains(tools,
                                e => e.Model == toolModel2 && e.Manufacture == toolManufacture2 && e.Price == toolPrice2);
            }

            provider.Dispose();
        }

        [Fact]
        public async Task TestIndex()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager =
                    scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(
                                                                   provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                                                                   new ConstructionSitePermissions()
                                                                   {
                                                                       CanSeeAllTools = false
                                                                   });

                Tool testTool1 = toolManager.CreateTool("Den store hammer", "Gerts Hammerfabrik", 900);

                toolManager.AssignToUser(testTool1, provider.GetTestUser);
                organizationManager.AssignAssetToOrganization(testTool1, provider.GetTestOrg);

                IActionResult result = await toolController.Index(null);
                ViewResult viewResult = Assert.IsType<ViewResult>(result);
                List<ToolModel> tools = Assert.IsType<List<ToolModel>>(viewResult.Model);

                Assert.Contains(tools, e => e.Tool.Model == testTool1.Model);
                Assert.Contains(tools, e => e.Tool.Id == testTool1.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public async Task TestReport()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            //report broken
            Tool tool;
            Event first;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                SavingToolSubscriptionManager subscriptionManager =
                    scope.ServiceProvider.GetRequiredService<SavingToolSubscriptionManager>();

                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg);

                tool = toolManager.CreateTool("", "", 1);

                ToolSubscriptionGroup toolSubscriptionGroup = subscriptionManager.Create("group of engineers");
                subscriptionManager.AddTool(toolSubscriptionGroup, tool);
                subscriptionManager.Subscribe(toolSubscriptionGroup, provider.GetTestUser);

                IActionResult result = await toolController.Report(tool.Id, "Tool is broken");
                Assert.IsType<RedirectToActionResult>(result);
            }

            //Check broken
            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();

                first = eventManager.GetEventsByType<ReportBrokenEvent>(tool).Single();
                Assert.Equal(typeof(ReportBrokenEvent).Name, first.Type);

                MockNotificationService notificationService =
                    (MockNotificationService)scope.ServiceProvider.GetRequiredService<INotificationService>();
                Assert.Single(notificationService.GroupNotifications);
            }

            //Report fixed
            using (IServiceScope scope = provider.GetScope())
            {
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg);

                IActionResult result = await toolController.ReportFixed(first.Id, tool.Id);
                Assert.IsType<RedirectToActionResult>(result);
            }

            //Check fixed
            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();

                ReportFixedEvent fixedEvent = eventManager.GetEventsByType<ReportFixedEvent>(tool).Single();
                Assert.Equal(typeof(ReportFixedEvent).Name, fixedEvent.Type);

                MockNotificationService notificationService =
                    (MockNotificationService)scope.ServiceProvider.GetRequiredService<INotificationService>();
                Assert.Equal(2, notificationService.GroupNotifications.Count);
            }

            provider.Dispose();
        }

        [Fact]
        public async void ToolRequest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            //Tool request
            Tool tool;
            Event first;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                SavingToolSubscriptionManager subscriptionManager =
                    scope.ServiceProvider.GetRequiredService<SavingToolSubscriptionManager>();

                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg);
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                tool = toolManager.CreateTool("", "", 1);
                var user = userManager.CreateUser("aawd", "adads");
                toolManager.AssignToUser(tool, user);

                ToolSubscriptionGroup toolSubscriptionGroup = subscriptionManager.Create("group of engineers");
                subscriptionManager.AddTool(toolSubscriptionGroup, tool);
                subscriptionManager.Subscribe(toolSubscriptionGroup, provider.GetTestUser);

                IActionResult result = await toolController.ToolRequest(tool.Id);

                Assert.IsType<RedirectToActionResult>(result);
            }

            //Check if requested
            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();

                first = eventManager.GetEventsByType<AssignmentRequestEvent>(tool).Single();
                Assert.Equal(typeof(AssignmentRequestEvent).Name, first.Type);

                MockNotificationService notificationService =
                    (MockNotificationService)scope.ServiceProvider.GetRequiredService<INotificationService>();
                Assert.Single(notificationService.UserNotifications);
                Assert.Equal(((AssignmentRequestEvent)first).RequestingUserId, provider.GetTestUser.Id);
            }

            //Accept request
            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                User user = toolManager.GetUserAssignedToTool(tool);
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg);

                IActionResult result = await toolController.Accept(first.Id, tool.Id, provider.GetTestUser.Id);
                Assert.IsType<RedirectToActionResult>(result);
            }

            //Check accepted
            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                AssignmentConfirmationEvent acceptEvent = eventManager.GetEventsByType<AssignmentConfirmationEvent>(tool).Single();
                Assert.Equal(typeof(AssignmentConfirmationEvent).Name, acceptEvent.Type);
                AssignmentEvent assignmentEvent = eventManager.GetEventsByType<AssignmentEvent>(tool).FirstOrDefault();
                Assert.Equal(typeof(AssignmentEvent).Name, assignmentEvent.Type);

                MockNotificationService notificationService =
                    (MockNotificationService)scope.ServiceProvider.GetRequiredService<INotificationService>();
                Assert.Equal(1, notificationService.UserNotifications.Count);
                User user = toolManager.GetUserAssignedToTool(tool);
                Assert.Equal(user.Id, provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestTransfer()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg);
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();

                User testUser1 = provider.GetTestUser;

                Tool testTool = toolManager.CreateTool("Den store hammer", "Gerts Hammerfabrik", 900);
                User testUser2 = userManager.CreateUser("børge", "andersen");

                // Give to original user
                toolManager.AssignToUser(testTool, testUser2);

                IQueryable<Tool> tools = toolManager.GetToolsAssignedToUser(testUser2);
                Assert.Contains(tools, e => e.Model == testTool.Model);

                // Assign to another user
                toolController.TransferToMe(testTool.Id);

                tools = toolManager.GetToolsAssignedToUser(testUser1);
                Assert.Contains(tools, e => e.Model == testTool.Model);
            }

            provider.Dispose();
        }

        [Fact]
        public async void CancelToolRequestTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            //Tool request
            Tool tool;
            Event theEvent;
            AssignmentRequestEvent request;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                tool = toolManager.CreateTool("", "", 1);
                var user = userManager.CreateUser("aawd", "adads");
                toolManager.AssignToUser(tool, user);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                request = toolManager.RequestTool(tool, provider.GetTestUser);
            }

            //Check if requested
            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                theEvent = eventManager.GetEventsByType<AssignmentRequestEvent>(tool).Single();
                Assert.Equal(typeof(AssignmentRequestEvent).Name, theEvent.Type);
                Assert.Equal(((AssignmentRequestEvent)theEvent).RequestingUserId, provider.GetTestUser.Id);
            }

            // Cancel tool request
            using (IServiceScope scope = provider.GetScope())
            {
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg);
                await toolController.CancelToolRequest(request.Id);
            }

            //Check if requested is cancelled
            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                theEvent = eventManager.GetEventsByType<AssignmentRequestEvent>(tool).Single();
                Assert.Equal(typeof(AssignmentRequestEvent).Name, theEvent.Type);
                Assert.Equal(((AssignmentRequestEvent)theEvent).RequestingUserId, provider.GetTestUser.Id);
                Assert.False(theEvent.IsActive);
            }

            provider.Dispose();
        }

        [Fact]
        public async void AcceptResolutionTest()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            //Tool request
            Tool tool;
            User user1;
            User user2;
            Event first;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                user1 = userManager.CreateUser("wd", "ad");
                user2 = userManager.CreateUser("wds", "ads");
                tool = toolManager.CreateTool("", "", 1);
                toolManager.AssignToUser(tool, user1);

                toolManager.RequestTool(tool, provider.GetTestUser);
                toolManager.RequestTool(tool, user2);
            }

            //Check if requested
            using (IServiceScope scope = provider.GetScope())
            {
                ToolController toolController = scope.ServiceProvider.GetRequiredService<ToolController>()
                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg);

                var result = await toolController.AcceptResolution(tool.Id);
                ViewResult viewResult = Assert.IsType<ViewResult>(result);
                ToolAcceptResolutionModel resolutionModel = Assert.IsType<ToolAcceptResolutionModel>(viewResult.Model);
                Assert.Contains(resolutionModel.PendingRequests,
                                e => e.AssignmentRequestEvent.RequestingUserId == provider.GetTestUser.Id);
                Assert.Contains(resolutionModel.PendingRequests,
                                e => e.AssignmentRequestEvent.RequestingUserId == user2.Id);
            }

            provider.Dispose();
        }
    }
}