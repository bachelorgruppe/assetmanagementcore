﻿using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.Authentication;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.Frontend.Controllers;
using AssetManagementPlatform.Frontend.ViewModels.Organization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Construction.Tests.Controllers
{
    public class OrganizationControllerTest
    {
        [Fact]
        public void TestCreate()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider();

            const string orgNameString = "test org";
            const string orgAddressString = "Test address";

            using (IServiceScope scope = provider.GetScope())
            {
                OrganizationController organizationController = scope.ServiceProvider.GetRequiredService<OrganizationController>();
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();

                OrganizationCreateModel createModel = new OrganizationCreateModel
                {
                    Name = orgNameString,
                    Address = orgAddressString
                };

                // Create Org
                organizationController.Create(createModel);

                // Fetch orgs
                List<Organization> orgs = organizationManager
                                          .GetOrganizations()
                                          .Include(x => x.Location)
                                          .ToList();

                Assert.Contains(orgs, e => e.Name == orgNameString && e.Location.Address == orgAddressString);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestDelete()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider();

            const string orgNameString = "test org";
            const string orgAddressString = "Test address";

            Organization testOrg;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();

                // Create org
                testOrg = organizationManager.CreateOrganization(orgNameString, new Location { Address = orgAddressString });
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                OrganizationController organizationController = scope.ServiceProvider.GetRequiredService<OrganizationController>();

                // Fetch orgs
                List<Organization> orgs = organizationManager
                                          .GetOrganizations()
                                          .Include(x => x.Location)
                                          .ToList();

                Assert.Contains(orgs, e => e.Name == orgNameString && e.Location.Address == orgAddressString);

                // Delete org
                organizationController.Delete(testOrg.Id);

                // Fetch orgs
                orgs = organizationManager
                       .GetOrganizations()
                       .Include(e => e.Location)
                       .ToList();

                Assert.DoesNotContain(orgs, e => e.Name == orgNameString && e.Location.Address == orgAddressString);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestEdit()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider();

            const string orgNameString1 = "test org";
            const string orgAddressString1 = "Test address";

            const string orgNameString2 = "test org";
            const string orgAddressString2 = "Test address";

            Organization testOrg;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                OrganizationController organizationController = scope.ServiceProvider.GetRequiredService<OrganizationController>();

                // Create Org
                testOrg = organizationManager.CreateOrganization(orgNameString1, new Location { Address = orgAddressString1 });

                // Fetch orgs
                List<Organization> orgs = organizationManager
                                          .GetOrganizations()
                                          .Include(x => x.Location)
                                          .ToList();

                Assert.Contains(orgs, e => e.Name == orgNameString1 && e.Location.Address == orgAddressString1);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                OrganizationController organizationController = scope.ServiceProvider.GetRequiredService<OrganizationController>();



                OrganizationEditModel editModel = new OrganizationEditModel
                {
                    Name = orgNameString2,
                    Address = orgAddressString2,
                    Id = testOrg.Id
                };

                // Edit org
                organizationController.Edit(editModel);

                // Fetch orgs
                var orgs = organizationManager
                       .GetOrganizations()
                       .Include(x => x.Location)
                       .ToList();

                Assert.Contains(orgs, e => e.Name == orgNameString2 && e.Location.Address == orgAddressString2);
            }

            provider.Dispose();
        }

        [Fact] // Tests with the assumption of one org per user
        public void TestIndex()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                OrganizationController organizationController = scope.ServiceProvider.GetRequiredService<OrganizationController>()
                                                                     .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions()
                                                                     {
                                                                         ManageOrganizations = true
                                                                     }, new ConstructionSitePermissions());

                // Get the only org assigned to the user
                IActionResult result = organizationController.Index();
                var viewResult = Assert.IsType<ViewResult>(result);
                List<Organization> organizations = Assert.IsType<List<Organization>>(viewResult.Model);

                Assert.Single(organizations);
                Assert.Equal(provider.GetTestOrg.Id, organizations[0].Id);
            }

            provider.Dispose();
        }
    }
}