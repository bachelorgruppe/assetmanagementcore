﻿using System;
using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Controllers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.ViewModels.Reservation;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using AssetManagementPlatform.Shared.Tests;

namespace AssetManagementPlatform.Construction.Tests.Controllers
{
    public class ReservationControllerTest
    {
        [Fact]
        public void BaseCase()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            User testUser;
            Tool testTool;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();
                ReservationController reservationController = scope.ServiceProvider.GetRequiredService<ReservationController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg);

                //Setup
                testUser = userManager.CreateUser("hej","test");
                testTool = toolManager.CreateTool("hej","tool",99);

                IActionResult result1 = reservationController.Index();
                ViewResult model1 = Assert.IsType<ViewResult>(result1);
                ReservationIndexModel modelResult1 = Assert.IsType<ReservationIndexModel>(model1.Model);
                Assert.Empty(modelResult1.Reservations);

                
                IActionResult result = reservationController.Create();
                ViewResult model2 = Assert.IsType<ViewResult>(result);
                Assert.IsType<ReservationCreateModel>(model2.Model);
            }

            ReservationEvent reservationEvent;
            using (IServiceScope scope = provider.GetScope())
            {
                //Make reservation
                ReservationController reservationController = scope.ServiceProvider.GetRequiredService<ReservationController>();
                SavingReservationManager reservationManager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();
                
                ReservationCreateModel model = new ReservationCreateModel
                {
                    ReservedToId = testUser.Id,
                    ReservedAssetId = testTool.Id,
                    ReservationStartDate = new DateTime(),
                    ReservationEndDate = new DateTime()
                };
                IActionResult result = reservationController.Create(model);
                Assert.IsType<RedirectToActionResult>(result);
                reservationEvent = reservationManager.GetReservationEvents().First();
            }

            using (IServiceScope scope = provider.GetScope())
            {
                ReservationController reservationController = scope.ServiceProvider.GetRequiredService<ReservationController>();
                IActionResult result = reservationController.Edit(reservationEvent.Id);

                ViewResult model = Assert.IsType<ViewResult>(result);
                ReservationEditModel modelResult = Assert.IsType<ReservationEditModel>(model.Model);

                Assert.Equal(testUser.Id.ToString(), modelResult.ReservedToOptions.Single(a => a.Selected).Value);
                Assert.Equal(testTool.Id.ToString(), modelResult.ReservedAssetOptions.Single(a => a.Selected).Value);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                //Edit reservation
                ReservationController reservationController = scope.ServiceProvider.GetRequiredService<ReservationController>();
                ReservationEditModel model = new ReservationEditModel
                {
                    Id = reservationEvent.Id,
                    ReservedAssetId = reservationEvent.ReservedToId,
                    ReservedToId = reservationEvent.ReservedAssetId,
                    ReservationEndDate = reservationEvent.ReservationEndDate,
                    ReservationStartDate = reservationEvent.ReservationStartDate
                };
                IActionResult result3 = reservationController.Edit(model);
                Assert.IsType<RedirectToActionResult>(result3);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingReservationManager reservationManager = scope.ServiceProvider.GetRequiredService<SavingReservationManager>();

                IQueryable<ReservationEvent> model = reservationManager.GetReservationEvents();
                Assert.False(model.Single(r => r.Id == reservationEvent.Id).IsActive);
                ReservationEvent single = model.Single(r => r.Id != reservationEvent.Id && r.IsActive);
                Assert.True(single.IsActive);

                Assert.Equal(reservationEvent.ReservedToId, single.ReservedAssetId);
                Assert.Equal(reservationEvent.ReservedAssetId, single.ReservedToId);
            }

            provider.Dispose();
        }
    }
}