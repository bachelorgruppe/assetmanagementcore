﻿using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.ConstructionModule.Code.SavingManagers;
using AssetManagementPlatform.ConstructionModule.Controllers;
using AssetManagementPlatform.ConstructionModule.Entities;
using AssetManagementPlatform.ConstructionModule.ViewModels.User;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using AssetManagementPlatform.Shared.Tests;

namespace AssetManagementPlatform.Construction.Tests.Controllers
{
    public class UserControllerTest
    {
        [Fact]
        public void TestCreate()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            const string firstNameString = "Gert";
            const string lastnameString = "Andersen";

            using (IServiceScope scope = provider.GetScope())
            {
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                UserController userController = scope.ServiceProvider.GetRequiredService<UserController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg);
                SavingUserManager savingUserManager = userManager;

                userController
                    .Create(new UserCreateModel
                    {
                        Firstname = firstNameString,
                        LastName = lastnameString
                    });
                IQueryable<User> users = savingUserManager.GetUsers();

                Assert.Contains(users, e => e.FirstName == firstNameString);
                Assert.Contains(users, e => e.LastName == lastnameString);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestDelete()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider();

            const string firstNameString = "Gert";
            const string lastnameString = "Andersen";
            string fullnameString = $"{firstNameString} {lastnameString}";

            using (IServiceScope scope = provider.GetScope())
            {
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                UserController userController = scope.ServiceProvider.GetRequiredService<UserController>();

                User onlyUser = userManager.CreateUser(firstNameString, lastnameString);

                Assert.Equal(firstNameString, onlyUser.FirstName);
                Assert.Equal(lastnameString, onlyUser.LastName);
                Assert.Equal(fullnameString, onlyUser.FullName);

                userController.Delete(onlyUser.Id);

                User removedUser = userManager.GetUsers().SingleOrDefault(u => u.Id == onlyUser.Id);
                Assert.Null(removedUser);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestDetails()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider();

            const string firstNameString = "Gert";
            const string lastnameString = "Andersen";
            const string addressString = "Boholtevej 32";

            const string modelString = "Test tool Model";
            const string manufactureString = "Test tool Manufacture";
            const int priceDecimal = 666;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                UserController userController = scope.ServiceProvider.GetRequiredService<UserController>();

                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                SavingOrganizationManager orgManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                SavingToolManager toolManager = scope.ServiceProvider.GetRequiredService<SavingToolManager>();

                Location localization = new Location
                {
                    Address = addressString
                };
                Organization org = orgManager.CreateOrganization("test", localization);
                Project project = projectManager.CreateProject("test project", localization, org);

                User onlyUser = userManager.CreateUser(firstNameString, lastnameString);
                projectManager.AssignAssetToProject(project, onlyUser);

                Tool testTool = toolManager.CreateTool(modelString, manufactureString, priceDecimal);
                toolManager.AssignToUser(testTool, onlyUser);

                Assert.Equal(firstNameString, onlyUser.FirstName);
                Assert.Equal(lastnameString, onlyUser.LastName);

                ViewResult result = userController.Details(onlyUser.Id);

                UserDetailsModel detailsResult = Assert.IsType<UserDetailsModel>(result.Model);
                Assert.Equal(firstNameString, detailsResult.Firstname);
                Assert.Equal(lastnameString, detailsResult.LastName);
                Assert.Equal(addressString, detailsResult.Address);
                Assert.Contains(detailsResult.Tools, tool => tool.Id == testTool.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestEdit()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider();

            const string firstNameString1 = "Gert";
            const string lastnameString1 = "Andersen";

            const string firstNameString2 = "Børge";
            const string lastnameString2 = "Bo";

            User onlyUser;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();

                // Create user
                onlyUser = userManager.CreateUser(firstNameString1, lastnameString1);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                UserController userController = scope.ServiceProvider.GetRequiredService<UserController>();

                // Edit user
                userController
                    .Edit(new UserEditModel
                    {
                        Firstname = firstNameString2,
                        LastName = lastnameString2,
                        Id = onlyUser.Id
                    });

                IQueryable<User> users = userManager.GetUsers();

                Assert.DoesNotContain(users, e => e.FirstName == firstNameString1);
                Assert.DoesNotContain(users, e => e.LastName == lastnameString1);

                Assert.Contains(users, e => e.FirstName == firstNameString2);
                Assert.Contains(users, e => e.LastName == lastnameString2);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestIndex()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            const string firstNameString = "Gert";
            const string lastnameString = "Andersen";

            using (IServiceScope scope = provider.GetScope())
            {
                SavingOrganizationManager organizationManager = scope.ServiceProvider.GetRequiredService<SavingOrganizationManager>();
                SavingUserManager userManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                UserController userController = scope.ServiceProvider.GetRequiredService<UserController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg);

                User onlyUser = userManager.CreateUser(firstNameString, lastnameString);

                organizationManager.AssignAssetToOrganization(onlyUser, provider.GetTestOrg);

                ViewResult result = userController.Index(null);

                List<User> users = Assert.IsType<List<User>>(result.Model);
                Assert.Contains(users, e => e.FirstName == firstNameString && e.LastName == lastnameString);
            }

            provider.Dispose();
        }
    }
}