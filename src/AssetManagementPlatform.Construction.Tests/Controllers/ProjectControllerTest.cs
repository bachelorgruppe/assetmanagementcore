﻿using System.Collections.Generic;
using AssetManagementPlatform.ConstructionModule.Controllers;
using AssetManagementPlatform.ConstructionModule.ViewModels.Project;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using AssetManagementPlatform.Shared.Tests;

namespace AssetManagementPlatform.Construction.Tests.Controllers
{
    public class ProjectControllerTest
    {
        [Fact]
        public void TestCreate()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                ProjectController projectController = scope.ServiceProvider.GetRequiredService<ProjectController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg);

                const string projectNameString = "Test project";
                const string testAddressString = "Test address";

                ProjectCreateModel model = new ProjectCreateModel
                {
                    Name = projectNameString,
                    Address = testAddressString
                };
                projectController.Create(model);

                IIncludableQueryable<Project, Location> projects = projectManager
                                                                   .GetProjects()
                                                                   .Include(e => e.Location);

                Assert.Contains(projects, e => e.Name == projectNameString && e.Location.Address == testAddressString);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestDelete()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                ProjectController projectController = scope.ServiceProvider.GetRequiredService<ProjectController>();

                const string projectNameString = "Test project";
                const string testAddressString = "Test address";

                Location location = new Location
                {
                    Address = testAddressString
                };

                Project testProject = projectManager.CreateProject(projectNameString, location, provider.GetTestOrg);

                projectController.Delete(testProject.Id);
                IIncludableQueryable<Project, Location> projects = projectManager
                                                                   .GetProjects()
                                                                   .Include(e => e.Location);

                Assert.DoesNotContain(projects, e => e.Name == projectNameString && e.Location.Address == testAddressString);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestDetails()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                ProjectController projectController = scope.ServiceProvider.GetRequiredService<ProjectController>();

                const string projectNameString = "Test project";
                const string projectAddressString = "Test address";

                Location location = new Location
                {
                    Address = projectAddressString
                };

                Project testProject = projectManager
                    .CreateProject(projectNameString, location, provider.GetTestOrg);

                projectManager.AssignAssetToProject(testProject, provider.GetTestUser);

                ViewResult result = projectController.Details(testProject.Id);

                ProjectDetailsModel project = Assert.IsType<ProjectDetailsModel>(result.Model);

                Assert.Equal(projectNameString, project.Name);
                Assert.Equal(projectAddressString, project.Address);
                Assert.Contains(project.Users, e => e.Id == provider.GetTestUser.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestEdit()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            Project testProject;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                ProjectController projectController = scope.ServiceProvider.GetRequiredService<ProjectController>();
                const string projectNameString1 = "Test project";
                const string projectAddressString1 = "Test address";

                Location location = new Location
                {
                    Address = projectAddressString1
                };

                testProject = projectManager
                    .CreateProject(projectNameString1, location, provider.GetTestOrg);

                Assert.Equal(projectNameString1, testProject.Name);
                Assert.Equal(projectAddressString1, testProject.Location.Address);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                const string projectNameString2 = "Other test project";
                const string projectAddressString2 = "Other test address";
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                ProjectController projectController = scope.ServiceProvider.GetRequiredService<ProjectController>();
                ViewResult result = projectController.Edit(testProject.Id);
                ProjectEditModel projectModel = Assert.IsType<ProjectEditModel>(result.Model);

                projectModel.Name = projectNameString2;
                projectModel.Address = projectAddressString2;
                projectModel.Id = testProject.Id;

                projectController.Edit(projectModel);

                Project project = projectManager
                                  .GetProjects()
                                  .GetEntity<Project, ProjectId>(testProject);

                Assert.Equal(projectNameString2, project.Name);
                Assert.Equal(projectAddressString2, project.Location.Address);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestIndex()
        {
            ConstructionTestProvider provider = new ConstructionTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                SavingProjectManager projectManager = scope.ServiceProvider.GetRequiredService<SavingProjectManager>();
                ProjectController projectController = scope.ServiceProvider.GetRequiredService<ProjectController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg);
                const string projectNameString = "Test project";
                const string testAddressString = "Test address";

                Location location = new Location
                {
                    Address = testAddressString
                };

                Project testProject = projectManager.CreateProject(projectNameString, location, provider.GetTestOrg);
                projectManager.AssignAssetToProject(testProject, provider.GetTestUser);

                ViewResult result = projectController.Index(null);
                List<Project> projects = Assert.IsType<List<Project>>(result.Model);

                Assert.Contains(projects, e => e.Name == projectNameString && e.Location.Address == testAddressString);
            }

            provider.Dispose();
        }
    }
}