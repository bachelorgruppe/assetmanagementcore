using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.Loader;
using AssetManagementPlatform.Core;
using AssetManagementPlatform.Core.DB;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AssetManagementPlatform.Frontend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IMvcBuilder builder = services.AddControllersWithViews();
            builder.AddRazorRuntimeCompilation();

            services.AddHttpContextAccessor();

            //services.AddDbContext<DataContext>(optionsBuilder => optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=ScheduleAlgorithm;Trusted_Connection=True;"));
            services.AddDbContext<DataContext>(optionsBuilder => optionsBuilder.UseMySql("Server=localhost;Database=test;Uid=root;Pwd=8594;"));
            //services.AddDbContext<DataContext>(optionsBuilder => optionsBuilder.UseInMemoryDatabase("db"));
            services.AddCoreServices();

            //string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //foreach (string dll in Directory.GetFiles(path, "*Module*.dll"))
            //{
            //    Assembly loadFile = AssemblyLoadContext.Default.LoadFromAssemblyPath(dll);
            //    builder.AddApplicationPart(loadFile);
            //
            //    AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(dll));
            //}

            services.AddModuleSetups(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        [UsedImplicitly]
        public static void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
            {
                app.UseExceptionHandler("/Home/Error");

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStatusCodePages(async context =>
            {
                var response = context.HttpContext.Response;

                if (response.StatusCode == (int)HttpStatusCode.Unauthorized ||
                    response.StatusCode == (int)HttpStatusCode.Forbidden)
                    response.Redirect("/Login");
            });

            app.UseStaticFiles();

            app.UseRouting();

            app.AddModuleSetups();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                                             "default",
                                             "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}