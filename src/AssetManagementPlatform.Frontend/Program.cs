using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.Core;
using AssetManagementPlatform.Core.DB;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace AssetManagementPlatform.Frontend
{
    public sealed class Program
    {
        public static async Task Main(string[] args)
        {
            IHostBuilder builder = CreateHostBuilder(args);
            using (IHost webHost = builder.Build())
            {
                using (IServiceScope scope = webHost.Services.CreateScope())
                {
                    IHostEnvironment environment = scope.ServiceProvider.GetRequiredService<IHostEnvironment>();

                    ILogger<Program> logger = scope.ServiceProvider.GetService<ILogger<Program>>();
                    DataContext db = scope.ServiceProvider.GetService<DataContext>();

                    if (!db.Database.IsInMemory())
                    {
                        // Migrate DB
                        List<string> pending = db.Database.GetPendingMigrations().ToList();

                        if (pending.Count > 0)
                        {
                            logger.LogInformation("Pending migrations: {Migrations}", pending);
                            db.Database.Migrate();

                            //if (environment.IsDevelopment())
                                scope.ServiceProvider.SeedContexts();
                        }
                    }
                    else
                    {
                        //if (environment.IsDevelopment())
                            scope.ServiceProvider.SeedContexts();
                    }
                }

                await webHost.RunAsync();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                       .ConfigureWebHostDefaults(webBuilder =>
                       {
                           webBuilder.UseStaticWebAssets();
                           webBuilder.UseStartup<Startup>();
                       });
        }
    }
}