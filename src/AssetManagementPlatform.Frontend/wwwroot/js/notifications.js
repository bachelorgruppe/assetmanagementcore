﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/notifications").build();

connection.on("NotificationUpdate",
    function (eventType, data) {
        console.log("event:" + eventType + ": " + data);

        var body;

        switch (eventType) {
            case "ReportBrokenEvent":
                body = "Tool reported broken by " + data.userName + '<br><a href="/Tool/Details/' + data.toolId + '">See tool<a/>';
                break;
            case "ReportFixedEvent":
                body = "Tool Fixed by " + data.userName + '<br><a href="/Tool/Details/' + data.toolId + '">See tool<a/>';
                break;
            case "AssignmentRequestEvent":
                body = "Tool requested by " + data.userName + '<br><a href="/Tool/Details/' + data.toolId + '">See tool<a/>';
                break;
            case "AssignmentConfirmationEvent":
                body = "Transfer request confirmed by " + data.userName + '<br><a href="/Tool/Details/' + data.toolId + '">See tool<a/>';
                break;
            case "NewMessage":
                body = "Message from: " + data.userName + '<br>' + data.text;
                break;
        }

        var id = Math.floor(Math.random() * 100000);

        var string = '<div id="' +
            id +
            '" class="toast ml-auto" data-delay="5000" data-autohide="true" style="z-index: 9999">' +
            '  <div class="toast-header">' +
            '    <strong class="mr-auto">New Message</strong>' +
            "</div>" +
            '  <div class="toast-body">' +
            (body) +
            "." +
            "  </div>" +
            "</div>";
        $("#alerts").append(string);

        $("#" + id).toast("show");
        $("#" + id).on("hidden.bs.toast",
            function () {
                $("#" + id).remove();
            });
    }
);

connection.start();