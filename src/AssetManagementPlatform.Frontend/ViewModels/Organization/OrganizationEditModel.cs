﻿using System;

namespace AssetManagementPlatform.Frontend.ViewModels.Organization
{
    public class OrganizationEditModel : OrganizationCreateModel
    {
        public Guid Id { get; set; }
    }
}