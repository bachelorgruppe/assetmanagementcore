﻿namespace AssetManagementPlatform.Frontend.ViewModels.Organization
{
    public class OrganizationCreateModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}