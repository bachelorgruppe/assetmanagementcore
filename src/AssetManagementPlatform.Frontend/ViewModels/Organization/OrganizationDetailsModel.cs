﻿using System;

namespace AssetManagementPlatform.Frontend.ViewModels.Organization
{
    public class OrganizationDetailsModel : OrganizationCreateModel
    {
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}