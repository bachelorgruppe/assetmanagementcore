﻿using System.Diagnostics;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.Web;
using AssetManagementPlatform.Frontend.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.Frontend.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IHomeRedirectionService _redirectionService;

        public HomeController(IHomeRedirectionService redirectionService)
        {
            _redirectionService = redirectionService;
        }

        public IActionResult Index()
        {
            return RedirectToAction(_redirectionService.Action, _redirectionService.Controller);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}