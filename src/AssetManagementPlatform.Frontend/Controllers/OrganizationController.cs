﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Authorization;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.Core.Web;
using AssetManagementPlatform.Frontend.ViewModels.Organization;
using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.Frontend.Controllers
{
    public class OrganizationController : BaseController
    {
        private readonly SavingOrganizationManager _organizationManager;

        public OrganizationController(SavingOrganizationManager organizationManager)
        {
            _organizationManager = organizationManager;
        }

        public IActionResult Index()
        {
            if (!User.GetUserPermissions().ManageOrganizations)
                return RedirectToAction("Details", new { Id = User.GetUserOrganizationId().Id });

            return View(_organizationManager.GetOrganizations().ToList());
        }

        [RoleAuthorize(nameof(BaseSitePermissions.ManageOrganizations))]
        public IActionResult Create()
        {
            return View();
        }

        [RoleAuthorize(nameof(BaseSitePermissions.ManageOrganizations))]
        [HttpPost]
        public ActionResult Create(OrganizationCreateModel createModel)
        {
            Organization org = _organizationManager.CreateOrganization(createModel.Name);
            org.Location = new Location { Address = createModel.Address };

            _organizationManager.EditOrganization(org);

            return RedirectToAction("Index");
        }

        [RoleAuthorize(nameof(BaseSitePermissions.ManageOrganizations))]
        public IActionResult Edit()
        {
            return View();
        }

        public IActionResult Details(Guid id)
        {
            Organization org = _organizationManager.GetOrganizations().GetEntity(new OrganizationId(id));
            return View(new OrganizationDetailsModel
            {
                Name = org.Name,
                Address = org.Location.Address,
                CreatedOn = org.CreatedOn,
                UpdatedOn = org.UpdatedOn
            });
        }

        [RoleAuthorize(nameof(BaseSitePermissions.ManageOrganizations))]
        public ActionResult Delete(Guid id)
        {
            _organizationManager.RemoveOrganization(new OrganizationId(id));
            return RedirectToAction("Index");
        }

        [RoleAuthorize(nameof(BaseSitePermissions.ManageOrganizations))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OrganizationEditModel editModel)
        {
            if (!ModelState.IsValid)
                return View(editModel);

            Organization org = _organizationManager.GetOrganizations().GetEntity(new OrganizationId(editModel.Id));
            org.Name = editModel.Name;
            org.Location = new Location { Address = editModel.Address };

            _organizationManager.EditOrganization(org);

            return RedirectToAction("Index");
        }
    }
}