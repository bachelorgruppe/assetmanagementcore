﻿using AssetManagementPlatform.Core.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.Frontend.Controllers
{
    [AllowAnonymous]
    public class LoginController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>Allows us to login as test users</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Auth(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                Response.Cookies.Delete("access");
                return RedirectToAction("Index");
            }

            Response.Cookies.Append("access", id, new CookieOptions());
            return RedirectToAction("Index", "Home");
        }
    }
}