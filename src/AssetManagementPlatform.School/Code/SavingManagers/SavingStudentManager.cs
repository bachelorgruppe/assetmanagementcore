﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;

namespace AssetManagementPlatform.SchoolModule.Code.SavingManagers
{
    public class SavingStudentManager : BaseManager<SchoolContext>, IStudentManager
    {
        private readonly IStudentManager _manager;

        public SavingStudentManager(SchoolContext dbContext, IStudentManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<Student> GetStudents()
        {
            return _manager.GetStudents();
        }

        public Student CreateStudent(string firstName, string lastName, DateTime dateOfBirth)
        {
            Student student = _manager.CreateStudent(firstName, lastName, dateOfBirth);
            SaveContext();
            return student;
        }

        public void MarkAbsent(StudentId studentId, ClassroomId classroomId)
        {
            _manager.MarkAbsent(studentId, classroomId);
            SaveContext();
        }

        public void UnmarkAbsent(EventId eventId)
        {
            _manager.UnmarkAbsent(eventId);
            SaveContext();
        }

        public void DeleteStudent(StudentId studentId)
        {
            _manager.DeleteStudent(studentId);
            SaveContext();
        }

        public void EditStudent(Student student)
        {
            _manager.EditStudent(student);
            SaveContext();
        }

        public void AssignBook(BookId bookId, StudentId studentId)
        {
            _manager.AssignBook(bookId, studentId);
            SaveContext();
        }
        public IQueryable<StudentAbsentEvent> GetStudentAbsentEvents(StudentId studentId, DateTime fromDate, DateTime toDate)
        {
            return _manager.GetStudentAbsentEvents(studentId, fromDate, toDate);
        }
    }
}