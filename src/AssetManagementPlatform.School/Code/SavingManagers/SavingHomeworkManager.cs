﻿using System.Linq;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;

namespace AssetManagementPlatform.SchoolModule.Code.SavingManagers
{
    public class SavingHomeworkManager : BaseManager<SchoolContext>, IHomeworkManager
    {
        private readonly IHomeworkManager _manager;

        public SavingHomeworkManager(SchoolContext dbContext, IHomeworkManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<Homework> GetAllHomework()
        {
            return _manager.GetAllHomework();
        }

        public IQueryable<Homework> GetHomework(ClassroomId classroomId)
        {
            return _manager.GetHomework(classroomId);
        }

        public IQueryable<Homework> GetHomework(StudentId studentId)
        {
            return _manager.GetHomework(studentId);
        }

        public IQueryable<Homework> GetHomework(TeacherId teacherId)
        {
            return _manager.GetHomework(teacherId);
        }

        public IQueryable<Student> GetMissingSubmissions(HomeworkId homeworkId)
        {
            return _manager.GetMissingSubmissions(homeworkId);
        }

        public void CreateHomework(Homework homework)
        {
            _manager.CreateHomework(homework);
            SaveContext();
        }

        public void DeleteHomework(HomeworkId homeworkId)
        {
            _manager.DeleteHomework(homeworkId);
            SaveContext();
        }

        public void EditHomework(Homework homework)
        {
            _manager.EditHomework(homework);
            SaveContext();
        }

        public void SubmitHomework(HomeworkSubmission homeworkSubmission)
        {
            _manager.SubmitHomework(homeworkSubmission);
            SaveContext();
        }

        public IQueryable<HomeworkSubmissionEvent> GetSubmissions(HomeworkId homeworkId)
        {
            return _manager.GetSubmissions(homeworkId);
        }

        public HomeworkSubmissionEvent GetSubmissionFromStudent(HomeworkId homeworkId, StudentId studentId)
        {
            return _manager.GetSubmissionFromStudent(homeworkId, studentId);
        }
    }
}