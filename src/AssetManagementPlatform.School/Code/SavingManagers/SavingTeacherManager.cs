﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;

namespace AssetManagementPlatform.SchoolModule.Code.SavingManagers
{
    public class SavingTeacherManager : BaseManager<SchoolContext>, ITeacherManager
    {
        private readonly ITeacherManager _manager;

        public SavingTeacherManager(SchoolContext dbContext, ITeacherManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<Teacher> GetTeachers()
        {
            return _manager.GetTeachers();
        }

        public IQueryable<Teacher> GetAssignedTeachers(StudentId studentId)
        {
            return _manager.GetAssignedTeachers(studentId);
        }

        public Teacher Create(string firstName, string lastName, DateTime dateOfBirth)
        {
            Teacher teacher = _manager.Create(firstName, lastName, dateOfBirth);
            SaveContext();
            return teacher;
        }

        public void Delete(TeacherId teacherId)
        {
            _manager.Delete(teacherId);
            SaveContext();
        }

        public void Assign(Asset asset, Teacher teacher)
        {
            _manager.Assign(asset, teacher);
            SaveContext();
        }

        public void EditTeacher(Teacher teacher)
        {
            _manager.EditTeacher(teacher);
            SaveContext();
        }
    }
}