﻿using System.Linq;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;

namespace AssetManagementPlatform.SchoolModule.Code.SavingManagers
{
    public class SavingBookManager : BaseManager<SchoolContext>, IBookManager
    {
        private readonly IBookManager _manager;

        public SavingBookManager(SchoolContext dbContext, IBookManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<Book> GetBooks()
        {
            return _manager.GetBooks();
        }

        public Book CreateBook(string title, string author)
        {
            Book book = _manager.CreateBook(title, author);
            SaveContext();
            return book;
        }

        public void DeleteBook(BookId bookId)
        {
            _manager.DeleteBook(bookId);
            SaveContext();
        }

        public IQueryable<Book> GetStudentBooks(UserId userId)
        {
            return _manager.GetStudentBooks(userId);
        }

        public void EditBook(Book book)
        {
            _manager.EditBook(book);
            SaveContext();
        }
    }
}