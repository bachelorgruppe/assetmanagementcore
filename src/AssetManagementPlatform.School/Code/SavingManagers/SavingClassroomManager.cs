﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;

namespace AssetManagementPlatform.SchoolModule.Code.SavingManagers
{
    public class SavingClassroomManager : BaseManager<SchoolContext>, IClassroomManager
    {
        private readonly IClassroomManager _manager;

        public SavingClassroomManager(SchoolContext dbContext, IClassroomManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<Classroom> GetClassrooms(UserId userId)
        {
            return _manager.GetClassrooms(userId);
        }

        public IQueryable<Classroom> GetAllClassrooms()
        {
            return _manager.GetAllClassrooms();
        }

        public void AssignAsset(AssetId assetId, ClassroomId classroomId)
        {
            _manager.AssignAsset(assetId, classroomId);
            SaveContext();
        }

        public void UnassignAsset(AssetId assetId, ClassroomId classroomId)
        {
            _manager.UnassignAsset(assetId, classroomId);
            SaveContext();
        }

        public Classroom Create(string name)
        {
            Classroom classroom = _manager.Create(name);
            SaveContext();
            return classroom;
        }

        public void Delete(ClassroomId classroomId)
        {
            _manager.Delete(classroomId);
            SaveContext();
        }

        public void Edit(Classroom classroom)
        {
            _manager.Edit(classroom);
            SaveContext();
        }

        public void Assign(UserId userId, ClassroomId classroomId)
        {
            _manager.Assign(userId, classroomId);
            SaveContext();
        }

        public void UnAssign(UserId userId, ClassroomId classroomId)
        {
            _manager.UnAssign(userId, classroomId);
            SaveContext();
        }

        public IQueryable<Student> GetStudentsInClass(ClassroomId classroomId)
        {
            return _manager.GetStudentsInClass(classroomId);
        }

        public IQueryable<Teacher> GetTeachersInClass(ClassroomId classroomId)
        {
            return _manager.GetTeachersInClass(classroomId);
        }

        public IQueryable<StudentAbsentEvent> GetStudentAbsentEvents(ClassroomId classroomId, DateTime fromDate, DateTime toDate)
        {
            return _manager.GetStudentAbsentEvents(classroomId, fromDate, toDate);
        }
    }
}