﻿using System.Security.Claims;
using AssetManagementPlatform.Core.Authorization.Permissions;
using JetBrains.Annotations;

namespace AssetManagementPlatform.SchoolModule.Code.Authentication.Extensions
{
    [PublicAPI]
    public static class ClaimsPrincipalExtensions
    {
        public static SchoolSitePermissions GetSchoolUserPermissions([NotNull]this ClaimsPrincipal principal)
        {
            return SitePermissionsExtensions.CreateFromPermissionString<SchoolSitePermissions>(principal.FindFirst(AssetClaims.SchoolPermissions).Value);
        }

        public static bool TryGetSchoolUserPermissions([NotNull]this ClaimsPrincipal principal, out SchoolSitePermissions constructionSitePermissions)
        {
            constructionSitePermissions = null;
            Claim findFirst = principal.FindFirst(AssetClaims.SchoolPermissions);
            if (findFirst == null)
                return false;

            constructionSitePermissions = SitePermissionsExtensions.CreateFromPermissionString<SchoolSitePermissions>(findFirst.Value);
            return true;
        }
    }
}