﻿using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.SchoolModule.Code.Authentication
{
    public sealed class RoleAuthorizeSchoolAttribute : TypeFilterAttribute
    {
        public RoleAuthorizeSchoolAttribute(params string[] item)
            : base(typeof(AuthorizeActionFilter))
        {
            Arguments = new object[] { item };
        }
    }
}