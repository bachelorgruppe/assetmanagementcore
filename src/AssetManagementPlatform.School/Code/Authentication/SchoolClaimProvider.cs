﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AssetManagementPlatform.Core.Authentication;
using AssetManagementPlatform.Core.Authorization.Permissions;
using Microsoft.IdentityModel.JsonWebTokens;

namespace AssetManagementPlatform.SchoolModule.Code.Authentication
{
    public class SchoolClaimProvider : IDomainClaimProvider
    {
        public IEnumerable<Claim> GetClaims(string id)
        {
            if (id == "sch1")
            {
                const string userId = "292fa3ef-8617-4f30-ae30-5571fa0488e1";
                Claim[] claims =
                {
                    new Claim(ClaimTypes.NameIdentifier, userId),
                    new Claim(JwtRegisteredClaimNames.NameId, userId),
                    new Claim(JwtRegisteredClaimNames.Sub, userId),
                    new Claim(JwtRegisteredClaimNames.GivenName, "Manager"),
                    new Claim("middle_name", "Den"),
                    new Claim(JwtRegisteredClaimNames.FamilyName, "Store"),
                    new Claim(JwtRegisteredClaimNames.Email, "A@B.dk"),
                    new Claim(Core.Authentication.AssetClaims.OrganizationId, "391ab491-4233-4a8f-b9c6-4719dbb202d9"),
                    new Claim(Core.Authorization.AssetClaims.Permissions, new BaseSitePermissions { Message = true, CanSeeOrganizations = true }.GetPermissionsAsString()), new Claim(AssetClaims.SchoolPermissions, new SchoolSitePermissions
                    {
                        StudentCreate = true,
                        SeeOwnBooks = true,
                        SeeOwnClasses = true,
                        DeleteStudent = true,
                        EditStudent = true,
                        EditTeacher = true,
                        MarkAbsence = true,
                        DeleteTeacher = true,
                        SeeAllClasses = true,
                        CanCreateBook = true,
                        CanDeleteBook = true,
                        CanEditBook = true,
                        CanDeleteClass = true,
                        CanEditClass = true,
                        CanCreateClass = true,
                        AssignBooks = true,
                        AssignClasses = true,
                        SeeAllBooks = true
                    }.GetPermissionsAsString())
                };
                return claims;
            }

            if (id == "sch2")
            {
                const string userId = "c7f9d850-9383-4142-b782-d7d4c9e7a8f0";
                Claim[] claims =
                {
                    new Claim(ClaimTypes.NameIdentifier, userId),
                    new Claim(JwtRegisteredClaimNames.NameId, userId),
                    new Claim(JwtRegisteredClaimNames.Sub, userId),
                    new Claim(JwtRegisteredClaimNames.GivenName, "Teacher"),
                    new Claim("middle_name", "Den"),
                    new Claim(JwtRegisteredClaimNames.FamilyName, "Store"),
                    new Claim(JwtRegisteredClaimNames.Email, "A@B.dk"),
                    new Claim(Core.Authentication.AssetClaims.OrganizationId, "391ab491-4233-4a8f-b9c6-4719dbb202d9"),
                    new Claim(Core.Authorization.AssetClaims.Permissions, new BaseSitePermissions { Message = true, CanSeeOrganizations = true }.GetPermissionsAsString()), new Claim(AssetClaims.SchoolPermissions, new SchoolSitePermissions
                    {
                        SeeOwnBooks = true,
                        SeeOwnClasses = true,
                        IsTeacher = true,
                        CanDeleteBook = true,
                        MarkAbsence = true,
                        AssignBooks = true,
                        EditTeacher = true,
                        DeleteTeacher = true,
                        AssignClasses = true,
                        CanCreateBook = true,
                        CanEditBook = true,
                        DeleteStudent = true,
                        CanDeleteClass = true,
                        CanEditClass = true,
                        CanCreateClass = true,
                        EditStudent = true,
                    }.GetPermissionsAsString())
                };
                return claims;
            }

            if (id == "sch3")
            {
                const string userId = "ac7a7638-dc99-4ad1-be16-e56cfae4ca42";
                Claim[] claims =
                {
                    new Claim(ClaimTypes.NameIdentifier, userId),
                    new Claim(JwtRegisteredClaimNames.NameId, userId),
                    new Claim(JwtRegisteredClaimNames.Sub, userId),
                    new Claim(JwtRegisteredClaimNames.GivenName, "Student"),
                    new Claim("middle_name", "Den"),
                    new Claim(JwtRegisteredClaimNames.FamilyName, "Store"),
                    new Claim(JwtRegisteredClaimNames.Email, "A@B.dk"),
                    new Claim(Core.Authentication.AssetClaims.OrganizationId, "391ab491-4233-4a8f-b9c6-4719dbb202d9"),
                    new Claim(Core.Authorization.AssetClaims.Permissions, new BaseSitePermissions { Message = true, CanSeeOrganizations = true }.GetPermissionsAsString()),
                    new Claim(AssetClaims.SchoolPermissions, new SchoolSitePermissions
                    {
                        SeeOwnBooks = true,
                        SeeOwnClasses = true,
                        IsStudent = true
                    }.GetPermissionsAsString())
                };
                return claims;
            }

            return Enumerable.Empty<Claim>();
        }
    }
}