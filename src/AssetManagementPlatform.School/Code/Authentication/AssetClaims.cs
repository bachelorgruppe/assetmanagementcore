﻿namespace AssetManagementPlatform.SchoolModule.Code.Authentication
{
    public static class AssetClaims
    {
        public const string SchoolPermissions = "asset_permissions_school";
    }
}