﻿using System.Security.Claims;
using AssetManagementPlatform.Core.Authorization.Permissions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AssetManagementPlatform.SchoolModule.Code.Authentication
{
    public class AuthorizeActionFilter : IAuthorizationFilter
    {
        private readonly SchoolSitePermissions _item;

        public AuthorizeActionFilter(string[] item)
        {
            _item = SitePermissionsExtensions.CreateFromPermissionNames<SchoolSitePermissions>(item);
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            Claim claim = context.HttpContext.User.FindFirst(AssetClaims.SchoolPermissions);

            if (claim == null)
            {
                context.Result = new ForbidResult();
                return;
            }

            SchoolSitePermissions baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionString<SchoolSitePermissions>(claim.Value);
            bool isAuthorized = baseSitePermissions.HasAllPermissions(_item);

            if (!isAuthorized)
                context.Result = new ForbidResult();
        }
    }
}