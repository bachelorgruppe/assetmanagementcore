﻿using AssetManagementPlatform.Core.Authorization.Permissions;

namespace AssetManagementPlatform.SchoolModule.Code.Authentication
{
    public class SchoolSitePermissions : BasePermissions
    {
        [Order(1)]
        public bool StudentCreate { get; set; }

        [Order(2)]
        public bool SeeAllClasses { get; set; }

        [Order(3)]
        public bool SeeOwnClasses { get; set; }

        [Order(4)]
        public bool AssignClasses { get; set; }

        [Order(5)]
        public bool SeeAllBooks { get; set; }

        [Order(6)]
        public bool SeeOwnBooks { get; set; }

        [Order(7)]
        public bool AssignBooks { get; set; }

        [Order(8)]
        public bool EditTeacher { get; set; }

        [Order(9)]
        public bool DeleteTeacher { get; set; }

        [Order(10)]
        public bool EditStudent { get; set; }

        [Order(11)]
        public bool DeleteStudent { get; set; }

        [Order(12)]
        public bool IsTeacher { get; set; }

        [Order(13)]
        public bool IsStudent { get; set; }

        [Order(14)]
        public bool MarkAbsence { get; set; }

        [Order(15)]
        public bool CanCreateBook { get; set; }

        [Order(16)]
        public bool CanDeleteBook { get; set; }

        [Order(17)]
        public bool CanEditBook { get; set; }

        [Order(18)]
        public bool CanEditClass { get; set; }

        [Order(19)]
        public bool CanDeleteClass { get; set; }

        [Order(20)]
        public bool CanCreateClass { get; set; }
    }
}