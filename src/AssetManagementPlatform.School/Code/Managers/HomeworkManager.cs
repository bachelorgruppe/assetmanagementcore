﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule.Code.Managers
{
    public class HomeworkManager : BaseManager<SchoolContext>, IHomeworkManager
    {
        private readonly IAssetGroupManager _assetGroupManager;
        private readonly IEventManager _eventManager;
        private readonly IClassroomManager _classroomManager;

        public HomeworkManager(SchoolContext dbContext, IAssetGroupManager assetGroupManager, IEventManager eventManager, IClassroomManager classroomManager) : base(dbContext)
        {
            _assetGroupManager = assetGroupManager;
            _eventManager = eventManager;
            _classroomManager = classroomManager;
        }

        public IQueryable<Homework> GetAllHomework()
        {
            return Db.Homework.AsNoTracking();
        }

        public IQueryable<Homework> GetHomework(ClassroomId classroomId)
        {
            return _assetGroupManager.GetAssetsInGroup(classroomId).OfType<Homework>();
        }

        public IQueryable<Homework> GetHomework(StudentId studentId)
        {
            var groupsFromAsset = _assetGroupManager.GetGroupsFromAsset(studentId);
            var assetGroupAssets = groupsFromAsset.SelectMany(x => x.Assets)
                .Include(x => x.Asset)
                .Select(x => x.Asset);
            return assetGroupAssets.OfType<Homework>();
        }

        public IQueryable<Homework> GetHomework(TeacherId teacherId)
        {
            var groupsFromAsset = _assetGroupManager.GetGroupsFromAsset(teacherId);
            var assetGroupAssets = groupsFromAsset.SelectMany(x => x.Assets)
                .Include(x => x.Asset)
                .Select(x => x.Asset);
            return assetGroupAssets.OfType<Homework>();
        }

        public IQueryable<Student> GetMissingSubmissions(HomeworkId homeworkId)
        {
            var homework = Db.Homework.Include(x => x.Classroom).GetEntity(homeworkId);
            var submittedEvents = _eventManager.GetEventsByType<HomeworkSubmissionEvent>(homeworkId).Include(x => x.Student);
            var students = _classroomManager.GetStudentsInClass(homework.Classroom);
            return students.Except(submittedEvents.Select(x => x.Student));
        }

        public HomeworkSubmissionEvent GetSubmissionFromStudent(HomeworkId homeworkId, StudentId studentId)
        {
            return _eventManager.GetEventsByType<HomeworkSubmissionEvent>(homeworkId).FirstOrDefault(x => x.StudentId == studentId.Id);
        }

        public IQueryable<HomeworkSubmissionEvent> GetSubmissions(HomeworkId homeworkId)
        {
            var submittedEvents = _eventManager.GetEventsByType<HomeworkSubmissionEvent>(homeworkId)
                .Include(x => x.Student);
            return submittedEvents;
        }

        public void CreateHomework(Homework homework)
        {
            Db.Homework.Add(homework);
        }

        public void DeleteHomework(HomeworkId homeworkId)
        {
            Homework homework = Db.Homework.GetEntity(homeworkId);
            Db.Remove(homework);
        }

        public void EditHomework(Homework homework)
        {
            Db.Homework.Update(homework);
        }

        public void SubmitHomework(HomeworkSubmission homeworkSubmission)
        {
            _eventManager.CreateEvent(homeworkSubmission.Homework, new HomeworkSubmissionEvent
            {
                Answer = homeworkSubmission.Answer,
                StudentId = homeworkSubmission.StudentId
            });
        }
    }
}