﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule.Code.Managers
{
    public class StudentManager : BaseManager<SchoolContext>, IStudentManager
    {
        private readonly IAssetManager _assetManager;
        private readonly IEventManager _eventManager;

        public StudentManager(SchoolContext dbContext, IAssetManager assetManager, IEventManager eventManager) : base(dbContext)
        {
            _assetManager = assetManager;
            _eventManager = eventManager;
        }

        public IQueryable<Student> GetStudents()
        {
            return Db.Students.AsNoTracking();
        }

        public Student CreateStudent(string firstName, string lastName, DateTime dateOfBirth)
        {
            Student student = new Student
            {
                FirstName = firstName,
                LastName = lastName,
                DateOfBirth = dateOfBirth
            };
            _assetManager.CreateAsset(student);

            return student;
        }

        public void MarkAbsent(StudentId studentId, ClassroomId classroomId)
        {
            _eventManager.CreateEvent(studentId, new StudentAbsentEvent
            {
                ReportedByUserId = studentId.Id,
                ClassroomId = classroomId.Id
            });
        }

        public void UnmarkAbsent(EventId eventId)
        {
            _eventManager.CancelEvent(eventId);
        }

        public void DeleteStudent(StudentId studentId)
        {
            var student = Db.Students.GetEntity(studentId);
            Db.Remove(student);
        }

        public void EditStudent(Student student)
        {
            Db.Students.Update(student);
        }

        public void AssignBook(BookId bookId, StudentId studentId)
        {
            _assetManager.AssignAsset(bookId, studentId);
        }

        public IQueryable<StudentAbsentEvent> GetStudentAbsentEvents(StudentId studentId, DateTime fromDate, DateTime toDate)
        {
            return _eventManager.GetEventsByType<StudentAbsentEvent>(studentId)
                .Where(x => x.CreatedOn.Date >= fromDate
                            && x.CreatedOn.Date <= toDate
                            && x.IsActive);
        }
    }
}