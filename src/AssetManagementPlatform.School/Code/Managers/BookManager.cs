﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule.Code.Managers
{
    internal class BookManager : BaseManager<SchoolContext>, IBookManager
    {
        private readonly IAssetManager _assetManager;
        private readonly IEventManager _eventManager;

        public BookManager(SchoolContext dbContext, IAssetManager assetManager, IEventManager eventManager) : base(dbContext)
        {
            _assetManager = assetManager;
            _eventManager = eventManager;
        }

        public IQueryable<Book> GetBooks()
        {
            return Db.Books.AsNoTracking();
        }

        public IQueryable<Book> GetStudentBooks(UserId userId)
        {
            return _eventManager.GetAssignedAssets<Book>(userId);
        }

        public Book CreateBook(string title, string author)
        {
            Book book = new Book
            {
                Title = title,
                Author = author,
            };

            _assetManager.CreateAsset(book);
            return book;
        }

        public void DeleteBook(BookId bookId)
        {
            Book book = Db.Books.GetEntity(bookId);
            Db.Books.Remove(book);
        }

        public void EditBook(Book book)
        {
            Db.Books.Update(book);
        }
    }
}