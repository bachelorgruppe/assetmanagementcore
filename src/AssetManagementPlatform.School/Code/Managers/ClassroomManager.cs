﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule.Code.Managers
{
    public class ClassroomManager : BaseManager<SchoolContext>, IClassroomManager
    {
        private readonly IAssetGroupManager _assetGroupManager;
        private readonly IEventManager _eventManager;

        public ClassroomManager(SchoolContext dbContext, IAssetGroupManager assetGroupManager, IEventManager eventManager) : base(dbContext)
        {
            _assetGroupManager = assetGroupManager;
            _eventManager = eventManager;
        }

        public IQueryable<Classroom> GetClassrooms(UserId userId)
        {
            IQueryable<AssetGroupAsset> assetGroupAssets = GetAllClassrooms().Include(c => c.Assets).SelectMany(classroom => classroom.Assets);
            return assetGroupAssets.Where(asset => asset.AssetId == userId.Id).Select(asset => asset.AssetGroup).OfType<Classroom>();
        }

        public IQueryable<Classroom> GetAllClassrooms()
        {
            return Db.Classrooms.AsNoTracking();
        }

        public void AssignAsset(AssetId assetId, ClassroomId classroomId)
        {
            _assetGroupManager.AddAssetToGroup(classroomId, assetId);
        }

        public void UnassignAsset(AssetId assetId, ClassroomId classroomId)
        {
            _assetGroupManager.RemoveAssetFromGroup(classroomId, assetId);
        }

        public IQueryable<Student> GetStudentsInClass(ClassroomId classroomId)
        {
            return _assetGroupManager.GetAssetsInGroup(classroomId).OfType<Student>();
        }
        public IQueryable<Teacher> GetTeachersInClass(ClassroomId classroomId)
        {
            return _assetGroupManager.GetAssetsInGroup(classroomId).OfType<Teacher>();
        }

        public IQueryable<StudentAbsentEvent> GetStudentAbsentEvents(ClassroomId classroomId, DateTime fromDate, DateTime toDate)
        {
            return _eventManager.GetEventsByType<StudentAbsentEvent>()
                .Where(x => x.ClassroomId == classroomId.Id 
                            && x.CreatedOn.Date >= fromDate && x.CreatedOn.Date <= toDate
                            && x.IsActive);
        }

        public Classroom Create(string name)
        {
            Classroom classroom = new Classroom();
            classroom.Name = name;
            _assetGroupManager.CreateAssetGroup(classroom);
            return classroom;
        }

        public void Delete(ClassroomId classroomId)
        {
            var classroom = Db.AssetGroups.GetEntity(classroomId);
            Db.AssetGroups.Remove(classroom);
        }

        public void Edit(Classroom classroom)
        {
            Db.Classrooms.Update(classroom);
        }

        public void Assign(UserId userId, ClassroomId classroomId)
        {
            _assetGroupManager.AddAssetToGroup(classroomId, userId);
        }

        public void UnAssign(UserId userId, ClassroomId classroomId)
        {
            _assetGroupManager.RemoveAssetFromGroup(classroomId, userId);
        }
    }
}