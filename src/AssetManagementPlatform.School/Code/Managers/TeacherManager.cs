﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule.Code.Managers
{
    public class TeacherManager : BaseManager<SchoolContext>, ITeacherManager
    {
        private readonly IAssetManager _assetManager;
        private readonly IClassroomManager _classroomManager;

        public TeacherManager(SchoolContext dbContext, IAssetManager assetManager, IClassroomManager classroomManager) : base(dbContext)
        {
            _assetManager = assetManager;
            _classroomManager = classroomManager;
        }

        public IQueryable<Teacher> GetTeachers()
        {
            return Db.Teachers.AsNoTracking();
        }

        public Teacher Create(string firstName, string lastName, DateTime dateOfBirth)
        {
            Teacher teacher = new Teacher
            {
                FirstName = firstName,
                LastName = lastName,
                DateOfBirth = dateOfBirth
            };
            _assetManager.CreateAsset(teacher);

            return teacher;
        }

        public void Delete(TeacherId teacherId)
        {
            Teacher teacher = Db.Teachers.GetEntity(teacherId);
            Db.Teachers.Remove(teacher);
        }

        public void Assign(Asset asset, Teacher teacher)
        {
            _assetManager.AssignAsset(asset, teacher);
        }

        public void EditTeacher(Teacher teacher)
        {
            Db.Teachers.Update(teacher);
        }

        public IQueryable<Teacher> GetAssignedTeachers(StudentId studentId)
        {
            // TODO: Make IQueryable
            var classes = _classroomManager.GetClassrooms(studentId)
                .Include(x=>x.Assets)
                .ThenInclude(x=>x.Asset);

            return classes.SelectMany(x => x.Assets).Select(x => x.Asset).OfType<Teacher>();
        }
    }
}