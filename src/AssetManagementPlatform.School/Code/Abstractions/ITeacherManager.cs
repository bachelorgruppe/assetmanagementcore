﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using JetBrains.Annotations;

namespace AssetManagementPlatform.SchoolModule.Code.Abstractions
{
    public interface ITeacherManager
    {
        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Teacher> GetTeachers();

        Teacher Create(string firstName, string lastName, DateTime dateOfBirth);
        void Delete([NotNull]TeacherId teacherId);
        void Assign([NotNull]Asset asset, [NotNull]Teacher teacher);
        void EditTeacher([NotNull]Teacher teacher);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Teacher> GetAssignedTeachers([NotNull]StudentId studentId);
    }
}