﻿using System.Linq;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;
using JetBrains.Annotations;

namespace AssetManagementPlatform.SchoolModule.Code.Abstractions
{
    public interface IHomeworkManager
    {
        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Homework> GetAllHomework();

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Homework> GetHomework([NotNull]ClassroomId classroomId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Homework> GetHomework([NotNull]StudentId studentId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Homework> GetHomework([NotNull]TeacherId teacherId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Student> GetMissingSubmissions([NotNull]HomeworkId homeworkId);

        void CreateHomework([NotNull]Homework homework);
        void DeleteHomework([NotNull]HomeworkId homeworkId);
        void EditHomework([NotNull]Homework homework);
        void SubmitHomework([NotNull]HomeworkSubmission homeworkSubmission);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<HomeworkSubmissionEvent> GetSubmissions([NotNull]HomeworkId homeworkId);

        HomeworkSubmissionEvent GetSubmissionFromStudent([NotNull]HomeworkId homeworkId, [NotNull]StudentId studentId);
    }
}