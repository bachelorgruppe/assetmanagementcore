﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;
using JetBrains.Annotations;

namespace AssetManagementPlatform.SchoolModule.Code.Abstractions
{
    public interface IClassroomManager
    {
        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Classroom> GetClassrooms([NotNull]UserId userId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Classroom> GetAllClassrooms();

        void AssignAsset([NotNull]AssetId assetId, [NotNull]ClassroomId classroomId);
        void UnassignAsset([NotNull]AssetId assetId, [NotNull]ClassroomId classroomId);
        Classroom Create(string name);
        void Delete([NotNull]ClassroomId classroomId);
        void Edit([NotNull]Classroom classroom);
        void Assign([NotNull]UserId userId, [NotNull]ClassroomId classroomId);
        void UnAssign([NotNull]UserId userId, [NotNull]ClassroomId classroomId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Student> GetStudentsInClass([NotNull]ClassroomId classroomId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Teacher> GetTeachersInClass([NotNull]ClassroomId classroomId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<StudentAbsentEvent> GetStudentAbsentEvents([NotNull]ClassroomId classroomId, DateTime fromDate, DateTime toDate);
    }
}