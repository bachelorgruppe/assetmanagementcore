﻿using System.Linq;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using JetBrains.Annotations;

namespace AssetManagementPlatform.SchoolModule.Code.Abstractions
{
    public interface IBookManager
    {
        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Book> GetBooks();

        Book CreateBook(string title, string author);
        void DeleteBook([NotNull]BookId bookId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Book> GetStudentBooks([NotNull]UserId userId);

        void EditBook([NotNull]Book book);
    }
}