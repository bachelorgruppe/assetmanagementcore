﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;
using JetBrains.Annotations;

namespace AssetManagementPlatform.SchoolModule.Code.Abstractions
{
    public interface IStudentManager
    {
        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Student> GetStudents();

        Student CreateStudent(string firstName, string lastName, DateTime dateOfBirth);
        void MarkAbsent([NotNull]StudentId studentId, [NotNull]ClassroomId classroomId);
        void UnmarkAbsent([NotNull]EventId eventId);
        void DeleteStudent([NotNull]StudentId studentId);
        void EditStudent([NotNull]Student student);
        void AssignBook([NotNull]BookId bookId, [NotNull]StudentId studentId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<StudentAbsentEvent> GetStudentAbsentEvents([NotNull]StudentId studentId, DateTime fromDate, DateTime toDate);
    }
}