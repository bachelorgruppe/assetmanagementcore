﻿using System;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities;

namespace AssetManagementPlatform.SchoolModule.Code.IdHelpers
{
    public class ClassroomId : IHelperId
    {
        public ClassroomId(Guid assetId)
        {
            Id = assetId;
        }

        public Guid Id { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator ClassroomId(Classroom classroom)
        {
            return new ClassroomId(classroom.Id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator ClassroomId(AssetGroupId groupId)
        {
            return new ClassroomId(groupId.Id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator AssetGroupId(ClassroomId classroomId)
        {
            return new AssetGroupId(classroomId.Id);
        }
    }
}