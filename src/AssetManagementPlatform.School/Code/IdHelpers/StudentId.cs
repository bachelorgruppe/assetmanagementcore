﻿using System;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;

namespace AssetManagementPlatform.SchoolModule.Code.IdHelpers
{
    public class StudentId : IHelperId
    {
        public StudentId(UserId assetId)
        {
            Id = assetId.Id;
        }

        public StudentId(Guid assetId)
        {
            Id = assetId;
        }

        public Guid Id { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator StudentId(Asset asset)
        {
            return new StudentId(asset.Id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator UserId(StudentId studentId)
        {
            return new UserId(studentId.Id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator AssetId(StudentId studentId)
        {
            return new AssetId(studentId.Id);
        }
    }
}