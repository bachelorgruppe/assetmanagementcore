﻿using System;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;

namespace AssetManagementPlatform.SchoolModule.Code.IdHelpers
{
    public class HomeworkId : IHelperId
    {
        public HomeworkId(Guid assetId)
        {
            Id = assetId;
        }

        public Guid Id { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator HomeworkId(Asset asset)
        {
            return new HomeworkId(asset.Id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator AssetId(HomeworkId studentId)
        {
            return new AssetId(studentId.Id);
        }
    }
}