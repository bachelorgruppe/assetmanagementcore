﻿using System;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;

namespace AssetManagementPlatform.SchoolModule.Code.IdHelpers
{
    public class TeacherId : IHelperId
    {
        public TeacherId(Guid assetId)
        {
            Id = assetId;
        }

        public TeacherId(UserId userId)
        {
            Id = userId.Id;
        }

        public Guid Id { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator TeacherId(Asset asset)
        {
            return new TeacherId(asset.Id);
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator UserId(TeacherId teacherId)
        {
            return new UserId(teacherId.Id);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator AssetId(TeacherId teacherId)
        {
            return new AssetId(teacherId.Id);
        }
    }
}