﻿using System.Security.Claims;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.Authentication;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.SignalR;
using AssetManagementPlatform.Core.Web.Navbar;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.Managers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.JsonWebTokens;

namespace AssetManagementPlatform.SchoolModule
{
    [PublicAPI]
    public class Setup : ISetup
    {
        public void AddServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SchoolContext>(optionsBuilder => optionsBuilder.UseMySql(configuration.GetConnectionString("SchoolContext")));
            //services.AddDbContext<SchoolContext>(optionsBuilder => optionsBuilder.UseInMemoryDatabase("db"));
            services.AddScoped(provider => (DataContext)provider.GetService<SchoolContext>());

            services.AddScoped<IDbContextSeed, DbSchoolSeeder>();

            services.AddScoped<IBookManager, BookManager>();
            services.AddScoped<SavingBookManager>();
            services.AddScoped<IStudentManager, StudentManager>();
            services.AddScoped<SavingStudentManager>();
            services.AddScoped<IClassroomManager, ClassroomManager>();
            services.AddScoped<SavingClassroomManager>();
            services.AddScoped<ITeacherManager, TeacherManager>();
            services.AddScoped<SavingTeacherManager>();
            services.AddScoped<IHomeworkManager, HomeworkManager>();
            services.AddScoped<SavingHomeworkManager>();

            services.AddScoped<INavBarService, DefaultNavBarService>(provider =>
            {
                IHttpContextAccessor httpContextAccessor = provider.GetRequiredService<IHttpContextAccessor>();
                Claim claim = httpContextAccessor.HttpContext.User.FindFirst(JwtRegisteredClaimNames.GivenName);

                string name = claim != null ? claim.Value : "NoUser";

                NavBarItem[] menuItems =
                {
                    new NavBarItem("Home", "Home", "Index"),
                    new NavBarItem("Books", "Book", "Index"),
                    new NavBarItem("Classes", "Classroom", "Index"),
                    new NavBarItem("Teachers", "Teacher", "Index"),
                    new NavBarItem("Students", "Student", "Index"),
                    new NavBarItem("Assignments", "Homework", "Index"),
                    new NavBarItem("Messages", "Message", "Index", true),
                    new NavBarItem(name, "Login", "Index", true)
                };

                return new DefaultNavBarService(menuItems);
            });

            services.AddSingleton<IDomainClaimProvider, SchoolClaimProvider>();

            services.AddSingleton<IHomeRedirectionService, DefaultRedirectionService>(provider => new DefaultRedirectionService("School", "Index"));

            services.AddSignalRSetup();

            services.AddAuthentication();
            services.AddAuthorization();
            services.UseFakeAuth();
        }

        public void AddSetup(IApplicationBuilder builder)
        {
            builder.UseAuthentication();
            builder.UseAuthorization();

            builder.AddSignalRSetup();
        }
    }
}