﻿namespace AssetManagementPlatform.SchoolModule.ViewModels.Book
{
    public class BookCreateModel
    {
        public string Author { get; set; }
        public string Title { get; set; }

    }
}