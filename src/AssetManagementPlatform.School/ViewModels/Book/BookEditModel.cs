﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Book
{
    public class BookEditModel : BookCreateModel
    {
        public Guid Id { get; set; }

        public IList<SelectListItem> StudentOptions { get; set; }
        public Guid SelectedStudentId { get; set; }
    }
}