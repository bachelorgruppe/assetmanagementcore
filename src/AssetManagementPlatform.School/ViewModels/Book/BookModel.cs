﻿using AssetManagementPlatform.Core.DB.Entities;
using JetBrains.Annotations;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Book
{
    public class BookModel
    {
        [CanBeNull]
        public User AssignedUser { get; set; }

        public Entities.Book Book { get; set; }
    }
}