﻿using System;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Shared
{
    public class ItemSelection
    {
        public bool IsSelected { set; get; }
        public string Name { set; get; }
        public Guid Id { set; get; }
    }
}
