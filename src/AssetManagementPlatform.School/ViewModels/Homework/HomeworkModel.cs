﻿using System;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Homework
{
    public class HomeworkModel
    {
        public Entities.Homework Homework { get; set; }
        public DateTime? SubmissionDate { get; set; }
    }
}
