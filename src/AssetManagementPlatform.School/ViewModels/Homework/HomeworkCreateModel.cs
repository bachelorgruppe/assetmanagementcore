﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Homework
{
    public class HomeworkCreateModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public Guid ClassroomId { get; set; }
        public Entities.Classroom Classroom { get; set; }
        public IList<SelectListItem> ClassroomSelections { get; set; }
    }
}