﻿using System;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Homework
{
    public class HomeworkEditModel : HomeworkCreateModel
    {
        public Guid Id { get; set; }
    }
}