﻿using System;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Homework
{
    public class HomeworkDetailsModelStudent : HomeworkCreateModel
    {
        public DateTime? SubmissionDate { get; set; }
        public Guid Id { get; set; }
    }
}