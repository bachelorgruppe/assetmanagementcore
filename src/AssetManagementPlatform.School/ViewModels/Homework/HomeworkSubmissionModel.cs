﻿namespace AssetManagementPlatform.SchoolModule.ViewModels.Homework
{
    public class HomeworkSubmissionModel
    {
        public string Answer { get; set; }
    }
}