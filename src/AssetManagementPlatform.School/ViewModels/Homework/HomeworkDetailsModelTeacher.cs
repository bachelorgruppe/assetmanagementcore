﻿using System;
using System.Collections.Generic;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Homework
{
    public class HomeworkDetailsModelTeacher : HomeworkCreateModel
    {
        public ICollection<Entities.Student> MissingSubmissions { get; set; }
        public Guid Id { get; set; }
    }
}