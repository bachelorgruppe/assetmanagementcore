﻿using System;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Student
{
    public class StudentEditModel : StudentCreateModel
    {
        public Guid Id { get; set; }
    }
}