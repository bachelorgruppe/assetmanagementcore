﻿using System.Collections.Generic;
using AssetManagementPlatform.SchoolModule.Entities.Events;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Student
{
    public class StudentDetailsModel : StudentCreateModel
    {
        public string FullName => $"{Firstname} {LastName}";
        public IList<Entities.Classroom> AssignedClassrooms { get; set; }
        public IList<StudentAbsentEvent> AbsentStudentEvents { get; set; }
    }
}