﻿using System.Collections.Generic;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Entities.Events;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Classroom.Model
{
    public class AbsentModel
    {
        public Entities.Student Student { get; set; }
        public StudentId StudentId { get; set; }
        public IList<StudentAbsentEvent> AbsentEvents { get; set; }
    }
}
