﻿using System.Collections.Generic;
using AssetManagementPlatform.SchoolModule.ViewModels.Shared;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Classroom
{
    public class ClassroomCreateModel
    {
        public string Name { get; set; }
        public IList<ItemSelection> StudentSelections { get; set; }
        public IList<ItemSelection> TeacherSelections { get; set; }
    }
}