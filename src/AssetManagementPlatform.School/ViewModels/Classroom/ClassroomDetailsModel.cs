﻿using System;
using System.Collections.Generic;
using AssetManagementPlatform.SchoolModule.Entities.Events;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Classroom
{
    public class ClassroomDetailsModel : ClassroomCreateModel
    {
        public Guid Id { get; set; }
        public IList<Entities.Student> AssignedStudents { get; set; }
        public IList<StudentAbsentEvent> AbsentStudentEvents { get; set; }
        public IList<Entities.Teacher> AssignedTeachers { get; set; }
    }
}