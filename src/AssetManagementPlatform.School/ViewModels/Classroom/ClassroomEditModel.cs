﻿using System;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Classroom
{
    public class ClassroomEditModel : ClassroomCreateModel
    {
        public Guid Id { get; set; }
    }
}