﻿using System;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Teacher
{
    public class TeacherEditModel : TeacherCreateModel
    {
        public Guid Id { get; set; }
    }
}