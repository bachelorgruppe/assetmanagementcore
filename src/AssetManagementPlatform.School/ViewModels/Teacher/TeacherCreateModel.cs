﻿using System;
using System.Collections.Generic;
using AssetManagementPlatform.SchoolModule.ViewModels.Shared;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Teacher
{
    public class TeacherCreateModel
    {
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public IList<ItemSelection> ClassroomSelections { get; set; }
    }
}