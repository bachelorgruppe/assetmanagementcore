﻿using System.Collections.Generic;

namespace AssetManagementPlatform.SchoolModule.ViewModels.Teacher
{
    public class TeacherDetailsModel : TeacherCreateModel
    {
        public string FullName => $"{Firstname} {LastName}";
        public IList<Entities.Classroom> AssignedClassrooms { get; set; }
    }
}