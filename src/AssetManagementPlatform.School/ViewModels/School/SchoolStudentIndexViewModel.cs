﻿using System.Collections.Generic;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;

namespace AssetManagementPlatform.SchoolModule.ViewModels.School
{
    public class SchoolStudentIndexViewModel
    {
        public List<Entities.Classroom> Classrooms { get; set; }
        public List<ReservationEvent> ReservationEvents { get; set; }
        public List<Entities.Book> Books { get; set; }
        public List<Message> Messages { get; set; }
    }
}