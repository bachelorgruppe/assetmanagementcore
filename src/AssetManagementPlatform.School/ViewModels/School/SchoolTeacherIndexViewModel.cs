﻿using System.Collections.Generic;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;

namespace AssetManagementPlatform.SchoolModule.ViewModels.School
{
    public class SchoolTeacherIndexViewModel
    {
        public List<Entities.Classroom> Classrooms { get; set; }
        public List<Message> Messages { get; set; }
        public List<ReservationEvent> ReservationEvents { get; set; }
    }
}