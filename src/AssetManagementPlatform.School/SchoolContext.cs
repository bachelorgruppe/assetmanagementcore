﻿using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.Entities.Events;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule
{
    public class SchoolContext : DataContext
    {
        public SchoolContext(DbContextOptions<DataContext> dbContextOptions) : base(dbContextOptions) { }
        public DbSet<Student> Students { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Homework> Homework { get; set; }
        public DbSet<Classroom> Classrooms { get; set; }
        public DbSet<Teacher> Teachers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Book>()
                        .HasBaseType<Asset>();

            modelBuilder.Entity<Homework>()
                        .HasBaseType<Asset>();

            modelBuilder.Entity<Student>()
                        .HasBaseType<User>();

            modelBuilder.Entity<Classroom>()
                        .HasBaseType<AssetGroup>();

            modelBuilder.Entity<StudentAbsentEvent>()
                        .HasBaseType<Event>();

            modelBuilder.Entity<HomeworkSubmissionEvent>()
                        .HasBaseType<Event>();
        }
    }
}