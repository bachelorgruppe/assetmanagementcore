﻿using System;

namespace AssetManagementPlatform.SchoolModule.Entities
{
    public class HomeworkSubmission
    {
        public Homework Homework { get; set; }
        public string Answer { get; set; }
        public Guid StudentId { get; set; }
    }
}
