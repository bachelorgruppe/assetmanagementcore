﻿using System;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.SchoolModule.Entities
{
    public class Student : User
    {
        public DateTime DateOfBirth { get; set; }
    }
}