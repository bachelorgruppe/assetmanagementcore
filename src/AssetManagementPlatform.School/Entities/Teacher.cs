﻿using System;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.SchoolModule.Entities
{
    public class Teacher : User
    {
        public DateTime DateOfBirth { get; set; }
    }
}