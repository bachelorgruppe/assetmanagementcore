﻿using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.SchoolModule.Entities
{
    public class Book : Asset
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }
}