﻿using System;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.SchoolModule.Entities
{
    public class Homework : Asset
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public Guid ClassroomId { get; set; }
        public Classroom Classroom { get; set; }
    }
}
