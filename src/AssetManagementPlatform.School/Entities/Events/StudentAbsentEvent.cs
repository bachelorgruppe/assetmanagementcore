﻿using System;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.SchoolModule.Entities.Events
{
    public class StudentAbsentEvent : Event
    {
        public Guid ReportedByUserId { get; set; }
        public User ReportedByUser { get; set; }
        public Guid ClassroomId { get; set; }
        public Classroom Classroom { get; set; }
    }
}