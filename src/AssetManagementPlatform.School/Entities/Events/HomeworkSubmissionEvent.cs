﻿using System;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.SchoolModule.Entities.Events
{
    public class HomeworkSubmissionEvent : Event
    {
        public string Answer { get; set; }
        public Student Student { get; set; }
        public Guid StudentId { get; set; }
    }
}