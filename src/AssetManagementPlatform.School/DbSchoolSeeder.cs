﻿using System;
using System.Collections.Generic;
using System.Reflection;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using Bogus;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.SchoolModule
{
    public class DbSchoolSeeder : IDbContextSeed
    {
        private readonly SchoolContext _db;
        private readonly SavingOrganizationManager _organizationManager;
        private readonly SavingBookManager _bookManager;
        private readonly SavingClassroomManager _classroomManager;
        private readonly SavingStudentManager _studentManager;

        public DbSchoolSeeder(IServiceProvider serviceProvider)
        {
            Randomizer.Seed = new Random(8675309);

            _db = serviceProvider.GetRequiredService<SchoolContext>();
            _organizationManager = serviceProvider.GetRequiredService<SavingOrganizationManager>();
            _bookManager = serviceProvider.GetRequiredService<SavingBookManager>();
            _classroomManager = serviceProvider.GetRequiredService<SavingClassroomManager>();
            _studentManager = serviceProvider.GetRequiredService<SavingStudentManager>();
        }

        public void Seed()
        {
            List<Location> address = GetLocations().Generate(7);

            // Setup Organizations
            List<Organization> organizations = GetOrganizations().Generate(1);
            organizations[0].Name = "Munkekær skolen";
            organizations[0].Location = address[0];
            SetId(typeof(Organization), organizations[0], new Guid("391ab491-4233-4a8f-b9c6-4719dbb202d9"));

            _db.Organizations.AddRange(organizations);
            _db.SaveChanges();

            // Setup Users
            List<Student> students = GetStudents().Generate(4);
            List<Teacher> teachers = GetTeachers().Generate(2);

            //SetId(typeof(Teacher).BaseType.BaseType, teachers[0], new Guid("292fa3ef-8617-4f30-ae30-5571fa0488e1"));
            SetId(typeof(Student).BaseType.BaseType, students[0], new Guid("ac7a7638-dc99-4ad1-be16-e56cfae4ca42"));
            SetId(typeof(Teacher).BaseType.BaseType, teachers[0], new Guid("c7f9d850-9383-4142-b782-d7d4c9e7a8f0"));

            teachers[0].FirstName = "Teacher";
            students[0].FirstName = "Student";

            _db.Students.AddRange(students);
            _db.Teachers.AddRange(teachers);
            _db.SaveChanges();

            _organizationManager.AssignAssetToOrganization(students[0], organizations[0]);
            _organizationManager.AssignAssetToOrganization(students[1], organizations[0]);
            _organizationManager.AssignAssetToOrganization(students[2], organizations[0]);
            _organizationManager.AssignAssetToOrganization(students[3], organizations[0]);

            _organizationManager.AssignAssetToOrganization(teachers[0], organizations[0]);

            //Setup books
            Faker<Book> books = GetBooks();
            Book book = books.Generate();
            Book book1 = _bookManager.CreateBook(book.Title, book.Author);

            _studentManager.AssignBook(book1, students[0]);

            //Setup classes
            Classroom classroom1 = _classroomManager.Create("Math class 1");
            Classroom classroom2 = _classroomManager.Create("Math class 2");
            Classroom classroom3 = _classroomManager.Create("Math class 3");

            _classroomManager.Assign(students[0], classroom1);
            _classroomManager.Assign(students[1], classroom1);
            _classroomManager.Assign(students[2], classroom1);

            _classroomManager.Assign(teachers[0], classroom1);
            _classroomManager.Assign(teachers[0], classroom2);
        }

        private Faker<Book> GetBooks()
        {
            return new Faker<Book>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.Title, f => f.Lorem.Sentence())
                   .RuleFor(o => o.Author, f => f.Person.FullName)
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Project, f => null)
                   .RuleFor(o => o.ProjectId, f => null)
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null);
        }

        private Faker<Student> GetStudents()
        {
            return new Faker<Student>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.FirstName, f => f.Name.FirstName())
                   .RuleFor(o => o.LastName, f => f.Name.LastName())
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.DateOfBirth, f => f.Date.Recent(99))
                   .RuleFor(o => o.Project, f => null)
                   .RuleFor(o => o.ProjectId, f => null)
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null);
        }
        private Faker<Teacher> GetTeachers()
        {
            return new Faker<Teacher>()
                .StrictMode(true)
                .RuleFor(o => o.Id, f => f.Random.Guid())
                .RuleFor(o => o.FirstName, f => f.Name.FirstName())
                .RuleFor(o => o.LastName, f => f.Name.LastName())
                .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                .RuleFor(o => o.DateOfBirth, f => f.Date.Recent(99))
                .RuleFor(o => o.Project, f => null)
                .RuleFor(o => o.ProjectId, f => null)
                .RuleFor(o => o.Organization, f => null)
                .RuleFor(o => o.OrganizationId, f => null)
                .RuleFor(o => o.Events, f => null);
        }

        private void SetId(Type type, object theObject, Guid guid)
        {
            FieldInfo field = type.GetField("<Id>k__BackingField", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(theObject, guid);
        }

        private Faker<Location> GetLocations()
        {
            return new Faker<Location>()
                   .StrictMode(true)
                   .RuleFor(o => o.Address, f => f.Address.FullAddress());
        }

        private Faker<Organization> GetOrganizations()
        {
            return new Faker<Organization>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.Name, f => f.Company.CompanyName())
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Projects, f => new List<Project>())
                   .RuleFor(o => o.Assets, f => new List<Asset>())
                   .RuleFor(o => o.Location, f => null);
        }

        private Faker<Project> GetProjects()
        {
            return new Faker<Project>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.Name, f => f.Name.JobTitle())
                   .RuleFor(o => o.Location, f => null)
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null)
                   .RuleFor(o => o.StartOn, f => f.Date.Recent(50))
                   .RuleFor(o => o.EndOn, f => f.Date.Recent(30))
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Assets, f => new List<Asset>());
        }

        private Faker<User> GetUsers()
        {
            return new Faker<User>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.FirstName, f => f.Name.FirstName())
                   .RuleFor(o => o.LastName, f => f.Name.LastName())
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Project, f => null)
                   .RuleFor(o => o.ProjectId, f => null)
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null);
        }

        private Faker<AssignmentEvent> GetEvents()
        {
            return new Faker<AssignmentEvent>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.Asset, f => null)
                   .RuleFor(o => o.AssetId, f => f.Random.Guid())
                   .RuleFor(o => o.AssignedUser, f => null)
                   .RuleFor(o => o.AssignedUserId, f => f.Random.Guid())
                   .RuleFor(o => o.Type, f => null);
        }

        private Faker<Asset> GetAssets()
        {
            return new Faker<Asset>()
                   .StrictMode(true)
                   .RuleFor(o => o.Id, f => f.Random.Guid())
                   .RuleFor(o => o.Project, f => null)
                   .RuleFor(o => o.ProjectId, f => null)
                   .RuleFor(o => o.CreatedOn, f => f.Date.Recent(100))
                   .RuleFor(o => o.UpdatedOn, f => f.Date.Recent(99))
                   .RuleFor(o => o.Organization, f => null)
                   .RuleFor(o => o.OrganizationId, f => null)
                   .RuleFor(o => o.Events, f => new List<Event>())
                   .RuleFor(o => o.AssetGroups, f => null);
        }
    }
}