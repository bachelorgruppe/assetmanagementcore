﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.Authentication.Extensions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.Homework;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HomeworkCreateModel = AssetManagementPlatform.SchoolModule.ViewModels.Homework.HomeworkCreateModel;
using HomeworkEditModel = AssetManagementPlatform.SchoolModule.ViewModels.Homework.HomeworkEditModel;

namespace AssetManagementPlatform.SchoolModule.Controllers
{
    [Authorize]
    public class HomeworkController : Controller
    {
        private readonly IClassroomManager _classroomManager;
        private readonly IHomeworkManager _homeworkManager;

        public HomeworkController(SavingClassroomManager classroomManager, SavingHomeworkManager homeworkManager)
        {
            _classroomManager = classroomManager;
            _homeworkManager = homeworkManager;
        }

        public IActionResult Index()
        {
            SchoolSitePermissions permissions = User.GetSchoolUserPermissions();

            if (permissions.IsStudent)
                return RedirectToAction("IndexStudent");

            if (permissions.IsTeacher)
                return RedirectToAction("IndexTeacher");

            return Unauthorized();
        }

        [RoleAuthorizeSchool(nameof(SchoolSitePermissions.IsTeacher))]
        public ViewResult IndexTeacher(string query)
        {
            var homeworkQuery = _homeworkManager
                .GetHomework(new TeacherId(User.GetUserId()));

            if (!string.IsNullOrWhiteSpace(query))
                homeworkQuery = homeworkQuery.Where(a =>
                                                        a.Title.Contains(query, StringComparison.OrdinalIgnoreCase) ||
                                                        a.Description.Contains(query, StringComparison.OrdinalIgnoreCase));

            return View(homeworkQuery.Include(x => x.Classroom).ToList());
        }

        [RoleAuthorizeSchool(nameof(SchoolSitePermissions.IsStudent))]
        public IActionResult IndexStudent(string query)
        {
            SchoolSitePermissions permissions = User.GetSchoolUserPermissions();
            if (permissions.IsTeacher)
                return RedirectToAction("IndexTeacher", new { query });

            StudentId studentId = new StudentId(User.GetUserId());

            var homeworkQuery = _homeworkManager
                .GetHomework(studentId);

            if (!string.IsNullOrWhiteSpace(query))
                homeworkQuery = homeworkQuery.Where(a =>
                                                        a.Title.Contains(query, StringComparison.OrdinalIgnoreCase) ||
                                                        a.Description.Contains(query, StringComparison.OrdinalIgnoreCase));

            var model = homeworkQuery
                        .Include(x => x.Classroom)
                        .ToList()
                        .Select(x => new HomeworkModel
                        {
                            Homework = x,
                            SubmissionDate = _homeworkManager.GetSubmissionFromStudent(new HomeworkId(x.Id), studentId)?.CreatedOn
                        })
                        .ToList();

            return View(model);
        }

        public ViewResult Create()
        {
            TeacherId teacherId = new TeacherId(User.GetUserId());
            HomeworkCreateModel model = new HomeworkCreateModel
            {
                ClassroomSelections = GetClassroomOptions(teacherId).ToList()
            };
            return View(model);
        }

        [HttpPost]
        public RedirectToActionResult Create(HomeworkCreateModel model)
        {
            Homework homework = new Homework
            {
                Title = model.Title,
                Description = model.Description,
                DueDate = model.DueDate,
                ClassroomId = model.ClassroomId,
            };

            _homeworkManager.CreateHomework(homework);
            ClassroomId classroomId = new ClassroomId(model.ClassroomId);
            _classroomManager.AssignAsset(homework, classroomId);

            return RedirectToAction("IndexStudent");
        }

        public ViewResult DetailsTeacher(Guid id)
        {
            HomeworkId homeworkId = new HomeworkId(id);
            var homework = _homeworkManager
                           .GetAllHomework()
                           .Include(x => x.Classroom)
                           .GetEntity(homeworkId);

            return View(new HomeworkDetailsModelTeacher()
            {
                Id = homework.Id,
                Title = homework.Title,
                Description = homework.Description,
                DueDate = homework.DueDate,
                Classroom = homework.Classroom,
                MissingSubmissions = _homeworkManager.GetMissingSubmissions(homeworkId).ToList()
            });
        }

        public ViewResult DetailsStudent(Guid id)
        {
            HomeworkId homeworkId = new HomeworkId(id);
            var homework = _homeworkManager
                           .GetAllHomework()
                           .Include(x => x.Classroom)
                           .GetEntity(homeworkId);

            HomeworkDetailsModelStudent model = new HomeworkDetailsModelStudent
            {
                Id = homework.Id,
                Title = homework.Title,
                Description = homework.Description,
                DueDate = homework.DueDate,
                Classroom = homework.Classroom,
                SubmissionDate = _homeworkManager.GetSubmissionFromStudent(homeworkId, new StudentId(User.GetUserId()))?.CreatedOn
            };
            return View(model);
        }

        public ViewResult Edit(Guid id)
        {
            HomeworkId homeworkId = new HomeworkId(id);
            var homework = _homeworkManager
                           .GetAllHomework()
                           .Include(x => x.Classroom)
                           .GetEntity(homeworkId);

            return View(new HomeworkEditModel
            {
                Title = homework.Title,
                Description = homework.Description,
                DueDate = homework.DueDate,
                ClassroomSelections = GetClassroomOptions(new TeacherId(User.GetUserId()))
                    .Select(i =>
                    {
                        i.Selected = homework.ClassroomId.ToString() == i.Value;
                        return i;
                    }).ToList()
            });
        }

        [HttpPost]
        public RedirectToActionResult Edit(HomeworkEditModel model)
        {
            ClassroomId classroomId = new ClassroomId(model.ClassroomId);

            HomeworkId homeworkId = new HomeworkId(model.Id);
            var homework = _homeworkManager
                           .GetAllHomework()
                           .Include(x => x.Classroom)
                           .GetEntity(homeworkId);

            homework.Title = model.Title;
            homework.Description = model.Description;
            homework.DueDate = model.DueDate;

            // Assign homework to the selected class
            if (model.ClassroomId != homework.ClassroomId)
            {
                _classroomManager.UnassignAsset(homework, homework.Classroom);
                _classroomManager.AssignAsset(homework, classroomId);
            }

            homework.ClassroomId = model.ClassroomId;
            homework.Classroom = _classroomManager.GetAllClassrooms().GetEntity(classroomId);

            _homeworkManager.EditHomework(homework);
            return RedirectToAction("IndexStudent");
        }

        public RedirectToActionResult Delete(Guid id)
        {
            _homeworkManager.DeleteHomework(new HomeworkId(id));
            return RedirectToAction("IndexStudent");
        }

        private IEnumerable<SelectListItem> GetClassroomOptions(TeacherId teacherId)
        {
            var classesWithTeacherAssigned = _classroomManager.GetClassrooms(teacherId);
            return classesWithTeacherAssigned.Select(x => new SelectListItem(x.Name, x.Id.ToString()));
        }

        public IActionResult SubmitHomework(Guid id)
        {
            return View();
        }

        [HttpPost]
        public IActionResult SubmitHomework(Guid id, string answer)
        {
            HomeworkId homeworkId = new HomeworkId(id);
            var homework = _homeworkManager.GetAllHomework().GetEntity(homeworkId);
            _homeworkManager.SubmitHomework(new HomeworkSubmission
            {
                Answer = answer,
                Homework = homework,
                StudentId = User.GetUserId().Id
            });
            return RedirectToAction("DetailsStudent", new { id = id });
        }
    }
}