﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Web;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.Authentication.Extensions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.Classroom;
using AssetManagementPlatform.SchoolModule.ViewModels.Shared;
using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.SchoolModule.Controllers
{
    public class ClassroomController : BaseController
    {
        private readonly IClassroomManager _classroomManager;
        private readonly IStudentManager _studentManager;
        private readonly ITeacherManager _teacherManager;

        public ClassroomController(SavingClassroomManager classroomManager, SavingStudentManager studentManager,
                                   SavingTeacherManager teacherManager)
        {
            _classroomManager = classroomManager;
            _studentManager = studentManager;
            _teacherManager = teacherManager;
        }

        public IActionResult Create()
        {
            ClassroomCreateModel model = new ClassroomCreateModel
            {
                StudentSelections = GetStudentOptions().ToList(),
                TeacherSelections = GetTeacherOptions().ToList()
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(ClassroomCreateModel model)
        {
            var classroom = _classroomManager.Create(model.Name);
            ClassroomId classroomId = new ClassroomId(classroom.Id);

            if (model.StudentSelections != null)
                foreach (var student in model.StudentSelections.Where(i => i.IsSelected))
                {
                    UserId userId = new UserId(student.Id);
                    _classroomManager.Assign(userId, classroomId);
                }

            if (model.TeacherSelections != null)
                foreach (var teacher in model.TeacherSelections.Where(i => i.IsSelected))
                {
                    UserId userId = new UserId(teacher.Id);
                    _classroomManager.Assign(userId, classroomId);
                }

            return RedirectToAction("Index");
        }

        public ViewResult Details(Guid id)
        {
            ClassroomId classroomId = new ClassroomId(id);
            var classroom = _classroomManager.GetAllClassrooms().GetEntity(classroomId);
            var model = new ClassroomDetailsModel
            {
                Id = id,
                Name = classroom.Name,
                AssignedStudents = _classroomManager.GetStudentsInClass(classroomId).ToList(),
                AssignedTeachers = _classroomManager.GetTeachersInClass(classroomId).ToList(),
                AbsentStudentEvents = _classroomManager.GetStudentAbsentEvents(classroomId, DateTime.Now.AddDays(-30), DateTime.Today).ToList(),
            };
            return View(model);
        }

        public ViewResult Edit(Guid id)
        {
            ClassroomId classroomId = new ClassroomId(id);
            var classroom = _classroomManager.GetAllClassrooms().GetEntity(classroomId);
            var assignedStudents = _classroomManager.GetStudentsInClass(classroom);
            var assignedTeachers = _classroomManager.GetTeachersInClass(classroom);
            var model = new ClassroomEditModel
            {
                Id = classroom.Id,
                Name = classroom.Name,
                StudentSelections = GetStudentOptions()
                    .Select(i =>
                    {
                        i.IsSelected = assignedStudents.Any(s => s.Id == i.Id);
                        return i;
                    }).ToList(),
                TeacherSelections = GetTeacherOptions()
                    .Select(i =>
                    {
                        i.IsSelected = assignedTeachers.Any(s => s.Id == i.Id);
                        return i;
                    }).ToList()
            };
            return View(model);
        }

        [HttpPost]
        public RedirectToActionResult Edit(ClassroomEditModel model)
        {
            ClassroomId classroomId = new ClassroomId(model.Id);

            var classroom = _classroomManager.GetAllClassrooms().GetEntity(classroomId);
            var assignedStudents = _classroomManager.GetStudentsInClass(classroom);
            var assignedTeachers = _classroomManager.GetTeachersInClass(classroom);

            foreach (var studentSelection in model.StudentSelections)
            {
                UserId userId = new UserId(studentSelection.Id);
                if (studentSelection.IsSelected && !assignedStudents.Any(s => s.Id == studentSelection.Id))
                    _classroomManager.Assign(userId, classroomId);
                if (!studentSelection.IsSelected && assignedStudents.Any(s => s.Id == studentSelection.Id))
                    _classroomManager.UnAssign(userId, classroomId);
            }

            foreach (var teacherSelection in model.TeacherSelections)
            {
                UserId userId = new UserId(teacherSelection.Id);
                if (teacherSelection.IsSelected && !assignedTeachers.Any(s => s.Id == teacherSelection.Id))
                    _classroomManager.Assign(userId, classroomId);
                if (!teacherSelection.IsSelected && assignedTeachers.Any(s => s.Id == teacherSelection.Id))
                    _classroomManager.UnAssign(userId, classroomId);
            }

            classroom.Name = model.Name;

            _classroomManager.Edit(classroom);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(Guid id)
        {
            _classroomManager.Delete(new ClassroomId(id));
            return RedirectToAction("Index");
        }

        public IActionResult Index(string query)
        {
            IQueryable<Classroom> classroomQuery = _classroomManager.GetClassrooms(User.GetUserId());

            SchoolSitePermissions permissions = User.GetSchoolUserPermissions();
            if (permissions.SeeAllClasses)
                classroomQuery = _classroomManager.GetAllClassrooms();

            if (!permissions.SeeOwnClasses && !permissions.SeeAllClasses)
                return Unauthorized();

            if (!string.IsNullOrWhiteSpace(query))
                classroomQuery = classroomQuery.Where(a => a.Name.Contains(query, StringComparison.OrdinalIgnoreCase));



            return View(classroomQuery.ToList());
        }

        private IEnumerable<ItemSelection> GetStudentOptions()
        {
            return _studentManager.GetStudents()
                                  .Select(s => new ItemSelection
                                  {
                                      Id = s.Id,
                                      Name = s.FullName,
                                      IsSelected = false
                                  });
        }

        private IEnumerable<ItemSelection> GetTeacherOptions()
        {
            return _teacherManager.GetTeachers()
                                  .Select(t => new ItemSelection
                                  {
                                      Id = t.Id,
                                      Name = t.FullName,
                                      IsSelected = false
                                  });
        }

        public IActionResult MarkAbsence(Guid id, Guid studentId)
        {
            _studentManager.MarkAbsent(new StudentId(studentId), new ClassroomId(id));
            return RedirectToAction("Details", new { id = id });
        }

        public IActionResult UnmarkAbsense(Guid id, Guid studentId)
        {
            _studentManager.UnmarkAbsent(new EventId(studentId));
            return RedirectToAction("Details", new { id = id });
        }
    }
}