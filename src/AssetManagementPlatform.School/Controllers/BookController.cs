﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.Core.Web;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.Book;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule.Controllers
{
    public class BookController : BaseController
    {
        private readonly IBookManager _bookManager;
        private readonly IStudentManager _studentManager;
        private readonly IEventManager _eventManager;
        private readonly IAssetManager _assetManager;

        public BookController(SavingBookManager bookManager, SavingStudentManager studentManager, SavingEventManager eventManager, SavingAssetManager assetManager)
        {
            _bookManager = bookManager;
            _studentManager = studentManager;
            this._eventManager = eventManager;
            _assetManager = assetManager;
        }

        public async Task<ViewResult> Index()
        {
            List<Book> books = await _bookManager.GetBooks().ToListAsync();

            List<BookModel> toolModels = new List<BookModel>();
            foreach (Book book in books)
            {
                toolModels.Add(new BookModel
                {
                    Book = book,
                    AssignedUser = _eventManager.GetUserAssigned(book),
                });
            }

            return View(toolModels);
        }

        [RoleAuthorizeSchool(nameof(SchoolSitePermissions.CanCreateBook))]
        public IActionResult Create()
        {
            BookCreateModel createModel = new BookCreateModel();
            return View(createModel);
        }

        [RoleAuthorizeSchool(nameof(SchoolSitePermissions.CanCreateBook))]
        [HttpPost]
        public IActionResult Create(BookCreateModel bookModel)
        {
            if (!ModelState.IsValid)
                return View(bookModel);

            _bookManager.CreateBook(bookModel.Title, bookModel.Author);

            return RedirectToAction("Index");
        }

        [RoleAuthorizeSchool(nameof(SchoolSitePermissions.CanDeleteBook))]
        public IActionResult Delete(Guid id)
        {
            _bookManager.DeleteBook(new BookId(id));

            return RedirectToAction("Index");
        }

        public ViewResult Edit(Guid id)
        {
            BookId bookId = new BookId(id);
            Book entity = _bookManager.GetBooks().GetEntity(bookId);
            AssignmentEvent firstOrDefault = _eventManager.GetEventsByType<AssignmentEvent>(bookId).FirstOrDefault();

            BookEditModel editModel = new BookEditModel();
            editModel.Id = entity.Id;
            editModel.Title = entity.Title;
            editModel.Author = entity.Author;
            editModel.StudentOptions = GetStudentOptions(firstOrDefault).ToList();
            return View(editModel);
        }

        private IEnumerable<SelectListItem> GetStudentOptions(AssignmentEvent assignmentEvent)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (Student s in _studentManager.GetStudents())
                list.Add(new SelectListItem(s.FullName, s.Id.ToString(), assignmentEvent != null && assignmentEvent.AssignedUserId == s.Id));

            list.Add(new SelectListItem("-----", string.Empty, assignmentEvent == null));
            return list;
        }

        [HttpPost]
        public IActionResult Edit(BookEditModel bookModel)
        {
            if (!ModelState.IsValid)
                return View(bookModel);

            BookId bookId = new BookId(bookModel.Id);
            Book book = _bookManager.GetBooks().GetEntity(bookId);

            book.Title = bookModel.Title;
            book.Author = bookModel.Author;

            var assignmentEvent = _eventManager.GetEventsByType<AssignmentEvent>().LastOrDefault(t => t.AssetId == bookModel.Id);
            if (assignmentEvent != null && assignmentEvent.AssignedUserId == bookModel.SelectedStudentId)
            {
                _bookManager.EditBook(book);

                return RedirectToAction("Index");
            }

            _assetManager.AssignAsset(bookId, new UserId(bookModel.SelectedStudentId));
            _bookManager.EditBook(book);

            return RedirectToAction("Index");
        }
    }
}