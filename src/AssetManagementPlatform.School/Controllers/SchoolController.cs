﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.Core.Web;
using AssetManagementPlatform.MessageModule.Abstractions.Managers;
using AssetManagementPlatform.MessageModule.SavingManagers;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.Authentication.Extensions;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.School;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule.Controllers
{
    public class SchoolController : BaseController
    {
        private readonly IClassroomManager _classroomManager;
        private readonly IBookManager _bookManager;
        private readonly IMessageManager _messageManager;
        private readonly IReservationManager _reservationManager;

        /// <inheritdoc />
        public SchoolController(SavingClassroomManager classroomManager, SavingBookManager bookManager, SavingMessageManager messageManager, SavingReservationManager reservationManager)
        {
            _classroomManager = classroomManager;
            _bookManager = bookManager;
            _messageManager = messageManager;
            _reservationManager = reservationManager;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return View();

            var userPermissions = User.GetSchoolUserPermissions();

            if (userPermissions.HasAllPermissions(nameof(SchoolSitePermissions.IsStudent), nameof(SchoolSitePermissions.SeeOwnClasses), nameof(SchoolSitePermissions.SeeOwnBooks)))
                return RedirectToAction("StudentIndex");

            if (userPermissions.IsTeacher)
                return RedirectToAction("TeacherIndex");

            return Unauthorized();
        }

        [RoleAuthorizeSchool(nameof(SchoolSitePermissions.IsStudent), nameof(SchoolSitePermissions.SeeOwnClasses), nameof(SchoolSitePermissions.SeeOwnBooks))]
        public async Task<IActionResult> StudentIndex()
        {
            UserId userId = User.GetUserId();
            List<Classroom> classrooms = await _classroomManager.GetClassrooms(userId).ToListAsync();
            List<Book> books = await _bookManager.GetStudentBooks(userId).ToListAsync();
            List<Message> messages = await _messageManager
                                           .GetReceivedMessages(userId)
                                           .OrderByDescending(x => x.CreatedOn)
                                           .Take(5)
                                           .Include(x => x.Sender)
                                           .ToListAsync();
            List<ReservationEvent> reservationEvents = _reservationManager.GetReservationEvents(userId)
                                                                          .Include(x => x.ReservedAsset)
                                                                          .ToList();
            SchoolStudentIndexViewModel indexViewModel = new SchoolStudentIndexViewModel
            {
                Classrooms = classrooms,
                Books = books,
                Messages = messages,
                ReservationEvents = reservationEvents
            };

            return View(indexViewModel);
        }

        [RoleAuthorizeSchool(nameof(SchoolSitePermissions.IsTeacher))]
        public async Task<IActionResult> TeacherIndex()
        {
            UserId userId = User.GetUserId();
            List<Classroom> classrooms = await _classroomManager.GetClassrooms(userId).ToListAsync();
            List<Message> messages = await _messageManager
                                           .GetReceivedMessages(userId)
                                           .OrderByDescending(x => x.CreatedOn)
                                           .Take(5)
                                           .Include(x => x.Sender)
                                           .ToListAsync();
            List<ReservationEvent> reservationEvents = _reservationManager.GetReservationEvents(userId)
                                                                          .Include(x => x.ReservedAsset)
                                                                          .Include(x => x.Asset)
                                                                          .ToList();

            SchoolTeacherIndexViewModel model = new SchoolTeacherIndexViewModel
            {
                Classrooms = classrooms,
                Messages = messages,
                ReservationEvents = reservationEvents
            };

            return View(model);
        }
    }
}