﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.Web;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.Shared;
using AssetManagementPlatform.SchoolModule.ViewModels.Student;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.SchoolModule.Controllers
{
    public class StudentController : BaseController
    {
        private readonly IStudentManager _studentManager;
        private readonly IClassroomManager _classroomManager;

        public StudentController(SavingStudentManager studentManager, SavingClassroomManager classroomManager)
        {
            _studentManager = studentManager;
            _classroomManager = classroomManager;
        }

        public ViewResult Index(string query)
        {
            IQueryable<Student> studentQuery = _studentManager.GetStudents();

            if (!string.IsNullOrWhiteSpace(query))
                studentQuery = studentQuery.Where(a =>
                                                      a.FirstName.Contains(query, StringComparison.OrdinalIgnoreCase) ||
                                                      a.LastName.Contains(query, StringComparison.OrdinalIgnoreCase));

            return View(studentQuery.ToList());
        }

        public ViewResult Create()
        {
            var model = new StudentCreateModel
            {
                ClassroomSelections = GetClassroomOptions().ToList()
            };
            return View(model);
        }

        [HttpPost]
        public RedirectToActionResult Create(StudentCreateModel model)
        {
            var student = _studentManager.CreateStudent(model.Firstname, model.LastName, model.DateOfBirth);

            foreach (var selectedClassroom in model.ClassroomSelections.Where(x => x.IsSelected))
                _classroomManager.Assign(student, new ClassroomId(selectedClassroom.Id));

            return RedirectToAction("Index");
        }

        public ViewResult Details(Guid id)
        {
            StudentId studentId = new StudentId(id);
            var student = _studentManager.GetStudents().GetEntity(studentId);
            var assignedClassrooms = _classroomManager.GetClassrooms(student).ToList();

            var detailsModel = new StudentDetailsModel
            {
                Firstname = student.FirstName,
                LastName = student.LastName,
                DateOfBirth = student.DateOfBirth,
                AssignedClassrooms = assignedClassrooms,
                AbsentStudentEvents = _studentManager.GetStudentAbsentEvents(studentId, DateTime.Now.AddDays(-30), DateTime.Today).ToList(),
            };
            return View(detailsModel);
        }

        public ViewResult Edit(Guid id)
        {
            StudentId studentId = new StudentId(id);
            var student = _studentManager.GetStudents().GetEntity(studentId);
            var assignedClassrooms = _classroomManager.GetClassrooms(student);
            var model = new StudentEditModel
            {
                Id = student.Id,
                Firstname = student.FirstName,
                LastName = student.LastName,
                DateOfBirth = student.DateOfBirth,
                ClassroomSelections = GetClassroomOptions()
                    .Select(r =>
                    {
                        r.IsSelected = assignedClassrooms.Any(x => x.Id == r.Id);
                        return r;
                    }).ToList()
            };
            return View(model);
        }

        [HttpPost]
        public RedirectToActionResult Edit(StudentEditModel model)
        {
            StudentId studentId = new StudentId(model.Id);
            var student = _studentManager.GetStudents().GetEntity(studentId);

            var assignedClassrooms = _classroomManager.GetClassrooms(student);

            foreach (var classChoices in model.ClassroomSelections)
            {
                ClassroomId classroomId = new ClassroomId(classChoices.Id);

                if (classChoices.IsSelected && !assignedClassrooms.Any(x => x.Id == classChoices.Id))
                    _classroomManager.Assign(student, classroomId);

                if (!classChoices.IsSelected && assignedClassrooms.Any(x => x.Id == classChoices.Id))
                    _classroomManager.UnAssign(student, classroomId);
            }

            student.FirstName = model.Firstname;
            student.LastName = model.LastName;
            student.DateOfBirth = model.DateOfBirth;

            _studentManager.EditStudent(student);
            return RedirectToAction("Index");
        }

        public RedirectToActionResult Delete(Guid id)
        {
            _studentManager.DeleteStudent(new StudentId(id));
            return RedirectToAction("Index");
        }

        private IEnumerable<ItemSelection> GetClassroomOptions()
        {
            return _classroomManager.GetAllClassrooms()
                                    .Select(x => new ItemSelection
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        IsSelected = false
                                    });
        }

        public RedirectToActionResult AssignBook(Guid book, Guid student)
        {
            _studentManager.AssignBook(new BookId(book), new StudentId(student));
            return RedirectToAction("Index");
        }
    }
}