﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.Authorization.Extensions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.Web;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.Authentication.Extensions;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.Shared;
using AssetManagementPlatform.SchoolModule.ViewModels.Teacher;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TeacherEditModel = AssetManagementPlatform.SchoolModule.ViewModels.Teacher.TeacherEditModel;

namespace AssetManagementPlatform.SchoolModule.Controllers
{
    public class TeacherController : BaseController
    {
        private readonly ITeacherManager _teacherManager;
        private readonly IClassroomManager _classroomManager;

        public TeacherController(SavingTeacherManager teacherManager, SavingClassroomManager classroomManager)
        {
            _teacherManager = teacherManager;
            _classroomManager = classroomManager;
        }

        public IActionResult Index()
        {
            if (User.GetSchoolUserPermissions().IsStudent)
                return RedirectToAction("IndexStudent");

            if (User.GetSchoolUserPermissions().IsTeacher)
                return RedirectToAction("IndexTeacher");

            return Unauthorized();
        }

        [RoleAuthorizeSchool(nameof(SchoolSitePermissions.IsTeacher))]
        public ViewResult IndexTeacher(string query)
        {
            IQueryable<Teacher> teacherQuery = _teacherManager.GetTeachers();

            if (!string.IsNullOrWhiteSpace(query))
                teacherQuery = teacherQuery.Where(a =>
                                                      a.FirstName.Contains(query, StringComparison.OrdinalIgnoreCase) ||
                                                      a.LastName.Contains(query, StringComparison.OrdinalIgnoreCase));

            return View(teacherQuery.ToList());
        }

        public IActionResult IndexStudent(string query)
        {
            SchoolSitePermissions permissions = User.GetSchoolUserPermissions();
            if (permissions.IsTeacher)
                return RedirectToAction("IndexTeacher", new { query });

            var teacherQuery = _teacherManager.GetAssignedTeachers(new StudentId(User.GetUserId()));

            if (!string.IsNullOrWhiteSpace(query))
                teacherQuery = teacherQuery.Where(a =>
                                                      a.FirstName.Contains(query, StringComparison.OrdinalIgnoreCase) ||
                                                      a.LastName.Contains(query, StringComparison.OrdinalIgnoreCase));

            return View(teacherQuery.ToList());
        }

        public ViewResult Create()
        {
            return View(new TeacherCreateModel
            {
                ClassroomSelections = GetClassroomOptions().ToList()
            });
        }

        [HttpPost]
        public RedirectToActionResult Create(TeacherCreateModel createModel)
        {
            var teacher = _teacherManager.Create(createModel.Firstname, createModel.LastName, createModel.DateOfBirth);

            foreach (var selectedClassroom in createModel.ClassroomSelections.Where(x => x.IsSelected))
                _classroomManager.Assign(teacher, new ClassroomId(selectedClassroom.Id));

            return RedirectToAction("IndexStudent");
        }

        public IActionResult Delete(Guid id)
        {
            _teacherManager.Delete(new TeacherId(id));
            return RedirectToAction("IndexStudent");
        }

        public void Assign(Asset asset, Teacher teacher)
        {
            _teacherManager.Assign(asset, teacher);
        }

        public IActionResult Edit(Guid id)
        {
            TeacherId teacherId = new TeacherId(id);
            var teacher = _teacherManager.GetTeachers().GetEntity(teacherId);
            var assignedClassrooms = _classroomManager.GetClassrooms(teacher).ToList();
            return View(new TeacherEditModel
            {
                Id = teacher.Id,
                Firstname = teacher.FirstName,
                LastName = teacher.LastName,
                DateOfBirth = teacher.DateOfBirth,
                ClassroomSelections = GetClassroomOptions()
                    .Select(r =>
                    {
                        r.IsSelected = assignedClassrooms.Any(x => x.Id == r.Id);
                        return r;
                    }).ToList()
            });
        }

        [HttpPost]
        public RedirectToActionResult Edit(TeacherEditModel model)
        {
            TeacherId teacherId = new TeacherId(model.Id);
            var teacher = _teacherManager.GetTeachers().GetEntity(teacherId);
            var assignedClassrooms = _classroomManager.GetClassrooms(teacher).ToList();

            foreach (var classChoices in model.ClassroomSelections)
            {
                if (classChoices.IsSelected && !assignedClassrooms.Any(x => x.Id == classChoices.Id))
                    _classroomManager.Assign(teacher, new ClassroomId(classChoices.Id));

                if (!classChoices.IsSelected && assignedClassrooms.Any(x => x.Id == classChoices.Id))
                    _classroomManager.UnAssign(teacher, new ClassroomId(classChoices.Id));
            }

            teacher.FirstName = model.Firstname;
            teacher.LastName = model.LastName;
            teacher.DateOfBirth = model.DateOfBirth;

            _teacherManager.EditTeacher(teacher);
            return RedirectToAction("IndexStudent");
        }

        public IActionResult Details(Guid id)
        {
            TeacherId teacherId = new TeacherId(id);
            var teacher = _teacherManager.GetTeachers().GetEntity(teacherId);
            var assignedClassrooms = _classroomManager.GetClassrooms(teacher).ToList();

            return View(new TeacherDetailsModel
            {
                Firstname = teacher.FirstName,
                LastName = teacher.LastName,
                DateOfBirth = teacher.DateOfBirth,
                AssignedClassrooms = assignedClassrooms
            });
        }

        private IEnumerable<ItemSelection> GetClassroomOptions()
        {
            return _classroomManager.GetAllClassrooms()
                                    .Select(x => new ItemSelection
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        IsSelected = false
                                    });
        }
    }
}