﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.DB.Entities.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AssetManagementPlatform.Core.DB
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> dbContextOptions) : base(dbContextOptions) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<AssetGroup> AssetGroups { get; set; }
        public DbSet<AssetGroupAsset> AssetGroupAssets { get; set; }
        public DbSet<Message> Messages { get; set; }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            ProcessUpdatedOn();

            int result = base.SaveChanges(acceptAllChangesOnSuccess);
            return result;
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            ProcessUpdatedOn();
            int result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            return result;
        }

        private void ProcessUpdatedOn()
        {
            ChangeTracker.DetectChanges();
            DateTime now = DateTime.UtcNow;

            foreach (EntityEntry item in ChangeTracker.Entries())
            {
                if ((item.State == EntityState.Added || item.State == EntityState.Modified) && item.Entity is IUpdatedOn asUpdatedOn)
                    asUpdatedOn.UpdatedOn = now;

                if (item.State == EntityState.Added && item.Entity is ICreatedOn asCreatedOn)
                    asCreatedOn.CreatedOn = now;
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Asset>()
                        .HasKey(s => s.Id);

            modelBuilder.Entity<Project>()
                        .HasKey(s => s.Id);

            modelBuilder.Entity<Event>()
                        .HasKey(s => s.Id);

            modelBuilder.Entity<Organization>()
                        .HasKey(s => s.Id);

            modelBuilder.Entity<User>()
                        .HasBaseType<Asset>();

            modelBuilder.Entity<AssignmentEvent>()
                        .HasBaseType<Event>()
                        .HasOne(a => a.AssignedUser)
                        .WithMany()
                        .HasForeignKey(a => a.AssignedUserId);

            modelBuilder.Entity<AssignmentRequestEvent>()
                        .HasBaseType<Event>()
                        .HasOne(a => a.RequestingUser)
                        .WithMany()
                        .HasForeignKey(a => a.RequestingUserId);

            modelBuilder.Entity<AssignmentConfirmationEvent>()
                        .HasBaseType<Event>()
                        .HasOne(a => a.ConfirmedUser)
                        .WithMany()
                        .HasForeignKey(a => a.ConfirmedUserId);

            modelBuilder.Entity<ReservationEvent>()
                        .HasBaseType<Event>();

            modelBuilder.Entity<ReservationEvent>()
                        .HasOne(a => a.ReservedTo)
                        .WithMany()
                        .HasForeignKey(a => a.ReservedToId);

            modelBuilder.Entity<ReservationEvent>()
                        .HasOne(a => a.ReservedAsset)
                        .WithMany()
                        .HasForeignKey(a => a.ReservedAssetId);

            modelBuilder.Entity<AssetGroup>()
                        .HasKey(s => s.Id);

            modelBuilder.Entity<AssetGroupAsset>()
                        .HasKey(s => new { s.AssetId, s.AssetGroupId });

            modelBuilder.Entity<Message>()
                        .HasKey(s => s.Id);

            modelBuilder.Entity<Project>()
                        .HasMany(s => s.Assets)
                        .WithOne(s => s.Project)
                        .HasForeignKey(s => s.ProjectId)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Asset>()
                        .HasMany(s => s.Events)
                        .WithOne(s => s.Asset)
                        .HasForeignKey(s => s.AssetId)
                        .IsRequired();

            modelBuilder.Entity<AssetGroupAsset>()
                        .HasOne(a => a.Asset)
                        .WithMany(a => a.AssetGroups)
                        .HasForeignKey(a => a.AssetGroupId)
                        .IsRequired();

            modelBuilder.Entity<AssetGroupAsset>()
                        .HasOne(a => a.AssetGroup)
                        .WithMany(a => a.Assets)
                        .HasForeignKey(a => a.AssetId)
                        .IsRequired();

            modelBuilder.Entity<AssetGroup>()
                        .HasMany(s => s.Assets)
                        .WithOne(s => s.AssetGroup)
                        .HasForeignKey(s => s.AssetGroupId)
                        .IsRequired();

            modelBuilder.Entity<Asset>()
                        .HasMany(s => s.AssetGroups)
                        .WithOne(s => s.Asset)
                        .HasForeignKey(s => s.AssetId)
                        .IsRequired();

            modelBuilder.Entity<Project>()
                        .HasMany(s => s.AssetGroups)
                        .WithOne(s => s.Project)
                        .HasForeignKey(s => s.ProjectId)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Organization>()
                        .HasMany(s => s.Assets)
                        .WithOne(s => s.Organization)
                        .HasForeignKey(s => s.OrganizationId)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Organization>()
                        .HasMany(s => s.Projects)
                        .WithOne(s => s.Organization)
                        .HasForeignKey(s => s.OrganizationId)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                        .OwnsOne(project => project.Location);

            modelBuilder.Entity<Organization>()
                        .OwnsOne(org => org.Location);

            modelBuilder.Entity<Message>()
                        .HasOne(e => e.Sender)
                        .WithMany()
                        .HasForeignKey(e => e.SenderId)
                        .IsRequired()
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Message>()
                        .HasOne(e => e.Receiver)
                        .WithMany()
                        .HasForeignKey(e => e.ReceiverId)
                        .IsRequired()
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Event>()
                        .HasOne(a => a.Asset)
                        .WithMany(a => a.Events)
                        .HasForeignKey(e => e.AssetId);
        }
    }
}