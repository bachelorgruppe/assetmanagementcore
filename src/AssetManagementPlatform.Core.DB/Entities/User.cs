﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AssetManagementPlatform.Core.DB.Entities
{
    public class User : Asset
    {
        [Required]
        [MaxLength(128)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(128)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";
    }
}