﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AssetManagementPlatform.Core.DB.Entities.Interfaces;

namespace AssetManagementPlatform.Core.DB.Entities
{
    public class Organization : IId, IUpdatedOn, ICreatedOn
    {
        public Organization()
        {
            Id = Guid.NewGuid();
            Projects = new List<Project>();
            Assets = new List<Asset>();
        }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public Location Location { get; set; }

        public ICollection<Asset> Assets { get; }
        public ICollection<Project> Projects { get; }
        public DateTime CreatedOn { get; set; }

        public Guid Id { get; }
        public DateTime UpdatedOn { get; set; }
    }
}