﻿using System;
using AssetManagementPlatform.Core.DB.Entities.Interfaces;

namespace AssetManagementPlatform.Core.DB.Entities
{
    public class Message : IId, IUpdatedOn, ICreatedOn
    {
        public Message()
        {
            Id = Guid.NewGuid();
        }

        public Message(Guid sender, Guid receiver, string text) : this()
        {
            SenderId = sender;
            ReceiverId = receiver;
            Text = text;
        }

        public Guid SenderId { get; set; }
        public User Sender { get; set; }

        public Guid ReceiverId { get; set; }
        public User Receiver { get; set; }

        public string Text { get; set; }
        public DateTime CreatedOn { get; set; }

        public Guid Id { get; }

        public DateTime UpdatedOn { get; set; }
    }
}