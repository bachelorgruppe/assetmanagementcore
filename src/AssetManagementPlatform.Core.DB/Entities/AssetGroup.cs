﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AssetManagementPlatform.Core.DB.Entities.Interfaces;

namespace AssetManagementPlatform.Core.DB.Entities
{
    public class AssetGroup : IId, IUpdatedOn, ICreatedOn
    {
        public AssetGroup()
        {
            Id = Guid.NewGuid();
            Assets = new List<AssetGroupAsset>();
        }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public Guid? ProjectId { get; set; }
        public Project Project { get; set; }

        public ICollection<AssetGroupAsset> Assets { get; }

        public DateTime CreatedOn { get; set; }

        public Guid Id { get; }

        public DateTime UpdatedOn { get; set; }
    }
}