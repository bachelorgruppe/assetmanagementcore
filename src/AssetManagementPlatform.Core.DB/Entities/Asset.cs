﻿using System;
using System.Collections.Generic;
using AssetManagementPlatform.Core.DB.Entities.Interfaces;

namespace AssetManagementPlatform.Core.DB.Entities
{
    public class Asset : IId, IUpdatedOn, ICreatedOn
    {
        public Asset()
        {
            Id = Guid.NewGuid();
            Events = new List<Event>();
        }

        public Guid? ProjectId { get; set; }
        public Project Project { get; set; }

        public ICollection<Event> Events { get; }

        public ICollection<AssetGroupAsset> AssetGroups { get; }
        public Guid? OrganizationId { get; set; }
        public Organization Organization { get; set; }
        public DateTime CreatedOn { get; set; }

        public Guid Id { get; }

        public DateTime UpdatedOn { get; set; }
    }
}