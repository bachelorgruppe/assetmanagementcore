﻿using System;
using AssetManagementPlatform.Core.DB.Entities.Interfaces;

namespace AssetManagementPlatform.Core.DB.Entities
{
    public abstract class Event : IId, ICreatedOn
    {
        protected Event()
        {
            Id = Guid.NewGuid();
        }

        public string Type { get; set; }
        public Asset Asset { get; set; }
        public Guid AssetId { get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime CreatedOn { get; set; }
        public Guid Id { get; }
    }
}