﻿namespace AssetManagementPlatform.Core.DB.Entities
{
    public class Location
    {
        public string Address { get; set; }
    }
}