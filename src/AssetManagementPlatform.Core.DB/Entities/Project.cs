﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AssetManagementPlatform.Core.DB.Entities.Interfaces;

namespace AssetManagementPlatform.Core.DB.Entities
{
    public class Project : IId, IUpdatedOn, ICreatedOn
    {
        public Project()
        {
            Id = Guid.NewGuid();
        }

        public DateTime StartOn { get; set; }
        public DateTime? EndOn { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public Location Location { get; set; }

        public Guid? OrganizationId { get; set; }
        public Organization Organization { get; set; }

        public ICollection<Asset> Assets { get; }
        public ICollection<AssetGroup> AssetGroups { get; }

        public DateTime CreatedOn { get; set; }

        public Guid Id { get; }

        public DateTime UpdatedOn { get; set; }
    }
}