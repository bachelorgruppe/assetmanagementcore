﻿using System;

namespace AssetManagementPlatform.Core.DB.Entities.Events
{
    public class AssignmentConfirmationEvent : Event
    {
        public Guid ConfirmedUserId { get; set; }
        public virtual User ConfirmedUser { get; set; }
    }
}