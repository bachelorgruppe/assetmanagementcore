﻿using System;

namespace AssetManagementPlatform.Core.DB.Entities.Events
{
    public class AssignmentEvent : Event
    {
        public Guid AssignedUserId { get; set; }
        public virtual Asset AssignedUser { get; set; }
    }
}