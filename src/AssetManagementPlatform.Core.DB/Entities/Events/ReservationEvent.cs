﻿using System;

namespace AssetManagementPlatform.Core.DB.Entities.Events
{
    public class ReservationEvent : Event
    {
        public DateTime ReservationStartDate { get; set; }
        public DateTime ReservationEndDate { get; set; }
        public Asset ReservedTo { get; set; }
        public Asset ReservedAsset { get; set; }
        public Guid ReservedToId { get; set; }
        public Guid ReservedAssetId { get; set; }
    }
}