﻿using System;

namespace AssetManagementPlatform.Core.DB.Entities.Events
{
    public class AssignmentRequestEvent : Event
    {
        public Guid RequestingUserId { get; set; }
        public virtual User RequestingUser { get; set; }
    }
}