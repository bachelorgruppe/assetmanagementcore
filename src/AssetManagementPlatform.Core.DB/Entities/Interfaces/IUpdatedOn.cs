﻿using System;

namespace AssetManagementPlatform.Core.DB.Entities.Interfaces
{
    /// <summary>Provides a date a DB entity was changed.</summary>
    public interface IUpdatedOn
    {
        DateTime UpdatedOn { get; set; }
    }
}