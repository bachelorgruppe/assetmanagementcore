﻿using System;

namespace AssetManagementPlatform.Core.DB.Entities.Interfaces
{
    /// <summary>
    /// Provides a default Id type for all entities
    /// </summary>
    public interface IId
    {
        Guid Id { get; }
    }
}