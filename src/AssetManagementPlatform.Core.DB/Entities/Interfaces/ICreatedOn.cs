﻿using System;

namespace AssetManagementPlatform.Core.DB.Entities.Interfaces
{
    /// <summary>Provides a date of creation for DB entities.</summary>
    public interface ICreatedOn
    {
        DateTime CreatedOn { get; set; }
    }
}