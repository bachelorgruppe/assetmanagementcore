﻿using System;

namespace AssetManagementPlatform.Core.DB.Entities
{
    public class AssetGroupAsset
    {
        public Guid AssetId { get; set; }
        public Asset Asset { get; set; }
        public Guid AssetGroupId { get; set; }
        public AssetGroup AssetGroup { get; set; }
    }
}