﻿using System.Security.Claims;
using AssetManagementPlatform.Core.DB.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;

namespace AssetManagementPlatform.Shared.Tests
{
    public static class CoreControllerTestExtensions
    {
        public static T WithIdentity<T>(this T controller, User user, Organization org) where T : Controller
        {
            controller.EnsureHttpContext();

            ClaimsPrincipal principal = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.GivenName, user.FullName),
                new Claim(Core.Authentication.AssetClaims.OrganizationId, org.Id.ToString())

                // other required and custom claims
            }, "TestAuthentication"));

            controller.ControllerContext.HttpContext.User = principal;

            return controller;
        }

        public static T WithAnonymousIdentity<T>(this T controller) where T : Controller
        {
            controller.EnsureHttpContext();

            ClaimsPrincipal principal = new ClaimsPrincipal(new ClaimsIdentity());

            controller.ControllerContext.HttpContext.User = principal;

            return controller;
        }

        public static T EnsureHttpContext<T>(this T controller) where T : Controller
        {
            if (controller.ControllerContext == null)
                controller.ControllerContext = new ControllerContext();

            if (controller.ControllerContext.HttpContext == null)
                controller.ControllerContext.HttpContext = new DefaultHttpContext();

            return controller;
        }
    }
}