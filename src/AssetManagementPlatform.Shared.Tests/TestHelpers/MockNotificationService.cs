﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Notification.Abstractions;

namespace AssetManagementPlatform.Shared.Tests.TestHelpers
{
    public class MockNotificationService : INotificationService
    {
        public List<(UserId, string, object)> UserNotifications { get; } = new List<(UserId, string, object)>();
        public List<(AssetGroupId, string, object)> GroupNotifications { get; } = new List<(AssetGroupId, string, object)>();

        public void SendNotification(UserId userId, string eventType, object data)
        {
            SendNotificationAsync(userId, eventType, data).GetAwaiter().GetResult();
        }

        public Task SendNotificationAsync(UserId userId, string eventType, object data)
        {
            UserNotifications.Add((userId, eventType, data));
            return Task.CompletedTask;
        }

        public void SendGroupNotification(AssetGroupId assetGroupId, string eventType, object data)
        {
            SendGroupNotificationAsync(assetGroupId, eventType, data).GetAwaiter().GetResult();
        }

        public Task SendGroupNotificationAsync(AssetGroupId assetGroupId, string eventType, object data)
        {
            GroupNotifications.Add((assetGroupId, eventType, data));
            return Task.CompletedTask;
        }
    }
}