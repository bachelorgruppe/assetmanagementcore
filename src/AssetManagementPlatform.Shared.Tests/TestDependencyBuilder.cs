﻿using System;
using AssetManagementPlatform.Core;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.Frontend.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.Shared.Tests
{
    public abstract class TestDependencyBuilder<TContext> : IDisposable where TContext : DataContext, IDisposable
    {
        private readonly string _testOrgName = "Test Org";

        private readonly string _testUserFirstName = "Gunnar";
        private readonly string _testUserLastName = "Reckard";

        protected TestDependencyBuilder(bool disableSeededDb = true)
        {
            var services = new ServiceCollection();

            SetupServices(services);

            Provider = services.BuildServiceProvider();

            SetupEntities(disableSeededDb);
        }

        public User GetTestUser { get; set; }

        public Organization GetTestOrg { get; set; }

        private ServiceProvider Provider { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1063:Implement IDisposable Correctly", Justification = "<Pending>")]
        private void Dispose(bool disposing)
        {
            if (disposing)
                Provider.Dispose();
        }

        ~TestDependencyBuilder()
        {
            Dispose(false);
        }

        protected virtual void SetupEntities(bool disableSeededDb)
        {
            if (disableSeededDb)
                return;

            GetTestUser = Provider.GetRequiredService<SavingUserManager>().CreateUser(_testUserFirstName, _testUserLastName);
            GetTestOrg = Provider.GetRequiredService<SavingOrganizationManager>().CreateOrganization(_testOrgName);
        }

        protected virtual void SetupServices(ServiceCollection services)
        {
            string databaseName = Guid.NewGuid().ToString();
            services.AddDbContext<DataContext>(builder => builder.UseInMemoryDatabase(databaseName).EnableSensitiveDataLogging());
            services.AddDbContext<TContext>(builder => builder.UseInMemoryDatabase(databaseName).EnableSensitiveDataLogging());

            if (typeof(TContext) != typeof(DataContext))
                services.AddScoped(c => (DataContext)c.GetRequiredService<TContext>());

            services.AddCoreServices();

            services.AddScoped<HomeController>();
            services.AddScoped<LoginController>();
        }

        public IServiceScope GetScope()
        {
            IServiceScopeFactory serviceScopeFactory = Provider.GetRequiredService<IServiceScopeFactory>();
            IServiceScope serviceScope = serviceScopeFactory.CreateScope();
            return serviceScope;
        }
    }
}