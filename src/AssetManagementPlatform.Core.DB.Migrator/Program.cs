﻿using System;
using AssetManagementPlatform.ConstructionModule;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.SchoolModule;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DB.Migrator
{
    public static class Program
    {
        private static void Main()
        {
            Console.WriteLine("Hello World!");
        }
    }

    public class SchoolContextFactory : IDesignTimeDbContextFactory<SchoolContext>
    {
        public SchoolContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optionsBuilder = new DbContextOptionsBuilder<DataContext>()
                .UseMySql("Server=a;Database=b;Uid=c;Pwd=d;");

            return new SchoolContext(optionsBuilder.Options);
        }
    }

    public class ConstructionContextFactory : IDesignTimeDbContextFactory<ConstructionContext>
    {
        public ConstructionContext CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optionsBuilder = new DbContextOptionsBuilder<DataContext>()
                .UseMySql("Server=a;Database=b;Uid=c;Pwd=d;");

            return new ConstructionContext(optionsBuilder.Options);
        }
    }
}