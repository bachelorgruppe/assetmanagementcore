﻿using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities.Interfaces;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.Core.Code
{
    public static class DbExtensions
    {
        /// <summary>
        /// Gets exactly one entity from the database matching a given <see cref="IHelperId"/>.
        /// This ensures we don't give the DB garbage.
        /// </summary>
        /// <typeparam name="TEntity">The entity type we should return.</typeparam>
        /// <typeparam name="TIdEntity">The entity Id type that should be matched against.</typeparam>
        /// <param name="queryable">The data.</param>
        /// <param name="entity">The Id of the entity we require.</param>
        /// <returns>The item from the database.</returns>
        [NotNull]
        public static TEntity GetEntity<TEntity, TIdEntity>([NotNull]this IQueryable<TEntity> queryable, [NotNull]TIdEntity entity) where TEntity : IId where TIdEntity : IHelperId
        {
            return queryable.Single(e => e.Id == entity.Id);
        }

        /// <summary>
        /// Gets exactly one entity from the database matching a given <see cref="IHelperId"/>.
        /// This ensures we don't give the DB garbage.
        /// </summary>
        /// <typeparam name="TEntity">The entity type we should return.</typeparam>
        /// <typeparam name="TIdEntity">The entity Id type that should be matched against.</typeparam>
        /// <param name="queryable">The data.</param>
        /// <param name="entity">The Id of the entity we require.</param>
        /// <returns>The item from the database.</returns>
        [NotNull]
        public static Task<TEntity> GetEntityAsync<TEntity, TIdEntity>([NotNull]this IQueryable<TEntity> queryable, [NotNull]TIdEntity entity) where TEntity : IId where TIdEntity : IHelperId
        {
            return queryable.SingleAsync(e => e.Id == entity.Id);
        }
    }
}