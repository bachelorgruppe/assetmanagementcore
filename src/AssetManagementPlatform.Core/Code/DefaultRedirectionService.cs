﻿using AssetManagementPlatform.Core.Abstractions;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Code
{
    /// <summary>
    /// Default implementation of the module entry-point.
    /// </summary>
    public class DefaultRedirectionService : IHomeRedirectionService
    {
        public DefaultRedirectionService([AspMvcController]string controller, [AspMvcAction]string action)
        {
            Controller = controller;
            Action = action;
        }

        public string Controller { get; }

        public string Action { get; }
    }
}