﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AssetManagementPlatform.Core.Abstractions
{
    /// <summary>
    /// Helps abstract the underlying type of id's used in the database away. 
    /// </summary>
    public interface IHelperId
    {
        /// <summary>
        /// The underlying value for the id type.
        /// </summary>
        [NotNull]
        public Guid Id { get; }
    }
}