﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.Core.Abstractions
{
    /// <summary>
    /// Provides access to the service collection form domain modules if they need to inject services.
    /// </summary>
    public interface ISetup
    {
        void AddServices([NotNull]IServiceCollection services, IConfiguration configuration);
        void AddSetup([NotNull]IApplicationBuilder builder);
    }
}