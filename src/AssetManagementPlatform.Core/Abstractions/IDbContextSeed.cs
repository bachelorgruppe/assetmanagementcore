﻿using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions
{
    [PublicAPI]
    public interface IDbContextSeed
    {
        void Seed();
    }
}