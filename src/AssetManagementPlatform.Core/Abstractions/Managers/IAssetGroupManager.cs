﻿using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions.Managers
{
    [PublicAPI]
    public interface IAssetGroupManager
    {
        [ItemNotNull]
        [NotNull]
        [Pure]
        IQueryable<AssetGroup> GetAssetGroups();

        [ItemNotNull]
        [NotNull]
        [Pure]
        IQueryable<Asset> GetAssetsInGroup([NotNull]AssetGroupId assetGroupId);

        [ItemNotNull]
        [NotNull]
        [Pure]
        IQueryable<AssetGroup> GetGroupsFromAsset([NotNull]AssetId assetId);

        [NotNull]
        AssetGroup CreateAssetGroup([NotNull]string name, [NotNull]ProjectId projectId);

        [NotNull]
        AssetGroup CreateAssetGroup([NotNull]string name);

        void EditAssetGroup([NotNull] AssetGroup assetGroup);
        void RemoveAssetGroup([NotNull]AssetGroupId assetGroupId);
        void AddAssetToGroup([NotNull]AssetGroupId assetGroupId, [NotNull]AssetId assetId);
        void RemoveAssetFromGroup([NotNull]AssetGroupId assetGroupId, [NotNull]AssetId assetId);
        void CreateAssetGroup([NotNull]AssetGroup assetGroup);
    }
}