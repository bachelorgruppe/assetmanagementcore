﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions.Managers
{
    [PublicAPI]
    public interface IReservationManager
    {
        [NotNull]
        ReservationEvent CreateReservation(DateTime reservationStartDate, DateTime reservationEndDateTime,
                                           [NotNull]AssetId reservedTo, [NotNull]AssetId reservedAsset);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<ReservationEvent> GetReservationEventsForAsset([NotNull]AssetId assetId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<ReservationEvent> GetReservationEvents([NotNull]AssetId assetId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<ReservationEvent> GetReservationEvents();

        void CancelReservation([NotNull]EventId reservationEvent);
        ReservationEvent EditReservation([NotNull]ReservationEvent reservationEvent);
    }
}