﻿using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions.Managers
{
    [PublicAPI]
    public interface IOrganizationManager
    {
        [NotNull]
        Organization CreateOrganization([NotNull]string name, [CanBeNull]Location localization = null);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Organization> GetOrganizations();

        void EditOrganization([NotNull]Organization org);

        void RemoveOrganization([NotNull]OrganizationId organizationId);
        void AssignAssetToOrganization([NotNull]AssetId assetId, [NotNull]OrganizationId organizationId);
        void AssignProjectToOrganization([NotNull]ProjectId projectId, [NotNull]OrganizationId organizationId);
    }
}