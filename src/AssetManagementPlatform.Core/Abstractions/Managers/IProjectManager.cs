﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions.Managers
{
    [PublicAPI]
    public interface IProjectManager
    {
        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Project> GetProjects();

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<Project> GetProjectsByOrganization(OrganizationId organizationId);

        void AssignAssetToProject([NotNull]ProjectId projectId, [NotNull]AssetId assetId);
        void RemoveAssetFromProject([NotNull]ProjectId projectId, [NotNull]AssetId assetId);
        void AssignAssetGroupToProject([NotNull]ProjectId projectId, [NotNull]AssetGroupId assetGroupId);
        void RemoveAssetGroupFromProject([NotNull]ProjectId projectId, [NotNull]AssetGroupId assetGroupId);

        [NotNull]
        Project CreateProject(string name, Location location, [NotNull]OrganizationId organizationId, DateTime? deadline = null);

        void RemoveProject([NotNull]ProjectId projectId);
        void EditProject(Project project);
    }
}