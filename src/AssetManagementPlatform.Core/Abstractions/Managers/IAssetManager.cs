﻿using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions.Managers
{
    [PublicAPI]
    public interface IAssetManager
    {
        void RemoveAsset([NotNull]AssetId assetId);

        /// <summary>Assign asset to another asset</summary>
        /// <param name="assetId">The asset to assign</param>
        /// <param name="assetId2"></param>
        void AssignAsset([NotNull]AssetId assetId, [NotNull]AssetId assetId2);

        [ItemNotNull]
        [NotNull]
        [Pure]
        IQueryable<Asset> GetPendingRequests(AssetId assetId);

        void CreateAsset([NotNull]Asset asset);
        void EditAsset([NotNull]Asset asset);

        [ItemNotNull]
        [NotNull]
        [Pure]
        IQueryable<Asset> GetAssets();
    }
}