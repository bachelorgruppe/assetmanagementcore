﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions.Managers
{
    [PublicAPI]
    public interface IEventManager
    {
        /// <summary>Creating a new assigment event, effectively assigning the given asset to the given user</summary>
        /// <param name="assetId">Asset you want to assign</param>
        /// <param name="assetId2">User you want to assign to</param>
        void ConfirmAssignmentRequest([NotNull]EventId eventId, [NotNull]AssetId assetId, [NotNull]AssetId assetId2);

        void CancelEvent([NotNull]EventId eventId);

        [NotNull]
        ReservationEvent CreateReservationEvent(DateTime reservationStartDate, DateTime reservationEndDate, [NotNull]AssetId reservedTo, [NotNull]AssetId reservedAsset);

        [ItemNotNull]
        [NotNull]
        [Pure]

        /// <summary>Retrieves a list of all events for a given asset</summary>
        IQueryable<Event> GetEventsByAsset([NotNull]AssetId assetId);

        [ItemNotNull]
        [NotNull]
        [Pure]

        /// <summary>Retrieves a list of events for a given asset for a specific action type</summary>
        IQueryable<T> GetEventsByType<T>([NotNull]AssetId assetId) where T : Event;

        [ItemNotNull]
        [NotNull]
        [Pure]

        /// <summary>Retrieves a list of events for a specific action type</summary>
        IQueryable<T> GetEventsByType<T>() where T : Event;

        [ItemNotNull]
        [NotNull]
        [Pure]
        IQueryable<T> GetAssignedAssets<T>([NotNull]AssetId user) where T : Asset;

        void CreateEvent(AssetId assetId, Event theEvent);

        [CanBeNull]
        User GetUserAssigned([NotNull]AssetId assetId);

        void EditEvent(Event @event);
    }
}