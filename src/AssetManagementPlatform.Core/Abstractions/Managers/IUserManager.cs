﻿using System.Linq;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions.Managers
{
    [PublicAPI]
    public interface IUserManager
    {
        void RemoveUser([NotNull]UserId userId);

        [NotNull]
        User CreateUser([NotNull]string firstName, [NotNull]string lastName);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<User> GetUserListByProject([NotNull]ProjectId projectId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<User> GetUserListByOrganization([NotNull]OrganizationId organizationId);

        [Pure]
        [NotNull]
        [ItemNotNull]
        IQueryable<User> GetUsers();

        void EditUser(User user);
    }
}