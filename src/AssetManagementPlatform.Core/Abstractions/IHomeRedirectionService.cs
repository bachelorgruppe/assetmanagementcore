﻿using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Abstractions
{
    /// <summary>
    /// Dictates which controller and action should be access when a user opens the "homepage"
    /// </summary>
    [PublicAPI]
    public interface IHomeRedirectionService
    {
        [NotNull]
        public string Controller { get; }

        [NotNull]
        public string Action { get; }
    }
}