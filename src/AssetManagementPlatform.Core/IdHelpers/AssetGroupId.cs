﻿using System;
using System.Diagnostics.CodeAnalysis;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.Core.IdHelpers
{
    public class AssetGroupId : IHelperId
    {
        public AssetGroupId(Guid assetGroupId)
        {
            Id = assetGroupId;
        }

        public Guid Id { get; }

        [SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator AssetGroupId(AssetGroup assetGroup)
        {
            return new AssetGroupId(assetGroup.Id);
        }
    }
}