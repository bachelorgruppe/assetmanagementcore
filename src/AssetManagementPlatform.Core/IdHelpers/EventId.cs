﻿using System;
using System.Diagnostics.CodeAnalysis;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.Core.IdHelpers
{
    public class EventId : IHelperId
    {
        public EventId(Guid eventId)
        {
            Id = eventId;
        }

        public Guid Id { get; }

        [SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator EventId(Event anEvent)
        {
            return new EventId(anEvent.Id);
        }
    }
}