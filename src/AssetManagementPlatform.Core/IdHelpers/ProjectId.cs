﻿using System;
using System.Diagnostics.CodeAnalysis;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.Core.IdHelpers
{
    public class ProjectId : IHelperId
    {
        public ProjectId(Guid projectId)
        {
            Id = projectId;
        }

        public Guid Id { get; }

        [SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator ProjectId(Project project)
        {
            return new ProjectId(project.Id);
        }
    }
}