﻿using System;
using System.Diagnostics.CodeAnalysis;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.IdHelpers
{
    [PublicAPI]
    public class MessageId : IHelperId
    {
        public MessageId(Guid messageId)
        {
            Id = messageId;
        }

        public Guid Id { get; }

        [SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator MessageId(Message message)
        {
            return new MessageId(message.Id);
        }
    }
}