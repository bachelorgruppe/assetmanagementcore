﻿using System;
using System.Diagnostics.CodeAnalysis;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.Core.IdHelpers
{
    public class OrganizationId : IHelperId
    {
        public OrganizationId(Guid organizationId)
        {
            Id = organizationId;
        }

        public Guid Id { get; }

        [SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator OrganizationId(Organization organization)
        {
            return new OrganizationId(organization.Id);
        }
    }
}