﻿using System;
using System.Diagnostics.CodeAnalysis;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.DB.Entities;

namespace AssetManagementPlatform.Core.IdHelpers
{
    public class UserId : IHelperId
    {
        public UserId(Guid userId)
        {
            Id = userId;
        }

        public Guid Id { get; }

        [SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator UserId(User user)
        {
            return new UserId(user.Id);
        }

        [SuppressMessage("Usage", "CA2225:Operator overloads have named alternates", Justification = "<Pending>")]
        public static implicit operator AssetId(UserId userId)
        {
            return new AssetId(userId.Id);
        }
    }
}