﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.Core.SavingManagers
{
    public class SavingOrganizationManager : BaseManager<DataContext>, IOrganizationManager
    {
        private readonly IOrganizationManager _manager;

        public SavingOrganizationManager(DataContext dbContext, IOrganizationManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public Organization CreateOrganization(string name, Location localization = null)
        {
            Organization org = _manager.CreateOrganization(name, localization);
            SaveContext();
            return org;
        }

        public IQueryable<Organization> GetOrganizations()
        {
            return _manager.GetOrganizations();
        }

        public void RemoveOrganization(OrganizationId organizationId)
        {
            _manager.RemoveOrganization(organizationId);
            SaveContext();
        }

        public void AssignAssetToOrganization(AssetId assetId, OrganizationId organizationId)
        {
            _manager.AssignAssetToOrganization(assetId, organizationId);
            SaveContext();
        }

        public void AssignProjectToOrganization(ProjectId projectId, OrganizationId organizationId)
        {
            _manager.AssignProjectToOrganization(projectId, organizationId);
            SaveContext();
        }

        public void EditOrganization(Organization org)
        {
            _manager.EditOrganization(org);
            SaveContext();
        }
    }
}