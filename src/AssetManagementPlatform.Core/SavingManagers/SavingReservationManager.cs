﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.Core.SavingManagers
{
    public class SavingReservationManager : BaseManager<DataContext>, IReservationManager
    {
        private readonly IReservationManager _manager;

        public SavingReservationManager(DataContext dbContext, IReservationManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public ReservationEvent CreateReservation(DateTime reservationStartDate, DateTime reservationEndDateTime,
                                                  AssetId reservedTo, AssetId reservedAsset)
        {
            ReservationEvent reservationEvent = _manager.CreateReservation(reservationStartDate, reservationEndDateTime, reservedTo, reservedAsset);
            SaveContext();
            return reservationEvent;
        }

        public IQueryable<ReservationEvent> GetReservationEventsForAsset(AssetId assetId)
        {
            return _manager.GetReservationEventsForAsset(assetId);
        }

        public IQueryable<ReservationEvent> GetReservationEvents(AssetId assetId)
        {
            return _manager.GetReservationEvents(assetId);
        }

        public IQueryable<ReservationEvent> GetReservationEvents()
        {
            return _manager.GetReservationEvents();
        }

        public void CancelReservation(EventId reservationEvent)
        {
            _manager.CancelReservation(reservationEvent);
            SaveContext();
        }

        public ReservationEvent EditReservation(ReservationEvent reservationEvent)
        {
            reservationEvent = _manager.EditReservation(reservationEvent);
            SaveContext();
            return reservationEvent;
        }
    }
}