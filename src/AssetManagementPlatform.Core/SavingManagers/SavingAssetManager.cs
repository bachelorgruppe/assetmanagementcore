﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.Core.SavingManagers
{
    public class SavingAssetManager : BaseManager<DataContext>, IAssetManager
    {
        private readonly IAssetManager _manager;

        public SavingAssetManager(DataContext dbContext, IAssetManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public void RemoveAsset(AssetId assetId)
        {
            _manager.RemoveAsset(assetId);
            SaveContext();
        }

        public void AssignAsset(AssetId assetId, AssetId assetId2)
        {
            _manager.AssignAsset(assetId, assetId2);
            SaveContext();
        }

        public IQueryable<Asset> GetPendingRequests(AssetId assetId)
        {
            return _manager.GetPendingRequests(assetId);
        }

        public void CreateAsset(Asset asset)
        {
            _manager.CreateAsset(asset);
            SaveContext();
        }

        public void EditAsset(Asset asset)
        {
            _manager.EditAsset(asset);
            SaveContext();
        }

        public IQueryable<Asset> GetAssets()
        {
            return _manager.GetAssets();
        }
    }
}