﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.Core.SavingManagers
{
    public class SavingProjectManager : BaseManager<DataContext>, IProjectManager
    {
        private readonly IProjectManager _manager;

        public SavingProjectManager(DataContext dbContext, IProjectManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<Project> GetProjects()
        {
            return _manager.GetProjects();
        }

        public IQueryable<Project> GetProjectsByOrganization(OrganizationId organizationId)
        {
            return _manager.GetProjectsByOrganization(organizationId);
        }

        public void AssignAssetToProject(ProjectId projectId, AssetId assetId)
        {
            _manager.AssignAssetToProject(projectId, assetId);
            SaveContext();
        }

        public void RemoveAssetFromProject(ProjectId projectId, AssetId assetId)
        {
            _manager.RemoveAssetFromProject(projectId, assetId);
            SaveContext();
        }

        public void AssignAssetGroupToProject(ProjectId projectId, AssetGroupId assetGroupId)
        {
            _manager.AssignAssetGroupToProject(projectId, assetGroupId);
            SaveContext();
        }

        public void RemoveAssetGroupFromProject(ProjectId projectId, AssetGroupId assetGroupId)
        {
            _manager.RemoveAssetGroupFromProject(projectId, assetGroupId);
            SaveContext();
        }

        public Project CreateProject(string name, Location location, OrganizationId organizationId, DateTime? deadline = null)
        {
            Project project = _manager.CreateProject(name, location, organizationId, deadline);
            SaveContext();
            return project;
        }

        public void RemoveProject(ProjectId projectId)
        {
            _manager.RemoveProject(projectId);
            SaveContext();
        }

        public void EditProject(Project project)
        {
            _manager.EditProject(project);
            SaveContext();
        }
    }
}