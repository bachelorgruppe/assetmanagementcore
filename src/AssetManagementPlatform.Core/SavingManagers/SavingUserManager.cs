﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.Core.SavingManagers
{
    public class SavingUserManager : BaseManager<DataContext>, IUserManager
    {
        private readonly IUserManager _userManager;

        public SavingUserManager(DataContext dbContext, IUserManager userManager) : base(dbContext)
        {
            _userManager = userManager;
        }

        public void RemoveUser(UserId userId)
        {
            _userManager.RemoveUser(userId);
            SaveContext();
        }

        public User CreateUser(string firstName, string lastName)
        {
            User user = _userManager.CreateUser(firstName, lastName);
            SaveContext();
            return user;
        }

        public IQueryable<User> GetUserListByProject(ProjectId projectId)
        {
            return _userManager.GetUserListByProject(projectId);
        }

        public IQueryable<User> GetUserListByOrganization(OrganizationId organizationId)
        {
            return _userManager.GetUserListByOrganization(organizationId);
        }

        public IQueryable<User> GetUsers()
        {
            return _userManager.GetUsers();
        }

        public void EditUser(User user)
        {
            _userManager.EditUser(user);
            SaveContext();
        }
    }
}