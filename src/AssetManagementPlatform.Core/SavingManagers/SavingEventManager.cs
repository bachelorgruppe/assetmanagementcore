﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.Core.SavingManagers
{
    public class SavingEventManager : BaseManager<DataContext>, IEventManager
    {
        private readonly IEventManager _manager;

        public SavingEventManager(DataContext dbContext, IEventManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public void CancelEvent(EventId eventId)
        {
            _manager.CancelEvent(eventId);
            SaveContext();
        }

        public void ConfirmAssignmentRequest(EventId eventId, AssetId assetId, AssetId assetId2)
        {
            _manager.ConfirmAssignmentRequest(eventId, assetId, assetId2);
            SaveContext();
        }

        public ReservationEvent CreateReservationEvent(DateTime reservationStartDate, DateTime reservationEndDate, AssetId reservedTo, AssetId reservedAsset)
        {
            ReservationEvent reservationEvent = _manager.CreateReservationEvent(reservationStartDate, reservationEndDate, reservedTo, reservedAsset);
            SaveContext();
            return reservationEvent;
        }

        public IQueryable<Event> GetEventsByAsset(AssetId assetId)
        {
            return _manager.GetEventsByAsset(assetId);
        }

        public IQueryable<T> GetEventsByType<T>(AssetId assetId) where T : Event
        {
            return _manager.GetEventsByType<T>(assetId);
        }

        public IQueryable<T> GetEventsByType<T>() where T : Event
        {
            return _manager.GetEventsByType<T>();
        }

        public IQueryable<T> GetAssignedAssets<T>(AssetId assetId) where T : Asset
        {
            return _manager.GetAssignedAssets<T>(assetId);
        }

        public void CreateEvent(AssetId assetId, Event theEvent)
        {
            _manager.CreateEvent(assetId, theEvent);
            SaveContext();
        }

        public User GetUserAssigned(AssetId assetId)
        {
            return _manager.GetUserAssigned(assetId);
        }

        /// <inheritdoc />
        public void EditEvent(Event @event)
        {
            _manager.EditEvent(@event);
            SaveContext();
        }
    }
}