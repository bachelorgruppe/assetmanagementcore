﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using AssetManagementPlatform.Core.Managers;

namespace AssetManagementPlatform.Core.SavingManagers
{
    public class SavingAssetGroupManager : BaseManager<DataContext>, IAssetGroupManager
    {
        private readonly IAssetGroupManager _manager;

        public SavingAssetGroupManager(DataContext dbContext, IAssetGroupManager manager) : base(dbContext)
        {
            _manager = manager;
        }

        public IQueryable<AssetGroup> GetAssetGroups()
        {
            return _manager.GetAssetGroups();
        }

        public IQueryable<Asset> GetAssetsInGroup(AssetGroupId assetGroupId)
        {
            return _manager.GetAssetsInGroup(assetGroupId);
        }

        public IQueryable<AssetGroup> GetGroupsFromAsset(AssetId assetId)
        {
            return _manager.GetGroupsFromAsset(assetId);
        }

        public AssetGroup CreateAssetGroup(string name, ProjectId projectId)
        {
            AssetGroup assetGroup = _manager.CreateAssetGroup(name, projectId);
            SaveContext();
            return assetGroup;
        }

        public AssetGroup CreateAssetGroup(string name)
        {
            AssetGroup assetGroup = _manager.CreateAssetGroup(name);
            SaveContext();
            return assetGroup;
        }

        public void EditAssetGroup(AssetGroup assetGroup)
        {
            _manager.EditAssetGroup(assetGroup);
            SaveContext();
        }

        public void RemoveAssetGroup(AssetGroupId assetGroupId)
        {
            _manager.RemoveAssetGroup(assetGroupId);
            SaveContext();
        }

        public void AddAssetToGroup(AssetGroupId assetGroupId, AssetId assetId)
        {
            _manager.AddAssetToGroup(assetGroupId, assetId);
            SaveContext();
        }

        public void RemoveAssetFromGroup(AssetGroupId assetGroupId, AssetId assetId)
        {
            _manager.RemoveAssetFromGroup(assetGroupId, assetId);
            SaveContext();
        }

        public void CreateAssetGroup(AssetGroup assetGroup)
        {
            _manager.CreateAssetGroup(assetGroup);
            SaveContext();
        }
    }
}