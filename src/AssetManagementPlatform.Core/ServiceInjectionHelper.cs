﻿using System;
using System.Collections.Generic;
using AssetManagementPlatform.Core.Abstractions;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Managers;
using AssetManagementPlatform.Core.SavingManagers;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.Core
{
    public static class ServiceInjectionHelper
    {
        private static IEnumerable<ISetup> _setups => TypeHelper.CreateInstanceOfAll<ISetup>();

        public static void AddModuleSetups(this IServiceCollection collection, IConfiguration configuration)
        {
            foreach (ISetup setup in _setups)
            {
                setup.AddServices(collection, configuration);
            }
        }

        public static void AddModuleSetups(this IApplicationBuilder app)
        {
            foreach (ISetup setup in _setups)
            {
                setup.AddSetup(app);
            }
        }

        public static void AddCoreServices(this IServiceCollection services)
        {
            services.AddScoped<IAssetManager, AssetManager>();
            services.AddScoped<SavingAssetManager>();

            services.AddScoped<IAssetGroupManager, AssetGroupManager>();
            services.AddScoped<SavingAssetGroupManager>();

            services.AddScoped<IEventManager, EventManager>();
            services.AddScoped<SavingEventManager>();

            services.AddScoped<IOrganizationManager, OrganizationManager>();
            services.AddScoped<SavingOrganizationManager>();

            services.AddScoped<IProjectManager, ProjectManager>();
            services.AddScoped<SavingProjectManager>();

            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<SavingUserManager>();

            services.AddScoped<IReservationManager, ReservationManager>();
            services.AddScoped<SavingReservationManager>();
        }
    }
}