﻿using System;
using System.Collections.Generic;
using AssetManagementPlatform.Core.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.Core
{
    public static class DbSeedHelper
    {
        public static void SeedContexts(this IServiceProvider serviceProvider)
        {
            IEnumerable<IDbContextSeed> seeders = serviceProvider.GetServices<IDbContextSeed>();

            foreach (IDbContextSeed seed in seeders)
            {
                seed.Seed();
            }
        }
    }
}