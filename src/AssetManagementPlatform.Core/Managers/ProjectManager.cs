﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.Core.Managers
{
    public class ProjectManager : BaseManager<DataContext>, IProjectManager
    {
        public ProjectManager(DataContext dbContext) : base(dbContext) { }

        public IQueryable<Project> GetProjects()
        {
            return Db.Projects.AsNoTracking();
        }

        public IQueryable<Project> GetProjectsByOrganization(OrganizationId organizationId)
        {
            return GetProjects()
                .Where(x => x.OrganizationId == organizationId.Id);
        }

        public void AssignAssetToProject(ProjectId projectId, AssetId assetId)
        {
            Asset asset = Db.Assets.GetEntity(assetId);
            Db.Projects.Include(p => p.Assets).GetEntity(projectId).Assets.Add(asset);
        }

        public void RemoveAssetFromProject(ProjectId projectId, AssetId assetId)
        {
            Asset asset = Db.Assets.GetEntity(assetId);
            Db.Projects.Include(p => p.Assets).GetEntity(projectId).Assets.Remove(asset);
        }

        public void AssignAssetGroupToProject(ProjectId projectId, AssetGroupId assetGroupId)
        {
            AssetGroup assetGroup = Db.AssetGroups.GetEntity(assetGroupId);
            Db.Projects.Include(p1 => p1.AssetGroups).GetEntity(projectId).AssetGroups.Add(assetGroup);
        }

        public void RemoveAssetGroupFromProject(ProjectId projectId, AssetGroupId assetGroupId)
        {
            AssetGroup assetGroup = Db.AssetGroups.Single(g => g.Id == assetGroupId.Id && g.ProjectId.Value == projectId.Id);
            assetGroup.Project = null;
        }

        public Project CreateProject(string name, Location location, OrganizationId organizationId, DateTime? deadline = null)
        {
            Project project = new Project
            {
                EndOn = deadline,
                OrganizationId = organizationId.Id,
                Name = name,
                Location = location
            };
            Db.Projects.Add(project);
            return project;
        }

        public void RemoveProject(ProjectId projectId)
        {
            Project project = Db.Projects.FirstOrDefault(x => x.Id == projectId.Id);
            if (project != null)
                Db.Projects.Remove(project);
        }

        public void EditProject(Project project)
        {
            Db.Projects.Update(project);
        }
    }
}