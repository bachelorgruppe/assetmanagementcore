﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;

namespace AssetManagementPlatform.Core.Managers
{
    public class ReservationManager : BaseManager<DataContext>, IReservationManager
    {
        private readonly IEventManager _eventManager;

        public ReservationManager(DataContext dbContext, IEventManager eventManager) : base(dbContext)
        {
            _eventManager = eventManager;
        }

        public IQueryable<ReservationEvent> GetReservationEvents()
        {
            return _eventManager.GetEventsByType<ReservationEvent>();
        }

        public IQueryable<ReservationEvent> GetReservationEventsForAsset(AssetId assetId)
        {
            return _eventManager.GetEventsByType<ReservationEvent>().Where(x => x.ReservedAssetId == assetId.Id);
        }

        public IQueryable<ReservationEvent> GetReservationEvents(AssetId assetId)
        {
            return _eventManager.GetEventsByType<ReservationEvent>().Where(x => x.ReservedToId == assetId.Id);
        }

        public void CancelReservation(EventId reservationEvent)
        {
            ReservationEvent entity = _eventManager.GetEventsByType<ReservationEvent>()
                                                   .GetEntity(reservationEvent);
            _eventManager.CancelEvent(entity);
        }

        public ReservationEvent CreateReservation(DateTime reservationStartDate, DateTime reservationEndDate,
                                                  AssetId reservedTo, AssetId reservedAsset)
        {
            _eventManager.CreateEvent(reservedTo, new ReservationCreatedEvent
            {
                ReservationStartDate = reservationStartDate,
                ReservationEndDate = reservationEndDate,
                ReservedAssetId = reservedAsset.Id,
            });

            var theEvent = new ReservationEvent
            {
                ReservationStartDate = reservationStartDate,
                ReservationEndDate = reservationEndDate,
                ReservedToId = reservedTo.Id,
                ReservedAssetId = reservedAsset.Id
            };
            _eventManager.CreateEvent(reservedAsset, theEvent);
            return theEvent;
        }

        public ReservationEvent EditReservation(ReservationEvent reservationEvent)
        {
            ReservationEvent reservation = _eventManager.GetEventsByType<ReservationEvent>().Single(r => r.Id == reservationEvent.Id);
            _eventManager.CancelEvent(reservationEvent);

            AssetId reservedAsset = new AssetId(reservationEvent.ReservedAssetId);
            AssetId reservedTo = new AssetId(reservationEvent.ReservedToId);
            return CreateReservation(reservation.ReservationStartDate, reservation.ReservationStartDate, reservedTo, reservedAsset);
        }
    }
}