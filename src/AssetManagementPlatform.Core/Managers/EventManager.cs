﻿using System;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.Core.Managers
{
    public class EventManager : BaseManager<DataContext>, IEventManager
    {
        public EventManager(DataContext dbContext) : base(dbContext) { }

        public void CreateEvent(AssetId assetId, Event theEvent)
        {
            theEvent.Type = theEvent.GetType().Name;
            theEvent.AssetId = assetId.Id;
            Db.Events.Add(theEvent);
        }

        public void CancelEvent(EventId eventId)
        {
            Event entity = Db.Events.GetEntity(eventId);
            entity.IsActive = false;
            Db.Events.Update(entity);
        }   
        
        public void EditEvent(Event @event)
        {
            Db.Events.Update(@event);
        }

        public void ConfirmAssignmentRequest(EventId eventId, AssetId assetId, AssetId assetId2)
        {
            CancelEvent(eventId);
            CreateEvent(assetId, new AssignmentEvent
            {
                AssignedUserId = assetId2.Id,
            });
        }

        public ReservationEvent CreateReservationEvent(DateTime reservationStartDate, DateTime reservationEndDateTime, AssetId reservedTo, AssetId reservedAsset)
        {
            ReservationEvent reservationEvent = new ReservationEvent { ReservationStartDate = reservationStartDate, ReservationEndDate = reservationEndDateTime, ReservedToId = reservedTo.Id, ReservedAssetId = reservedAsset.Id };

            Db.Events.Add(reservationEvent);
            return reservationEvent;
        }

        /// <summary>Retrieves a list of all events for a given asset</summary>
        public IQueryable<Event> GetEventsByAsset(AssetId assetId)
        {
            return Db.Events.AsNoTracking().Where(e => e.AssetId == assetId.Id);
        }

        /// <summary>Retrives a list of events for a given asset for a specific action type</summary>
        public IQueryable<T> GetEventsByType<T>(AssetId assetId) where T : Event
        {
            return GetEventsByType<T>().Where(e => e.AssetId == assetId.Id);
        }

        public User GetUserAssigned(AssetId assetId)
        {
            return GetEventsByType<AssignmentEvent>(assetId)
                   .Include(x => x.AssignedUser)
                   .Select(x => (User)x.AssignedUser)
                   .FirstOrDefault();
        }

        /// <summary>Retrives a list of events for a specific action type</summary>
        public IQueryable<T> GetEventsByType<T>() where T : Event
        {
            return Db.Events.AsNoTracking().OrderByDescending(x => x.CreatedOn).OfType<T>();
        }

        public IQueryable<T> GetAssignedAssets<T>(AssetId assetId) where T : Asset
        {
            return Db.Assets.AsNoTracking()
                     .Include(a => a.Events)
                     .Where(a => a.Events
                                  .OfType<AssignmentEvent>()
                                  .OrderByDescending(action => action.CreatedOn)
                                  .FirstOrDefault()
                                  .AssignedUserId ==
                                 assetId.Id)
                     .Cast<T>();
        }
    }
}