﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.Core.Managers
{
    public class UserManager : BaseManager<DataContext>, IUserManager
    {
        public UserManager(DataContext dbContext) : base(dbContext) { }

        public User CreateUser(string firstName, string lastName)
        {
            User user = new User
            {
                FirstName = firstName,
                LastName = lastName,
            };

            Db.Users.Add(user);
            return user;
        }

        public IQueryable<User> GetUserListByProject(ProjectId projectId)
        {
            return GetUsers().Where(e => e.ProjectId == projectId.Id);
        }

        public IQueryable<User> GetUserListByOrganization(OrganizationId organizationId)
        {
            return GetUsers().Where(e => e.OrganizationId == organizationId.Id);
        }

        public IQueryable<User> GetUsers()
        {
            return Db.Users.AsNoTracking();
        }

        public void EditUser(User user)
        {
            Db.Users.Update(user);
        }

        public void RemoveUser(UserId userId)
        {
            User user = Db.Users.GetEntity(userId);
            Db.Users.Remove(user);
        }
    }
}