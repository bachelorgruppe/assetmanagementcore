﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.DB.Entities.Events;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.Core.Managers
{
    public class AssetManager : BaseManager<DataContext>, IAssetManager
    {
        private readonly IEventManager _eventManager;

        public AssetManager(DataContext dbContext, IEventManager eventManager) : base(dbContext)
        {
            _eventManager = eventManager;
        }

        public void CreateAsset(Asset asset)
        {
            Db.Assets.Add(asset);
        }

        public void EditAsset(Asset asset)
        {
            Db.Assets.Update(asset);
        }

        public IQueryable<Asset> GetAssets()
        {
            return Db.Assets.AsNoTracking();
        }

        public void RemoveAsset(AssetId assetId)
        {
            Asset entity = Db.Assets.GetEntity(assetId);
            Db.Assets.Remove(entity);
        }

        public void AssignAsset(AssetId assetId, AssetId assetId2)
        {
            _eventManager.CreateEvent(assetId, new AssignmentEvent
            {
                AssignedUserId = assetId2.Id
            });
        }

        public IQueryable<Asset> GetPendingRequests(AssetId assetId)
        {
            return _eventManager.GetEventsByType<AssignmentRequestEvent>()
                .Where(x => x.IsActive && x.AssetId == assetId.Id)
                .Include(x => x.RequestingUser)
                .Select(x => x.RequestingUser);
        }
    }
}