﻿using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.Core.Managers
{
    public class OrganizationManager : BaseManager<DataContext>, IOrganizationManager
    {
        public OrganizationManager(DataContext dbContext) : base(dbContext) { }

        public Organization CreateOrganization(string name, Location localization = null)
        {
            Organization organization = new Organization { Name = name };

            if (localization != null)
                organization.Location = localization;

            Db.Organizations.Add(organization);
            return organization;
        }

        public IQueryable<Organization> GetOrganizations()
        {
            return Db.Organizations.AsNoTracking()
                     .Include(x => x.Location);
        }

        public void EditOrganization(Organization org)
        {
            Db.Organizations.Update(org);
        }

        public void RemoveOrganization(OrganizationId organizationId)
        {
            Organization organization = Db.Organizations.GetEntity(organizationId);
            Db.Organizations.Remove(organization);
        }

        public void AssignAssetToOrganization(AssetId assetId, OrganizationId organizationId)
        {
            Asset asset = Db.Assets.GetEntity(assetId);
            Db.Organizations
              .Include(x => x.Assets)
              .GetEntity(organizationId)
              .Assets
              .Add(asset);
        }

        public void AssignProjectToOrganization(ProjectId projectId, OrganizationId organizationId)
        {
            Project project = Db.Projects.GetEntity(projectId);
            Db.Organizations.GetEntity(organizationId).Projects.Add(project);
        }
    }
}