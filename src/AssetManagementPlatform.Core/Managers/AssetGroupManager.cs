﻿using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.Abstractions.Managers;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using Microsoft.EntityFrameworkCore;

namespace AssetManagementPlatform.Core.Managers
{
    public class AssetGroupManager : BaseManager<DataContext>, IAssetGroupManager
    {
        public AssetGroupManager(DataContext dbContext) : base(dbContext) { }

        public IQueryable<AssetGroup> GetAssetGroups()
        {
            return Db.AssetGroups.AsNoTracking();
        }

        public IQueryable<Asset> GetAssetsInGroup(AssetGroupId assetGroupId)
        {
            return Db.AssetGroupAssets.AsNoTracking().Where(a => a.AssetGroupId == assetGroupId.Id).Include(a => a.Asset).Select(a => a.Asset);
        }

        public IQueryable<AssetGroup> GetGroupsFromAsset(AssetId assetId)
        {
            return Db.AssetGroupAssets.AsNoTracking().Where(a => a.AssetId == assetId.Id).Include(a => a.AssetGroup).Select(a => a.AssetGroup);
        }

        public AssetGroup CreateAssetGroup(string name, ProjectId projectId)
        {
            AssetGroup assetGroup = new AssetGroup();
            assetGroup.Name = name;
            assetGroup.Project = Db.Projects.GetEntity(projectId);

            Db.AssetGroups.Add(assetGroup);

            return assetGroup;
        }

        public AssetGroup CreateAssetGroup(string name)
        {
            AssetGroup assetGroup = new AssetGroup();
            assetGroup.Name = name;

            Db.AssetGroups.Add(assetGroup);

            return assetGroup;
        }

        public void EditAssetGroup(AssetGroup assetGroup)
        {
            Db.AssetGroups.Update(assetGroup);
        }

        public void CreateAssetGroup(AssetGroup assetGroup)
        {
            Db.AssetGroups.Add(assetGroup);
        }

        public void RemoveAssetGroup(AssetGroupId assetGroupId)
        {
            AssetGroup assetGroup = Db.AssetGroups.GetEntity(assetGroupId);

            Db.AssetGroups.Remove(assetGroup);
            List<AssetGroupAsset> removeList = Db.AssetGroupAssets.Where(a => a.AssetGroupId == assetGroupId.Id).ToList();
            Db.AssetGroupAssets.RemoveRange(removeList);
        }

        public void AddAssetToGroup(AssetGroupId assetGroupId, AssetId assetId)
        {
            Db.AssetGroupAssets.Add(new AssetGroupAsset
            {
                AssetId = assetId.Id,
                AssetGroupId = assetGroupId.Id
            });
        }

        public void RemoveAssetFromGroup(AssetGroupId assetGroupId, AssetId assetId)
        {
            AssetGroupAsset toRemove = Db.AssetGroupAssets.Single(a => a.AssetGroupId == assetGroupId.Id && a.AssetId == assetId.Id);
            Db.AssetGroupAssets.Remove(toRemove);
        }
    }
}