﻿using AssetManagementPlatform.Core.DB;

namespace AssetManagementPlatform.Core.Managers
{
    public class BaseManager<T> where T : DataContext
    {
        protected BaseManager(T dbContext)
        {
            Db = dbContext;
        }

        protected T Db { get; }

        protected void SaveContext()
        {
            Db.SaveChanges();
        }
    }
}