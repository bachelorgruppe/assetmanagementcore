﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core
{
    public static class TypeHelper
    {
        /// <summary>
        /// Helps instantiate all implementations of a given type.
        /// </summary>
        /// <typeparam name="T">The type to find.</typeparam>
        /// <returns>A instantiation of all found types.</returns>
        [NotNull]
        [ItemNotNull]
        [Pure]
        public static IEnumerable<T> CreateInstanceOfAll<T>()
        {
            Type interfaceType = typeof(T);
            return AppDomain.CurrentDomain.GetAssemblies()
                            .SelectMany(x => x.GetTypes())
                            .Where(x => interfaceType.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                            .Select(x => (T)Activator.CreateInstance(x));
        }
    }
}