using System;
using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.School.Tests
{
    public class StudentManagerTests
    {
        [Fact]
        public void AssignSaving()
        {
            SchoolTestProvider provider = new SchoolTestProvider();

            Student student;
            Student student2;
            Book book;
            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                SavingBookManager bookManager = scope.ServiceProvider.GetRequiredService<SavingBookManager>();

                student = studentManager.CreateStudent("test", "test", DateTime.UtcNow);
                student2 = studentManager.CreateStudent("test2", "test2", DateTime.UtcNow);

                book = bookManager.CreateBook("Test Book", "Den store bogskriver");

                studentManager.AssignBook(book, student);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();

                List<Book> assignedAssets = eventManager.GetAssignedAssets<Book>(student).ToList();

                Assert.Single(assignedAssets);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                studentManager.AssignBook(book, student2);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingEventManager eventManager = scope.ServiceProvider.GetRequiredService<SavingEventManager>();
                List<Book> assignedAssets = eventManager.GetAssignedAssets<Book>(student).ToList();
                Assert.Empty(assignedAssets);

                List<Book> assignedAssets2 = eventManager.GetAssignedAssets<Book>(student2).ToList();
                Assert.Single(assignedAssets2);
            }

            provider.Dispose();
        }

        [Fact]
        public void CreateSaving()
        {
            SchoolTestProvider provider = new SchoolTestProvider();

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                studentManager.CreateStudent("test", "test", DateTime.UtcNow);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                Assert.NotEmpty(studentManager.GetStudents());
            }

            provider.Dispose();
        }
    }
}