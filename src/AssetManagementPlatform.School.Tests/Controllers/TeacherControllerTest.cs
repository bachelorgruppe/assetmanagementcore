﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Controllers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.Shared;
using AssetManagementPlatform.SchoolModule.ViewModels.Teacher;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.School.Tests.Controllers
{
    public class TeacherControllerTest
    {
        [Fact]
        public async Task Create()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                TeacherController teacherController = scope.ServiceProvider.GetRequiredService<TeacherController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                    new SchoolSitePermissions
                    {
                        IsTeacher = true
                    });
                ViewResult result = teacherController.IndexTeacher(null);
                Assert.IsType<ViewResult>(result);
                List<Teacher> teachers = Assert.IsType<List<Teacher>>(result.Model);
                Assert.Empty(teachers);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                TeacherController teacherController = scope.ServiceProvider.GetRequiredService<TeacherController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                    new SchoolSitePermissions
                    {
                        IsTeacher = true,
                    });
                RedirectToActionResult result = teacherController.Create(new TeacherCreateModel
                {
                    Firstname = "Dracula",
                    LastName = "wadds",
                    DateOfBirth = DateTime.Now,
                    ClassroomSelections = new List<ItemSelection>()
                });
                Assert.IsType<RedirectToActionResult>(result);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                TeacherController teacherController = scope.ServiceProvider.GetRequiredService<TeacherController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                    new SchoolSitePermissions
                    {
                        IsTeacher = true
                    });
                ViewResult result = teacherController.IndexTeacher(null);
                Assert.IsType<ViewResult>(result);
                List<Teacher> teachers = Assert.IsType<List<Teacher>>(result.Model);
                Assert.Single(teachers);
            }

            provider.Dispose();
        }

        [Fact]
        public async Task Delete()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                TeacherController teacherController = scope.ServiceProvider.GetRequiredService<TeacherController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                    new SchoolSitePermissions
                    {
                        IsTeacher = true
                    });
                ViewResult result = teacherController.IndexTeacher(null);
                Assert.IsType<ViewResult>(result);
                List<Teacher> teachers = Assert.IsType<List<Teacher>>(result.Model);
                Assert.Empty(teachers);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                TeacherController teacherController = scope.ServiceProvider.GetRequiredService<TeacherController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                    new SchoolSitePermissions
                    {
                        IsTeacher = true,
                        DeleteTeacher = true
                    });
                SavingTeacherManager teacherManager = scope.ServiceProvider.GetRequiredService<SavingTeacherManager>();
                Teacher teacher = teacherManager.Create("Vladimir", "Dracula", new DateTime());
                teacherManager.Create("Vladimir2", "Dracula2", new DateTime());

                IActionResult result = teacherController.Delete(teacher.Id);
                Assert.IsType<RedirectToActionResult>(result);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                TeacherController teacherController = scope.ServiceProvider.GetRequiredService<TeacherController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                    new SchoolSitePermissions
                    {
                        IsTeacher = true
                    });
                ViewResult result = teacherController.IndexTeacher(null);
                Assert.IsType<ViewResult>(result);
                List<Teacher> teachers = Assert.IsType<List<Teacher>>(result.Model);
                Assert.Single(teachers);
            }

            provider.Dispose();
        }
    }
}