﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Controllers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.Classroom;
using AssetManagementPlatform.SchoolModule.ViewModels.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.School.Tests.Controllers
{
    public class ClassroomControllerTest
    {
        [Fact]
        public async Task Create()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                ClassroomController classroomController = scope.ServiceProvider
                                                               .GetRequiredService<ClassroomController>()
                                                               .WithIdentity(provider.GetTestUser, provider.GetTestOrg,
                                                                             new BaseSitePermissions(), new SchoolSitePermissions() { SeeAllClasses = true });
                IActionResult result = classroomController.Index(null);
                ViewResult viewResult = Assert.IsType<ViewResult>(result);
                List<Classroom> classrooms = Assert.IsType<List<Classroom>>(viewResult.Model);
                Assert.Empty(classrooms);
            }

            using (IServiceScope scope = provider.GetScope())

            {
                ClassroomController classroomController =
                    scope.ServiceProvider.GetRequiredService<ClassroomController>();
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();

                var student = studentManager.CreateStudent("Karl", "Bendtsen", new DateTime());
                var model = new ClassroomCreateModel
                {
                    Name = "Classroom1",
                    StudentSelections = new List<ItemSelection>(),
                    TeacherSelections = new List<ItemSelection>()
                };
                IActionResult result = classroomController.Create(model);
                Assert.IsType<RedirectToActionResult>(result);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                ClassroomController classroomController = scope.ServiceProvider
                                                               .GetRequiredService<ClassroomController>()
                                                               .WithIdentity(provider.GetTestUser, provider.GetTestOrg,
                                                                             new BaseSitePermissions(), new SchoolSitePermissions() { SeeAllClasses = true });
                IActionResult result = classroomController.Index(null);
                ViewResult viewResult = Assert.IsType<ViewResult>(result);
                List<Classroom> classrooms = Assert.IsType<List<Classroom>>(viewResult.Model);
                Assert.Single(classrooms);
            }

            provider.Dispose();
        }

        [Fact]
        public async Task Delete()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                ClassroomController classroomController = scope.ServiceProvider
                                                               .GetRequiredService<ClassroomController>()
                                                               .WithIdentity(provider.GetTestUser, provider.GetTestOrg,
                                                                             new BaseSitePermissions(), new SchoolSitePermissions() { SeeAllClasses = true });
                IActionResult result = classroomController.Index(null);
                ViewResult viewResult = Assert.IsType<ViewResult>(result);
                List<Classroom> classrooms = Assert.IsType<List<Classroom>>(viewResult.Model);
                Assert.Empty(classrooms);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                ClassroomController classroomController =
                    scope.ServiceProvider.GetRequiredService<ClassroomController>();
                SavingClassroomManager classroomManager =
                    scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();
                Classroom classroom = classroomManager.Create("Classroom1");
                classroomManager.Create("Classroom2");

                IActionResult result = classroomController.Delete(classroom.Id);
                Assert.IsType<RedirectToActionResult>(result);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                ClassroomController classroomController = scope.ServiceProvider
                                                               .GetRequiredService<ClassroomController>()
                                                               .WithIdentity(provider.GetTestUser, provider.GetTestOrg,
                                                                             new BaseSitePermissions(), new SchoolSitePermissions() { SeeAllClasses = true });

                IActionResult result = classroomController.Index(null);
                ViewResult viewResult = Assert.IsType<ViewResult>(result);
                List<Classroom> classrooms = Assert.IsType<List<Classroom>>(viewResult.Model);
                Assert.Single(classrooms);
            }

            provider.Dispose();
        }

        [Fact]
        public void AssignStudent()
        {
            SchoolTestProvider provider = new SchoolTestProvider(true);
            Classroom classroom;
            Student student;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingClassroomManager classroomManager =
                    scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();

                classroom = classroomManager.Create("Classroom1");
                student = studentManager.CreateStudent("Shaquille", "O'Neal", new DateTime());
            }

            using (IServiceScope scope = provider.GetScope())
            {
                ClassroomController classroomController = scope.ServiceProvider.GetRequiredService<ClassroomController>();

                ClassroomEditModel editModel = new ClassroomEditModel();
                editModel.Id = classroom.Id;
                editModel.Name = "Classroom1";
                editModel.StudentSelections = new[] { new ItemSelection() { Id = student.Id, IsSelected = true, Name = "Shaquille" } };
                editModel.TeacherSelections = new List<ItemSelection>();
                classroomController.Edit(editModel);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingClassroomManager classroomManager =
                    scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();
                var students = classroomManager.GetStudentsInClass(new ClassroomId(classroom.Id)).ToList();
                Assert.Contains(students, e => e.Id == student.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public void AssignTeacher()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);
            Teacher teacher;
            Classroom classroom;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingClassroomManager classroomManager = scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();
                SavingTeacherManager teacherManager = scope.ServiceProvider.GetRequiredService<SavingTeacherManager>();
                classroom = classroomManager.Create("Classroom1");
                teacher = teacherManager.Create("O'Neal", "Shaquille", new DateTime());
            }

            using (IServiceScope scope = provider.GetScope())
            {
                ClassroomController classroomController = scope.ServiceProvider
                                                               .GetRequiredService<ClassroomController>()
                                                               .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(), new SchoolSitePermissions());

                ClassroomEditModel editModel = new ClassroomEditModel();
                editModel.Id = classroom.Id;
                editModel.Name = "Classroom1";
                editModel.TeacherSelections = new[] { new ItemSelection() { Id = teacher.Id, IsSelected = true, Name = "Shaquille" } };
                editModel.StudentSelections = new List<ItemSelection>();
                classroomController.Edit(editModel);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingClassroomManager classroomManager = scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();

                var teachersInClass = classroomManager.GetTeachersInClass(new ClassroomId(classroom.Id)).ToList();
                Assert.Contains(teachersInClass, e => e.Id == teacher.Id);
            }

            provider.Dispose();
        }

        [Fact]
        public async Task GetClassrooms()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                ClassroomController classroomController = scope.ServiceProvider
                                                               .GetRequiredService<ClassroomController>()
                                                               .WithIdentity(provider.GetTestUser, provider.GetTestOrg,
                                                                             new BaseSitePermissions(), new SchoolSitePermissions() { SeeAllClasses = true });
                SavingClassroomManager classroomManager =
                    scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();
                classroomManager.Create("Classroom1");
                IActionResult result = classroomController.Index(null);
                ViewResult viewResult = Assert.IsType<ViewResult>(result);
                List<Classroom> classrooms = Assert.IsType<List<Classroom>>(viewResult.Model);
                Assert.NotEmpty(classrooms);
            }

            provider.Dispose();
        }
    }
}