﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.SchoolModule.Code.IdHelpers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Controllers;
using AssetManagementPlatform.SchoolModule.Entities;
using AssetManagementPlatform.SchoolModule.ViewModels.Shared;
using AssetManagementPlatform.SchoolModule.ViewModels.Student;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.School.Tests.Controllers
{
    /// <summary>https://code-maze.com/testing-mvc-controllers-asp-net-core/</summary>
    public class StudentControllerTests
    {
        [Fact]
        public void TestCreate()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);

            const string firstNameString = "Gert";
            const string lastnameString = "Andersen";

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                StudentController studentController = scope.ServiceProvider.GetRequiredService<StudentController>()
                    .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                        new SchoolSitePermissions() { SeeAllClasses = true });

                studentController.Create(new StudentCreateModel
                {
                    Firstname = firstNameString,
                    LastName = lastnameString,
                    ClassroomSelections = new List<ItemSelection>()
                });
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                IQueryable<Student> users = studentManager.GetStudents();

                Assert.Contains(users, e => e.FirstName == firstNameString && e.LastName == lastnameString);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestDelete()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);
            Student onlyStudent;

            const string firstNameString = "Gert";
            const string lastnameString = "Andersen";
            DateTime dateOfBirth = DateTime.Today.Date;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                onlyStudent = studentManager.CreateStudent(firstNameString, lastnameString, dateOfBirth);
                Assert.Equal(firstNameString, onlyStudent.FirstName);
                Assert.Equal(lastnameString, onlyStudent.LastName);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                var studentController = scope.ServiceProvider.GetRequiredService<StudentController>()
                    .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                        new SchoolSitePermissions() { SeeAllClasses = true });

                studentController.Delete(onlyStudent.Id);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                Student removedStudent = studentManager.GetStudents().FirstOrDefault(x => x.Id == onlyStudent.Id);
                Assert.Null(removedStudent);
            }

            provider.Dispose();
        }

        [Fact]
        public void TestDetails()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);

            Classroom onlyClassroom;
            Student onlyStudent;

            const string firstNameString = "Gert";
            const string lastnameString = "Andersen";
            const string classroomString = "Maths";

            DateTime dateOfBirth = DateTime.Today.Date;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                SavingClassroomManager classroomManager =
                    scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();

                onlyClassroom = classroomManager.Create(classroomString);
                onlyStudent = studentManager.CreateStudent(firstNameString, lastnameString, dateOfBirth);
                classroomManager.Assign(onlyStudent, onlyClassroom);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingClassroomManager classroomManager = scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();

                StudentController studentController = scope.ServiceProvider.GetRequiredService<StudentController>()
                    .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(), new SchoolSitePermissions() { SeeAllClasses = true });

                ViewResult result = studentController.Details(onlyStudent.Id);
                StudentDetailsModel detailsResult = Assert.IsType<StudentDetailsModel>(result.Model);
                var classroom = detailsResult.AssignedClassrooms.First();

                var students = classroomManager.GetStudentsInClass(new ClassroomId(classroom.Id)).ToList();
                Assert.Contains(students, e => e.Id == onlyStudent.Id);
                Assert.Equal(firstNameString, detailsResult.Firstname);
                Assert.Equal(lastnameString, detailsResult.LastName);
                provider.Dispose();
            }
        }

        [Fact]
        public void TestEdit()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);
            Classroom onlyClassroom1;
            Classroom onlyClassroom2;
            Student onlyStudent;

            const string firstNameString1 = "Gert";
            const string lastnameString1 = "Andersen";
            const string classroomString1 = "Maths";
            DateTime dateOfBirth1 = DateTime.Today.Date;


            const string firstNameString2 = "Børge";
            const string lastnameString2 = "Bo";
            const string classroomString2 = "Physics";
            DateTime dateOfBirth2 = DateTime.Today.Date.AddDays(1);


            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                SavingClassroomManager classroomManager =
                    scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();

                // Create user
                onlyClassroom1 = classroomManager.Create(classroomString1);
                onlyClassroom2 = classroomManager.Create(classroomString2);
                onlyStudent = studentManager.CreateStudent(firstNameString1, lastnameString1, dateOfBirth1);
                classroomManager.Assign(onlyStudent, onlyClassroom1);

                var assignedClasses = classroomManager.GetClassrooms(onlyStudent);

                var classroom = assignedClasses.FirstOrDefault();
                var studentsInClass = classroomManager.GetStudentsInClass(new ClassroomId(classroom.Id)).ToList();
                Assert.Equal(firstNameString1, onlyStudent.FirstName);
                Assert.Equal(lastnameString1, onlyStudent.LastName);
                Assert.Contains(studentsInClass, e => e.Id == onlyStudent.Id);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                StudentController studentController = scope.ServiceProvider.GetRequiredService<StudentController>()
                    .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                        new SchoolSitePermissions() { SeeAllClasses = true });
                // Edit user
                studentController
                    .Edit(new StudentEditModel
                    {
                        Firstname = firstNameString2,
                        LastName = lastnameString2,
                        DateOfBirth = dateOfBirth2,
                        ClassroomSelections = new List<ItemSelection>
                        {
                            new ItemSelection
                            {
                                Id = onlyClassroom1.Id,
                                IsSelected = false,
                                Name = onlyClassroom1.Name
                            },
                            new ItemSelection
                            {
                                Id = onlyClassroom2.Id,
                                IsSelected = true,
                                Name = onlyClassroom2.Name
                            }
                        },
                        Id = onlyStudent.Id
                    });
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();
                SavingClassroomManager classroomManager =
                    scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();

                var assignedClasses = classroomManager.GetClassrooms(onlyStudent);

                var classroom = assignedClasses.FirstOrDefault();

                IQueryable<Student> students = studentManager.GetStudents();

                Assert.DoesNotContain(students, e => e.FirstName == firstNameString1 && e.LastName == lastnameString1);
                Assert.Contains(students, e => e.FirstName == firstNameString2 && e.LastName == lastnameString2);
                var studentsInClass = classroomManager.GetStudentsInClass(new ClassroomId(classroom.Id)).ToList();
                Assert.Contains(students, e => e.Id == onlyStudent.Id);
                Assert.DoesNotContain(onlyClassroom1, assignedClasses);
                Assert.Contains(studentsInClass, e => e.Id == onlyStudent.Id);

            }

            provider.Dispose();
        }

        [Fact]
        public void TestIndex()
        {
            SchoolTestProvider provider = new SchoolTestProvider(false);

            const string firstNameString = "Gert";
            const string lastnameString = "Andersen";

            DateTime dateOfBirth = DateTime.Today.Date;

            using (IServiceScope scope = provider.GetScope())
            {
                SavingStudentManager studentManager = scope.ServiceProvider.GetRequiredService<SavingStudentManager>();


                // Create user
                studentManager.CreateStudent(firstNameString, lastnameString, dateOfBirth);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                StudentController studentController = scope.ServiceProvider.GetRequiredService<StudentController>()
                    .WithIdentity(provider.GetTestUser, provider.GetTestOrg, new BaseSitePermissions(),
                        new SchoolSitePermissions() { SeeAllClasses = true });

                ViewResult result = studentController.Index(null);

                List<Student> students = Assert.IsType<List<Student>>(result.Model);
                Assert.Contains(students,
                    e => e.FirstName == firstNameString && e.LastName == lastnameString &&
                         e.DateOfBirth == dateOfBirth);
            }

            provider.Dispose();
        }
    }
}