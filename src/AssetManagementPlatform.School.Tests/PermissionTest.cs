using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using Xunit;

namespace AssetManagementPlatform.School.Tests
{
    public class PermissionTest
    {
        [Fact]
        public void PermissionInputTest()
        {
            SchoolSitePermissions schoolSitePermissions = SitePermissionsExtensions.CreateFromPermissionString<SchoolSitePermissions>("1");

            Assert.True(schoolSitePermissions.StudentCreate);
        }

        [Fact]
        public void PermissionOutputTest()
        {
            SchoolSitePermissions baseSitePermissions2 = new SchoolSitePermissions();
            baseSitePermissions2.StudentCreate = true;
            string permissionsAsString2 = baseSitePermissions2.GetPermissionsAsString();
            Assert.Equal('1', permissionsAsString2[0]);
        }
    }
}