using System;
using System.Linq;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.School.Tests
{
    public class TeacherManagerTest
    {
        [Fact]
        public void Create()
        {
            SchoolTestProvider provider = new SchoolTestProvider();

            using (IServiceScope scope = provider.GetScope())
            {
                SavingTeacherManager teacherManager = scope.ServiceProvider.GetRequiredService<SavingTeacherManager>();

                Assert.Empty(teacherManager.GetTeachers());
                teacherManager.Create("Vladimir", "Dracula", new DateTime());
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingTeacherManager teacherManager = scope.ServiceProvider.GetRequiredService<SavingTeacherManager>();

                IQueryable<Teacher> result = teacherManager.GetTeachers();
                Assert.NotEmpty(result);
                Assert.Single(result);
            }

            provider.Dispose();
        }

        [Fact]
        public void Delete()
        {
            SchoolTestProvider provider = new SchoolTestProvider();

            using (IServiceScope scope = provider.GetScope())
            {
                SavingTeacherManager teacherManager = scope.ServiceProvider.GetRequiredService<SavingTeacherManager>();

                Teacher teacher = teacherManager.Create("Vladimir", "Dracula", new DateTime());
                Assert.NotEmpty(teacherManager.GetTeachers());
                teacherManager.Delete(teacher);
                IQueryable<Teacher> result = teacherManager.GetTeachers();
                Assert.Empty(result);
            }

            provider.Dispose();
        }
    }
}