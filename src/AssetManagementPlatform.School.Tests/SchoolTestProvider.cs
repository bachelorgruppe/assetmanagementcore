﻿using AssetManagementPlatform.SchoolModule;
using AssetManagementPlatform.SchoolModule.Code.Abstractions;
using AssetManagementPlatform.SchoolModule.Code.Managers;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Controllers;
using AssetManagementPlatform.Shared.Tests;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.School.Tests
{
    public class SchoolTestProvider : TestDependencyBuilder<SchoolContext>
    {
        public SchoolTestProvider(bool disableSeededDb = true) : base(disableSeededDb) { }

        protected override void SetupServices(ServiceCollection services)
        {
            base.SetupServices(services);
            services.AddScoped<IBookManager, BookManager>();
            services.AddScoped<SavingBookManager>();
            services.AddScoped<IStudentManager, StudentManager>();
            services.AddScoped<SavingStudentManager>();
            services.AddScoped<ITeacherManager, TeacherManager>();
            services.AddScoped<SavingTeacherManager>();
            services.AddScoped<IClassroomManager, ClassroomManager>();
            services.AddScoped<SavingClassroomManager>();
            services.AddScoped<IHomeworkManager, HomeworkManager>();
            services.AddScoped<SavingHomeworkManager>();

            services.AddScoped<BookController>();
            services.AddScoped<ClassroomController>();
            services.AddScoped<SchoolController>();
            services.AddScoped<StudentController>();
            services.AddScoped<TeacherController>();
            services.AddScoped<HomeworkController>();
        }
    }
}