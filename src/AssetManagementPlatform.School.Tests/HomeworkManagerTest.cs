using System.Linq;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.School.Tests
{
    public class HomeworkManagerTest
    {
        [Fact]
        public void Create()
        {
            SchoolTestProvider provider = new SchoolTestProvider();

            using (IServiceScope scope = provider.GetScope())
            {
                SavingHomeworkManager homeworkManager = scope.ServiceProvider.GetRequiredService<SavingHomeworkManager>();
                SavingClassroomManager classroomManager = scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();

                Classroom classroom = classroomManager.Create("test");

                Homework homework = new Homework();
                homework.Title = "Hej";
                homework.Classroom = classroom;
                homework.ClassroomId = classroom.Id;
                homeworkManager.CreateHomework(homework);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingHomeworkManager homeworkManager = scope.ServiceProvider.GetRequiredService<SavingHomeworkManager>();
                IQueryable<Homework> allHomework = homeworkManager.GetAllHomework();
                Assert.NotEmpty(allHomework);
            }

            provider.Dispose();
        }
    }
}