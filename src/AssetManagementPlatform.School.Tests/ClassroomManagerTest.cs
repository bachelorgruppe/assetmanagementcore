using System.Linq;
using AssetManagementPlatform.SchoolModule.Code.SavingManagers;
using AssetManagementPlatform.SchoolModule.Entities;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.School.Tests
{
    public class ClassroomManagerTest
    {
        [Fact]
        public void Create()
        {
            SchoolTestProvider provider = new SchoolTestProvider();

            using (IServiceScope scope = provider.GetScope())
            {
                SavingClassroomManager classroomManager = scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();
                Classroom result = classroomManager.Create("Classroom1");
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingClassroomManager classroomManager = scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();
                IQueryable<Classroom> result = classroomManager.GetAllClassrooms();
                Assert.Single(result);
                Assert.Equal("Classroom1", result.First().Name);
            }

            provider.Dispose();
        }

        [Fact]
        public void Delete()
        {
            SchoolTestProvider provider = new SchoolTestProvider();

            using (IServiceScope scope = provider.GetScope())
            {
                SavingClassroomManager classroomManager = scope.ServiceProvider.GetRequiredService<SavingClassroomManager>();
                Classroom classroom = classroomManager.Create("Classroom1");
                classroomManager.Delete(classroom);
                IQueryable<Classroom> result = classroomManager.GetAllClassrooms();
                Assert.Empty(result);
            }

            provider.Dispose();
        }
    }
}