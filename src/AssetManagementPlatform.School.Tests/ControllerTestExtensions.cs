﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.SchoolModule.Code.Authentication;
using AssetManagementPlatform.Shared.Tests;
using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.School.Tests
{
    public static class ControllerTestExtensions
    {
        public static T WithIdentity<T>(this T controller, User user, Organization org, BaseSitePermissions sitePermissions, SchoolSitePermissions schoolSitePermissions) where T : Controller
        {
            controller.EnsureHttpContext();

            ClaimsPrincipal principal = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.GivenName, user.FullName),
                new Claim(Core.Authentication.AssetClaims.OrganizationId, org.Id.ToString()),
                new Claim(Core.Authorization.AssetClaims.Permissions, sitePermissions.GetPermissionsAsString()),
                new Claim(AssetClaims.SchoolPermissions, schoolSitePermissions.GetPermissionsAsString())

                // other required and custom claims
            }, "TestAuthentication"));

            controller.ControllerContext.HttpContext.User = principal;

            return controller;
        }
    }
}