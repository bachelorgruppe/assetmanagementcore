﻿using System.Threading.Tasks;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.Notification.Abstractions;
using AssetManagementPlatform.Core.SavingManagers;
using AssetManagementPlatform.MessageModule.Controllers;
using AssetManagementPlatform.MessageModule.ViewModels.Message;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using AssetManagementPlatform.Shared.Tests;
using AssetManagementPlatform.Shared.Tests.TestHelpers;

namespace AssetManagementPlatform.Core.Messaging.Tests.Controllers
{
    public class MessageControllerTest
    {
        [Fact]
        public async Task BaseCase()
        {
            MessageTestProvider provider = new MessageTestProvider(false);

            using (IServiceScope scope = provider.GetScope())
            {
                MockNotificationService mockNotificationService = (MockNotificationService)scope.ServiceProvider.GetRequiredService<INotificationService>();
                SavingUserManager savingUserManager = scope.ServiceProvider.GetRequiredService<SavingUserManager>();
                MessageController messageController = scope.ServiceProvider.GetRequiredService<MessageController>().WithIdentity(provider.GetTestUser, provider.GetTestOrg);

                User testUser1 = provider.GetTestUser;

                User testUser2 = savingUserManager.CreateUser("testUser", "the second");

                ViewResult result1 = messageController.Index(testUser2.Id);

                Assert.IsType<ViewResult>(result1);
                MessageViewModel model1 = Assert.IsType<MessageViewModel>(result1.Model);

                Assert.Single(model1.UserList);
                Assert.Equal(1, model1.ConversationCount);

                Assert.Empty(model1.Messages);

                Assert.Empty(mockNotificationService.GroupNotifications);
                Assert.Empty(mockNotificationService.UserNotifications);

                // Message testUser2
                MessageViewModel messageViewModel = new MessageViewModel();
                messageViewModel.TextToSend = "Hej";
                messageViewModel.UserToSendTo = testUser2.Id;

                IActionResult result2 = await messageController.SendMessage(messageViewModel);
                Assert.IsType<RedirectToActionResult>(result2);

                ViewResult result3 = messageController.Index(testUser2.Id);
                Assert.IsType<ViewResult>(result3);
                MessageViewModel model2 = Assert.IsType<MessageViewModel>(result3.Model);

                // Find the other user that is not us.
                Assert.Single(model2.UserList);
                Assert.Equal(1, model2.ConversationCount);

                Assert.Single(model2.Messages);
                Assert.Equal("Hej", model2.Messages[0].Text);
                Assert.Equal(testUser1.Id, model2.Messages[0].SenderId);
                Assert.Equal(testUser2.Id, model2.Messages[0].ReceiverId);

                Assert.Empty(mockNotificationService.GroupNotifications);
                Assert.Single(mockNotificationService.UserNotifications);

                Assert.Equal(testUser2.Id, mockNotificationService.UserNotifications[0].Item1.Id);
                Assert.Equal("NewMessage", mockNotificationService.UserNotifications[0].Item2);
            }

            provider.Dispose();
        }
    }
}