﻿using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.Notification.Abstractions;
using AssetManagementPlatform.MessageModule.Abstractions.Managers;
using AssetManagementPlatform.MessageModule.Controllers;
using AssetManagementPlatform.MessageModule.Managers;
using AssetManagementPlatform.MessageModule.SavingManagers;
using AssetManagementPlatform.Shared.Tests;
using AssetManagementPlatform.Shared.Tests.TestHelpers;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.Core.Messaging.Tests
{
    public class MessageTestProvider : TestDependencyBuilder<DataContext>
    {
        public MessageTestProvider(bool disableSeededDb = true) : base(disableSeededDb) { }

        protected override void SetupServices(ServiceCollection services)
        {
            base.SetupServices(services);
            services.AddSingleton<INotificationService, MockNotificationService>();
            services.AddScoped<IMessageManager, MessageManager>();
            services.AddScoped<SavingMessageManager>();
            services.AddScoped<MessageController>();
        }
    }
}