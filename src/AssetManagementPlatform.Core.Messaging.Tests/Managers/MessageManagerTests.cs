﻿using System.Collections.Generic;
using System.Linq;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.MessageModule.SavingManagers;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace AssetManagementPlatform.Core.Messaging.Tests.Managers
{
    public class MessageManagerTests
    {
        [Fact]
        public void SendMessageTest()
        {
            MessageTestProvider provider = new MessageTestProvider();

            User user1;
            User user2;
            using (IServiceScope scope = provider.GetScope())
            {
                using DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();

                user1 = new User { FirstName = "test1" };
                context.Users.Add(user1);

                user2 = new User { FirstName = "test2" };
                context.Users.Add(user2);
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingMessageManager manager = scope.ServiceProvider.GetRequiredService<SavingMessageManager>();
                Assert.Empty(manager.GetMessages());

                manager.Send(user1, user2, "Hello");
                manager.Send(user2, user1, "Hello2");
            }

            using (IServiceScope scope = provider.GetScope())
            {
                SavingMessageManager manager = scope.ServiceProvider.GetRequiredService<SavingMessageManager>();
                List<Message> collection = manager.GetMessages().ToList();
                Assert.NotEmpty(collection);

                Assert.NotEmpty(manager.GetSentMessages(user1));
                Assert.NotEmpty(manager.GetReceivedMessages(user2));

                Assert.NotEmpty(manager.GetReceivedMessages(user1));
                Assert.NotEmpty(manager.GetSentMessages(user2));

                Message message1 = collection[0];
                Assert.Equal(user1.Id, message1.SenderId);
                Assert.Equal(user2.Id, message1.ReceiverId);
                Assert.Equal("Hello", message1.Text);

                Message message2 = collection[1];
                Assert.Equal(user2.Id, message2.SenderId);
                Assert.Equal(user1.Id, message2.ReceiverId);
                Assert.Equal("Hello2", message2.Text);
            }

            provider.Dispose();
        }
    }
}