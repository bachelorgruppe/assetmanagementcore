using System.Collections.Generic;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using AssetManagementPlatform.Core.Authentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AssetManagementPlatform.Core.Authorization
{
    public class MockAuthenticatedUsers : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IDomainClaimProvider _claimsProvider;

        public MockAuthenticatedUsers(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IDomainClaimProvider claimsProvider)
            : base(options, logger, encoder, clock)
        {
            _claimsProvider = claimsProvider;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Cookies.TryGetValue("access", out string cookie))
                return Task.FromResult(AuthenticateResult.Fail("Not logged in"));

            IEnumerable<Claim> claims = _claimsProvider.GetClaims(cookie);

            ClaimsIdentity identity = new ClaimsIdentity(claims, Scheme.Name);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);
            AuthenticationTicket ticket = new AuthenticationTicket(principal, Scheme.Name);

            return Task.FromResult(AuthenticateResult.Success(ticket));
        }
    }
}