﻿using System.Security.Claims;
using AssetManagementPlatform.Core.Authorization.Permissions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AssetManagementPlatform.Core.Authorization
{
    public class AuthorizeActionFilter : IAuthorizationFilter
    {
        private readonly BaseSitePermissions _item;

        public AuthorizeActionFilter(string[] item)
        {
            _item = SitePermissionsExtensions.CreateFromPermissionNames<BaseSitePermissions>(item);
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            Claim claim = context.HttpContext.User.FindFirst(AssetClaims.Permissions);

            if (claim == null)
            {
                context.Result = new ForbidResult();
                return;
            }

            BaseSitePermissions baseSitePermissions = SitePermissionsExtensions.CreateFromPermissionString<BaseSitePermissions>(claim.Value);
            bool isAuthorized = _item.HasAllPermissions(baseSitePermissions);

            if (!isAuthorized)
                context.Result = new ForbidResult();
        }
    }
}