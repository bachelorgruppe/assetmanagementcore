﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;

namespace AssetManagementPlatform.Core.Authorization
{
    [PublicAPI]
    public sealed class RoleAuthorizeAttribute : TypeFilterAttribute
    {
        public RoleAuthorizeAttribute(params string[] item)
            : base(typeof(AuthorizeActionFilter))
        {
            Arguments = new object[] { item };
        }
    }
}