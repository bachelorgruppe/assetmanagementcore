﻿namespace AssetManagementPlatform.Core.Authorization
{
    public static class AssetClaims
    {
        public const string Permissions = "asset_permissions";
    }
}