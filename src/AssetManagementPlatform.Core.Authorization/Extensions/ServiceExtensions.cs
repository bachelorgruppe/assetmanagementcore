﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;

namespace AssetManagementPlatform.Core.Authorization.Extensions
{
    public static class ServiceExtensions
    {
        public static void UseFakeAuth(this IServiceCollection services)
        {
            services.AddAuthentication("BasicAuthentication")
                    .AddScheme<AuthenticationSchemeOptions, MockAuthenticatedUsers>("BasicAuthentication", null);
        }
    }
}