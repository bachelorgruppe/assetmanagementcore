﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using AssetManagementPlatform.Core.Authorization.Permissions;
using AssetManagementPlatform.Core.Code;
using AssetManagementPlatform.Core.DB;
using AssetManagementPlatform.Core.DB.Entities;
using AssetManagementPlatform.Core.IdHelpers;
using JetBrains.Annotations;

namespace AssetManagementPlatform.Core.Authorization.Extensions
{
    [PublicAPI]
    public static class ClaimsPrincipalExtensions
    {
        public static string GetUserEmail([NotNull]this ClaimsPrincipal principal)
        {
            return principal.FindFirst(JwtRegisteredClaimNames.Email).Value;
        }

        public static OrganizationId GetUserOrganizationId([NotNull]this ClaimsPrincipal principal)
        {
            return new OrganizationId(Guid.Parse(principal.FindFirst(Authentication.AssetClaims.OrganizationId).Value));
        }

        public static UserId GetUserId([NotNull]this ClaimsPrincipal principal)
        {
            return new UserId(Guid.Parse(principal.FindFirst(JwtRegisteredClaimNames.NameId).Value));
        }

        public static bool IsUserOfType<T>([NotNull]this ClaimsPrincipal principal, DataContext dbContext) where T : User
        {
            if (!principal.TryGetUserId(out UserId userId))
                return false;

            return dbContext.Users.GetEntity(userId).GetType() == typeof(T);
        }

        public static bool TryGetUserId([NotNull]this ClaimsPrincipal principal, out UserId userId)
        {
            userId = null;
            Claim findFirst = principal.FindFirst(JwtRegisteredClaimNames.NameId);
            if (findFirst != null && Guid.TryParse(findFirst.Value, out Guid id))
            {
                userId = new UserId(id);
                return true;
            }

            return false;
        }

        public static BaseSitePermissions GetUserPermissions([NotNull]this ClaimsPrincipal principal)
        {
            return SitePermissionsExtensions.CreateFromPermissionString<BaseSitePermissions>(principal.FindFirst(AssetClaims.Permissions).Value);
        }
    }
}