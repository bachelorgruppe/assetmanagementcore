﻿using System;

namespace AssetManagementPlatform.Core.Authorization.Permissions
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class OrderAttribute : Attribute
    {
        public OrderAttribute(int order)
        {
            Order = order;
        }

        public int Order { get; }
    }
}