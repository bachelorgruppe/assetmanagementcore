﻿namespace AssetManagementPlatform.Core.Authorization.Permissions
{
    public class BaseSitePermissions : BasePermissions
    {
        [Order(1)]
        public bool Message { get; set; }

        [Order(2)]
        public bool MessageSend { get; set; }

        [Order(3)]
        public bool MessageRead { get; set; }

        [Order(4)]
        public bool CanSeeOrganizations { get; set; }

        [Order(5)]
        public bool ManageOrganizations { get; set; }

        [Order(6)]
        public bool IsAdmin { get; set; }
    }
}