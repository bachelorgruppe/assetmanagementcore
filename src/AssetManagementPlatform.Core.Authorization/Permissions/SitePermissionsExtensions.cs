﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AssetManagementPlatform.Core.Authorization.Permissions
{
    public static class SitePermissionsExtensions
    {
        public static T CreateFromPermissionString<T>(string permissionString) where T : BasePermissions, new()
        {
            T baseSitePermissions = new T();

            IList<PropertyInfo> list = PropertyReflector.GetOrderedProperties(typeof(T));
            int listCount = Math.Min(list.Count, permissionString.Length);
            for (int index = 0; index < listCount; index++)
            {
                PropertyInfo property = list[index];
                bool value = permissionString[index] == '1';
                property.SetValue(baseSitePermissions, value);
            }

            return baseSitePermissions;
        }

        public static string GetPermissionsAsString<T>(this T a) where T : BasePermissions
        {
            StringBuilder stringBuilder = new StringBuilder();
            StringifyProperties(stringBuilder, a);
            return stringBuilder.ToString();
        }

        private static void StringifyProperties<T>(StringBuilder stringBuilder, T theObject) where T : BasePermissions
        {
            foreach (PropertyInfo orderedProperty in PropertyReflector.GetOrderedProperties(theObject.GetType()))
            {
                if ((bool)orderedProperty.GetValue(theObject))
                    stringBuilder.Append('1');
                else
                    stringBuilder.Append('0');
            }
        }

        public static bool HasAllPermissions<T>(this T a, T item) where T : BasePermissions
        {
            bool allEqual = true;
            foreach (PropertyInfo orderedProperty in PropertyReflector.GetOrderedProperties(a.GetType()))
            {
                //Checks the item we are comparing to has the permission set
                if (!(bool)orderedProperty.GetValue(item))
                    continue;

                //Checks the item is set in the current instance
                if ((bool)orderedProperty.GetValue(a))
                    continue;

                allEqual = false;
                break;
            }

            return allEqual;
        }

        public static bool HasAllPermissions<T>(this T a, params string[] strings) where T : BasePermissions, new()
        {
            return a.HasAllPermissions(CreateFromPermissionNames<T>(strings));
        }

        public static T CreateFromPermissionNames<T>(params string[] strings) where T : BasePermissions, new()
        {
            HashSet<string> setProperties = strings.ToHashSet();
            T baseSitePermissions = new T();
            foreach (PropertyInfo orderedProperty in PropertyReflector.GetOrderedProperties(typeof(T)))
            {
                if (setProperties.Contains(orderedProperty.Name))
                    orderedProperty.SetValue(baseSitePermissions, true);
            }

            return baseSitePermissions;
        }
    }
}