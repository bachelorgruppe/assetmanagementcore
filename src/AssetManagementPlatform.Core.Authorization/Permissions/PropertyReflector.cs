﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AssetManagementPlatform.Core.Authorization.Permissions
{
    /// <summary>https://stackoverflow.com/questions/7084206/guarantee-order-of-properties-in-a-list-matches-the-order-they-appear-in-code-fi</summary>
    public static class PropertyReflector
    {
        private static readonly object _syncObj = new object();

        private static readonly Dictionary<Type, List<PropertyInfo>> _propLookup =
            new Dictionary<Type, List<PropertyInfo>>();

        public static IList<PropertyInfo> GetOrderedProperties(Type type)
        {
            lock (_syncObj)
            {
                if (_propLookup.TryGetValue(type, out List<PropertyInfo> props))
                    return props;

                Dictionary<int, PropertyInfo> propsOrder = new Dictionary<int, PropertyInfo>();

                PropertyInfo[] properties = type.GetProperties();
                foreach (PropertyInfo p in properties.Where(info => info.DeclaringType == type))
                {
                    if (!Attribute.IsDefined(p, typeof(OrderAttribute)))
                        continue;

                    OrderAttribute attr = (OrderAttribute)p.GetCustomAttributes(typeof(OrderAttribute), true)[0];

                    propsOrder.Add(attr.Order, p);
                }

                props = new List<PropertyInfo>(propsOrder
                                               .OrderBy(kvp => kvp.Key)
                                               .Select(kvp => kvp.Value));

                _propLookup.Add(type, props);

                return props;
            }
        }
    }
}