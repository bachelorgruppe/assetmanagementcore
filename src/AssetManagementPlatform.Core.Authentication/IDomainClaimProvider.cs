﻿using System.Collections.Generic;
using System.Security.Claims;

namespace AssetManagementPlatform.Core.Authentication
{
    /// <summary>
    /// Must be implemented when user mocking is needed.
    /// </summary>
    public interface IDomainClaimProvider
    {
        IEnumerable<Claim> GetClaims(string id);
    }
}